      SUBROUTINE UGC001(SLIM)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *               LINE SCISSORING AND SHIELDING MODULE                *
C *                                                                   *
C *  THIS IS THE FIRST OF FOUR SUBROUTINES THAT MAY BE USED TO        *
C *  SCISSOR AND SHIELD LINE SEGMENTS.  SUBROUTINE UGC001 IS USED TO  *
C *  INITIALIZE THE PROCESS AND SUBROUTINE UGC002 MAY SUPPLY A        *
C *  NUMBER OF OPTIONAL SHIELD SPECIFICATIONS.  SUBROUTINE UGC003 IS  *
C *  USED TO SUPPLY A LINE SEGMENT END POINT TO THE SCISSORING AND    *
C *  SHIELDING MODULE, AND SUBROUTINE UGC004 IS USED TO RETRIEVE A    *
C *  LINE SEGMENT END POINT FROM THE MODULE.  THE NORMAL SEQUENCE OF  *
C *  CALLS IS THE FOLLOWING: (1) UGC001 IS CALLED TO INITIALIZE       *
C *  PROCESSING, (2) UGC002 IS CALLED A NUMBER OF TIMES TO SUPPLY     *
C *  THE OPTIONAL SHIELD SPECIFICATIONS, (3) UGC003 IS CALLED TO      *
C *  SUPPLY AND END POINT TO THE MODULES, (4) UGC004 IS CALLED        *
C *  REPEATEDLY TO RETRIEVE END POINTS OF LINES UNTIL IT SIGNALS      *
C *  THAT NO MORE DATA IS AVAILABLE, AND (5) STEP 3 IS REPEATED       *
C *  UNTIL NO MORE INPUT IS AVAILABLE.                                *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO INITIALIZE SCISSORING AND         *
C *  SHIELDING FOR LINE SEGMENTS.                                     *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGC001(SLIM)                                              *
C *                                                                   *
C *  THE PARAMETER IN THE CALLING SEQUENCE IS:                        *
C *    SLIM  A ARRAY OF DIMENSION 2 BY 2 WHICH CONTAINS THE           *
C *          SCISSORING LIMITS.  SLIM(1,1) IS THE LOW X VALUE,        *
C *          SLIM(1,2) IS THE HIGH X VALUE, SLIM(2,1) IS THE LOW Y    *
C *          VALUE, AND SLIM(2,2) IS THE HIGH Y VALUE.                *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      REAL          SLIM(2,2)
C
      INCLUDE        'include/ugc000.for'
C
C  INITIALIZE THE LINE SCISSORING AND SHIELDING PROCESS.
      LIMS(1,1)=SLIM(1,1)
      LIMS(1,2)=SLIM(1,2)
      LIMS(2,1)=SLIM(2,1)
      LIMS(2,2)=SLIM(2,2)
      IFLG=0
      NSHD=0
      KAVL=0
      NAVL=0
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE UGC002(SLIM)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *               LINE SCISSORING AND SHIELDING MODULE                *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO SUPPLY A SHIELD TO THE LINE       *
C *  SCISSORING AND SHIELDING MODULE.                                 *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGC002(SLIM)                                              *
C *                                                                   *
C *  THE PARAMETER IN THE CALLING SEQUENCE IS:                        *
C *    SLIM  A ARRAY OF DIMENSION 2 BY 2 WHICH CONTAINS THE SHIELD    *
C *          LIMITS.  SLIM(1,1) IS THE LOW X VALUE, SLIM(1,2) IS THE  *
C *          HIGH X VALUE, SLIM(2,1) IS THE LOW Y VALUE, AND          *
C *          SLIM(2,2) IS THE HIGH Y VALUE.                           *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      REAL          SLIM(2,2)
C
      INCLUDE        'include/ugc000.for'
C
C  SAVE A SHIELD SPECIFICATION.
      NSHD=NSHD+1
      SHLD(NSHD,1,1)=SLIM(1,1)
      SHLD(NSHD,1,2)=SLIM(1,2)
      SHLD(NSHD,2,1)=SLIM(2,1)
      SHLD(NSHD,2,2)=SLIM(2,2)
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE UGC003(BBIT,XCRD,YCRD)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *               LINE SCISSORING AND SHIELDING MODULE                *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO SUPPLY A POINT TO THE LINE        *
C *  SCISSORING AND SHIELDING MODULE.                                 *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGC003(BBIT,XCRD,YCRD)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    BBIT  THE BLANKING BIT: 0 MEANS MOVE WITHOUT DRAWING AND 1     *
C *          MEANS DRAW.                                              *
C *    XCRD  THE X COORDINATE OF A LINE END POINT.                    *
C *    YCRD  THE Y COORDINATE OF A LINE END POINT.                    *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       BBIT
      REAL          XCRD,YCRD
C
      INCLUDE        'include/ugc000.for'
C
      REAL          TSHD(2,2)
      INTEGER       KSHD
      INTEGER       KLIN,LLIN
C
      INTEGER       INT1
C
C  ACCEPT AN END POINT TO BE SCISSORED AND SHIELDED.
      KAVL=0
      NAVL=0
      XPNT(BBIT+1)=XCRD
      YPNT(BBIT+1)=YCRD
      IF (BBIT.EQ.0) THEN
        IFLG=0
        GO TO 201
      END IF
C
C  DO THE ACTUAL SCISSORING AND SHIELDING.
      CALL UGC005(NAVL)
      XPNT(1)=XPNT(2)
      YPNT(1)=YPNT(2)
      IF (NAVL.EQ.0) GO TO 201
      IF (NSHD.GT.0) THEN
        DO 105 KSHD=1,NSHD
          TSHD(1,1)=SHLD(KSHD,1,1)
          TSHD(1,2)=SHLD(KSHD,1,2)
          TSHD(2,1)=SHLD(KSHD,2,1)
          TSHD(2,2)=SHLD(KSHD,2,2)
          DO 103 KLIN=NAVL-1,1,-2
            DO 101 INT1=NAVL,KLIN+2,-1
              XSEG(INT1+2)=XSEG(INT1)
              YSEG(INT1+2)=YSEG(INT1)
  101       CONTINUE
            CALL UGC006(TSHD,KLIN,LLIN)
            IF (LLIN.EQ.4) THEN
              NAVL=NAVL+2
              GO TO 104
            ELSE
              DO 102 INT1=KLIN+2,NAVL
                XSEG(INT1+LLIN-2)=XSEG(INT1+2)
                YSEG(INT1+LLIN-2)=YSEG(INT1+2)
  102         CONTINUE
              NAVL=NAVL+LLIN-2
              IF (NAVL.EQ.0) GO TO 201
            END IF
  103     CONTINUE
  104     CONTINUE
  105   CONTINUE
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
  201 RETURN
C
      END
      SUBROUTINE UGC004(BBIT,XCRD,YCRD)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *               LINE SCISSORING AND SHIELDING MODULE                *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO RETRIEVE A POINT FROM THE LINE    *
C *  SCISSORING AND SHIELDING MODULE.                                 *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGC004(BBIT,XCRD,YCRD)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    BBIT  THE BLANKING BIT OR TERMINATION FLAG: 0 MEANS MOVE       *
C *          WITHOUT DRAWING, 1 MEANS DRAW, AND -1 MEANS NO MORE      *
C *          DATA IS AVAILABLE.                                       *
C *    XCRD  THE X COORDINATE OF A LINE END POINT.                    *
C *    YCRD  THE Y COORDINATE OF A LINE END POINT.                    *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       BBIT
      REAL          XCRD,YCRD
C
      INCLUDE        'include/ugc000.for'
C
C  PASS AN END POINT BACK TO THE CALLING MODULE.
  101 IF (KAVL.GE.NAVL) THEN
        BBIT=-1
      ELSE
        BBIT=MOD(KAVL,2)
        KAVL=KAVL+1
        XCRD=XSEG(KAVL)
        YCRD=YSEG(KAVL)
        IF (IFLG.NE.0) THEN
          IF (BBIT.EQ.0) THEN
            IF (XCRD.EQ.PXCD) THEN
              IF (YCRD.EQ.PYCD) GO TO 101
            END IF
          END IF
        END IF
        IFLG=1
        PXCD=XCRD
        PYCD=YCRD
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE UGC005(NPTS)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *               LINE SCISSORING AND SHIELDING MODULE                *
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO PERFORM THE ACTUAL SCISSORING         *
C *  OPERATION.  THE INPUT POINTS ARE IN THE COMMON ARRAYS XPNT AND   *
C *  YPNT.  THE SCISSORING LIMITS ARE IN THE COMMON ARRAY LIMS.       *
C *  THE OUTPUT LINE SEGMENT, IF ANY, IS IN THE COMMON ARRAYS XSEG    *
C *  AND YSEG.                                                        *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGC005(NPTS)                                              *
C *                                                                   *
C *  THE PARAMETER IN THE CALLING SEQUENCE IS:                        *
C *    NPTS  THE NUMBER OF END POINTS PUT INTO XSEG AND YSEG.  THIS   *
C *          NUMBER WILL BE 0 TO 2.                                   *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       NPTS
C
      INCLUDE        'include/ugc000.for'
C
      REAL          XCRD,YCRD
      INTEGER       FLG1,FLG2,FLGT
C
C  INITIALIZE AND MOVE LINE INTO OUTPUT ARRAY.
      NPTS=0
      XSEG(1)=XPNT(1)
      XSEG(2)=XPNT(2)
      YSEG(1)=YPNT(1)
      YSEG(2)=YPNT(2)
      CALL UGC007(LIMS,XSEG(1),YSEG(1),FLG1)
      CALL UGC007(LIMS,XSEG(2),YSEG(2),FLG2)
C
C  NOW DO THE ACTUAL SCISSORING.
  101 IF ((FLG1.NE.0).OR.(FLG2.NE.0)) THEN
        IF (IAND(FLG1,FLG2).NE.0) GO TO 201
        IF (FLG1.NE.0) THEN
          FLGT=FLG1
        ELSE
          FLGT=FLG2
        END IF
        IF (IAND(FLGT,1).NE.0) THEN
          XCRD=LIMS(1,1)
          YCRD=((YSEG(2)-YSEG(1))*(XCRD-XSEG(1))/
     X          (XSEG(2)-XSEG(1)))+YSEG(1)
        ELSE IF (IAND(FLGT,2).NE.0) THEN
          XCRD=LIMS(1,2)
          YCRD=((YSEG(2)-YSEG(1))*(XCRD-XSEG(1))/
     X          (XSEG(2)-XSEG(1)))+YSEG(1)
        ELSE IF (IAND(FLGT,4).NE.0) THEN
          YCRD=LIMS(2,1)
          XCRD=((XSEG(2)-XSEG(1))*(YCRD-YSEG(1))/
     X          (YSEG(2)-YSEG(1)))+XSEG(1)
        ELSE
          YCRD=LIMS(2,2)
          XCRD=((XSEG(2)-XSEG(1))*(YCRD-YSEG(1))/
     X          (YSEG(2)-YSEG(1)))+XSEG(1)
        END IF
        CALL UGC007(LIMS,XCRD,YCRD,FLGT)
        IF (FLG1.NE.0) THEN
          XSEG(1)=XCRD
          YSEG(1)=YCRD
          FLG1=FLGT
        ELSE
          XSEG(2)=XCRD
          YSEG(2)=YCRD
          FLG2=FLGT
        END IF
        GO TO 101
      END IF
      NPTS=2
C
C  RETURN TO CALLING SUBROUTINE.
  201 RETURN
C
      END
      SUBROUTINE UGC006(CSHD,ILIN,NPTS)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *               LINE SCISSORING AND SHIELDING MODULE                *
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO PERFORM THE ACTUAL SHIELDING          *
C *  OPERATION.  THE INPUT POINTS ARE IN THE COMMON ARRAYS XSEG AND   *
C *  YSEG.  THE OUTPUT LINE SEGMENTS, IF ANY, ARE IN THE COMMON       *
C *  ARRAYS XSEG AND YSEG.                                            *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGC006(CSHD,ILIN,NPTS)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    CSHD  AN ARRAY OF CONTAINING THE CURRENT SHIELD.               *
C *    ILIN  THE INDEX OF THE LINE IN XSEG AND YSEG.                  *
C *    NPTS  THE NUMBER OF END POINTS PUT INTO XSEG AND YSEG.  THIS   *
C *          NUMBER WILL BE 0, 2, OR 4.                               *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      REAL          CSHD(2,2)
      INTEGER       ILIN,NPTS
C
      INCLUDE        'include/ugc000.for'
C
      REAL          XCDS(2),YCDS(2)
      REAL          XCRD,YCRD
      INTEGER       IND1,IND2
      INTEGER       FLG1,FLG2,FLGT
C
C  INITIALIZE AND MOVE LINE INTO WORK ARRAY.
      NPTS=2
      XCDS(1)=XSEG(ILIN)
      XCDS(2)=XSEG(ILIN+1)
      YCDS(1)=YSEG(ILIN)
      YCDS(2)=YSEG(ILIN+1)
      CALL UGC007(CSHD,XCDS(1),YCDS(1),FLG1)
      CALL UGC007(CSHD,XCDS(2),YCDS(2),FLG2)
C
C  NOW DO THE ACTUAL SHIELDING.
      IF (IAND(FLG1,FLG2).NE.0) GO TO 201
      IF ((FLG1.EQ.0).AND.(FLG2.EQ.0)) THEN
        NPTS=0
        GO TO 201
      END IF
      IND1=0
      IND2=0
  101 IF ((FLG1.NE.0).OR.(FLG2.NE.0)) THEN
        IF (IAND(FLG1,FLG2).NE.0) GO TO 201
        IF (FLG1.NE.0) THEN
          FLGT=FLG1
        ELSE
          FLGT=FLG2
        END IF
        IF (IAND(FLGT,1).NE.0) THEN
          XCRD=CSHD(1,1)
          YCRD=((YCDS(2)-YCDS(1))*(XCRD-XCDS(1))/
     X          (XCDS(2)-XCDS(1)))+YCDS(1)
        ELSE IF (IAND(FLGT,2).NE.0) THEN
          XCRD=CSHD(1,2)
          YCRD=((YCDS(2)-YCDS(1))*(XCRD-XCDS(1))/
     X          (XCDS(2)-XCDS(1)))+YCDS(1)
        ELSE IF (IAND(FLGT,4).NE.0) THEN
          YCRD=CSHD(2,1)
          XCRD=((XCDS(2)-XCDS(1))*(YCRD-YCDS(1))/
     X          (YCDS(2)-YCDS(1)))+XCDS(1)
        ELSE
          YCRD=CSHD(2,2)
          XCRD=((XCDS(2)-XCDS(1))*(YCRD-YCDS(1))/
     X          (YCDS(2)-YCDS(1)))+XCDS(1)
        END IF
        CALL UGC007(CSHD,XCRD,YCRD,FLGT)
        IF (FLG1.NE.0) THEN
          XCDS(1)=XCRD
          YCDS(1)=YCRD
          FLG1=FLGT
          IND1=1
        ELSE
          XCDS(2)=XCRD
          YCDS(2)=YCRD
          FLG2=FLGT
          IND2=1
        END IF
        GO TO 101
      END IF
      IF ((IND1.NE.0).AND.(IND2.EQ.0)) THEN
        XSEG(ILIN+1)=XCDS(1)
        YSEG(ILIN+1)=YCDS(1)
      ELSE IF ((IND1.EQ.0).AND.(IND2.NE.0)) THEN
        XSEG(ILIN)=XCDS(2)
        YSEG(ILIN)=YCDS(2)
      ELSE
        XSEG(ILIN+3)=XSEG(ILIN+1)
        XSEG(ILIN+1)=XCDS(1)
        XSEG(ILIN+2)=XCDS(2)
        YSEG(ILIN+3)=YSEG(ILIN+1)
        YSEG(ILIN+1)=YCDS(1)
        YSEG(ILIN+2)=YCDS(2)
        NPTS=4
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
  201 RETURN
C
      END
      SUBROUTINE UGC007(LIMS,XCRD,YCRD,FLAG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *               LINE SCISSORING AND SHIELDING MODULE                *
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO ENCODE THE RELATION OF THE GIVEN      *
C *  POINT WITH A SET OF SCISSORING OR SHIELDING LIMITS.              *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGC007(LIMS,XCRD,YCRD,FLAG)                               *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    LIMS  AN ARRAY CONTAINING THE LIMITS.                          *
C *    XCRD  THE GIVEN X COORDINATE.                                  *
C *    YCRD  THE GIVEN Y COORDINATE.                                  *
C *    FLAG  THE ENCODED FLAG.                                        *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      REAL          LIMS(2,2)
      REAL          XCRD,YCRD
      INTEGER       FLAG
C
C  GENERATE THE FLAG VALUE.
      FLAG=0
      IF (XCRD.LT.LIMS(1,1)) THEN
        FLAG=1
      ELSE IF (XCRD.GT.LIMS(1,2)) THEN
        FLAG=2
      END IF
      IF (YCRD.LT.LIMS(2,1)) THEN
        FLAG=FLAG+4
      ELSE IF (YCRD.GT.LIMS(2,2)) THEN
        FLAG=FLAG+8
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END


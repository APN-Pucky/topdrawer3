/*******************  THE UNIFIED GRAPHICS SYSTEM  *******************
 *     SUBROUTINE TO EXECUTE A SUBROUTINE WITH POINTER ARGUMENTS     *
 *                                                                   *
 *  THIS SUBROUTINE MAY BE USED TO EXECUTE A SUBROUTINE WHEN THE     *
 *  ADDRESS OF THE SUBROUTINE AND/OR THE ADDRESSES OF THE            *
 *  SUBROUTINE'S ARGUMENTS ARE KNOWN.  THIS VERSION OF THE           *
 *  SUBROUTINE IS CALLED BY THE DEVICE-INDEPENDENT MODULES.          *
 *                                                                   *
 *  THE CALLING SEQUENCE IS:                                         *
 *    CALL UGZ006(SADR,NARG,IARG,ARG1,ARG2,...)                      *
 *                                                                   *
 *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
 *    SADR  THE ADDRESS OF THE SUBROUTINE TO BE CALLED.  THIS        *
 *          ADDRESS MUST HAVE BEEN DEVELOPED BY SUBROUTINE UGZ002    *
 *          OR UGZ004.                                               *
 *    NARG  THE NUMBER OF ARGUMENTS THAT ARE GIVEN BY ADDRESSES.     *
 *    IARG  AN ARRAY CONTAINING THE INDICES OF THE ARGUMENTS GIVEN   *
 *          BY ADDRESSES.                                            *
 *    ARG1  FIRST ACTUAL ARGUMENT.                                   *
 *    ARG2  SECOND ACTUAL ARGUMENT.                                  *
 *    ...   ...                                                      *
 *                                                                   *
 *                          ROBERT C. BEACH                          *
 *                    COMPUTATION RESEARCH GROUP                     *
 *                STANFORD LINEAR ACCELERATOR CENTER                 *
 *                                                                   *
 *********************************************************************/
UGZ006(sadr,narg,iarg,arg0,arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8,arg9,
      arg10,arg11,arg12,arg13,arg14,arg15,arg16,arg17,arg18,arg19)
 void (*(*sadr))();
 int  *narg,iarg[10],
      arg0,arg1,
      arg2,arg3,arg4,arg5,arg6,
      arg7,arg8,arg9,
      arg10,arg11,arg12,arg13,arg14,
      arg15,arg16,arg17,arg18,arg19;
{
  typedef union intptr { int i; int *p;} INTPTR;
  INTPTR srg0,srg1,
      srg2,srg3,srg4,srg5,srg6,srg7,srg8,srg9,
      srg10,srg11,srg12,srg13,srg14,
      srg15,srg16,srg17,srg18,srg19;
  void (*adr)();
  int j;
  adr = *sadr;
  srg0.i=arg0;
  srg1.i=arg1;
  srg2.i=arg2;
  srg3.i=arg3;
  srg4.i=arg4;
  srg5.i=arg5;
  srg6.i=arg6;
  srg7.i=arg7;
  srg8.i=arg8;
  srg9.i=arg9;
  srg10.i=arg10;
  srg11.i=arg11;
  srg12.i=arg12;
  srg13.i=arg13;
  srg14.i=arg14;
  srg15.i=arg15;
  srg16.i=arg16;
  srg17.i=arg17;
  srg18.i=arg18;
  srg19.i=arg19;
  for (j=0;j<*narg;j++) {
   switch (iarg[j]-1) {
   case 0:
           srg0.i = *srg0.p;
           break;
   case 1:
           srg1.i = *srg1.p;
           break;
   case 2:
           srg2.i = *srg2.p;
           break;
   case 3:
           srg3.i = *srg3.p;
           break;
   case 4:
           srg4.i = *srg4.p;
           break;
   case 5:
           srg5.i = *srg5.p;
           break;
   case 6:
           srg6.i = *srg6.p;
           break;
   case 7:
           srg7.i = *srg7.p;
           break;
   case 8:
           srg8.i = *srg8.p;
           break;
   case 9:
           srg9.i = *srg9.p;
           break;
   case 10:
           srg10.i = *srg10.p;
           break;
   case 11:
           srg11.i = *srg11.p;
           break;
   case 12:
           srg12.i = *srg12.p;
           break;
   case 13:
           srg13.i = *srg13.p;
           break;
   case 14:
           srg14.i = *srg14.p;
           break;
   case 15:
           srg15.i = *srg15.p;
           break;
   case 16:
           srg16.i = *srg16.p;
           break;
   case 17:
           srg17.i = *srg17.p;
           break;
   case 18:
           srg18.i = *srg18.p;
           break;
   case 19:
           srg19.i = *srg19.p;
           break;
 
   printf(" too many arguments in UGZ006, %d \n",*narg); exit(1);
  }
 }
(*adr)(srg0.i,srg1.i,srg2.i,srg3.i,srg4.i,srg5.i,srg6.i,srg7.i,srg8.i,srg9.i,
srg10.i,srg11.i,srg12.i,srg13.i,srg14.i,srg15.i,srg16.i,srg17.i,srg18.i,srg19);
}

      SUBROUTINE UGPL01(DDIN,DDST,DDEX)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                  DEVICE-DEPENDENT MODULE FOR THE                  *
C *                    LINE DRAWING PSEUDO-DEVICE                     *
C *                                                                   *
C *  THIS SUBROUTINE IS A DEVICE-DEPENDENT MODULE FOR A NON-          *
C *  INTERACTIVE GRAPHIC DISPLAY DEVICE.                              *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGPL01(DDIN,DDST,DDEX)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    DDIN  AN INTEGER INPUT ARRAY.                                  *
C *    DDST  A CHARACTER STRING FOR INPUT AND OUTPUT.                 *
C *    DDEX  AN INTEGER OUTPUT ARRAY.                                 *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       DDIN(*)
      CHARACTER*(*) DDST
      INTEGER       DDEX(*)
C
      INCLUDE        'include/ugddacbk.for'
C
      INCLUDE        'include/ugddxpdl.for'
C
      INTEGER*4     INST(103)
      INTEGER*4     EXST(18),EXLS,EXXN,EXYN,EXXM,EXYM
      REAL*4        EXXZ,EXYZ
      INTEGER*4     EXEX,EX1Z,EX1X,EX1Y,EX2Z,EX2X,EX2Y,
     X              EXR1,EXR2,EXR3,EXFG
      EQUIVALENCE   (EXLS,EXST( 1)),      (EXXN,EXST( 2)),
     X              (EXYN,EXST( 3)),      (EXXM,EXST( 4)),
     X              (EXYM,EXST( 5)),      (EXXZ,EXST( 6)),
     X              (EXYZ,EXST( 7)),      (EXEX,EXST( 8)),
     X              (EX1Z,EXST( 9)),      (EX1X,EXST(10)),
     X              (EX1Y,EXST(11)),      (EX2Z,EXST(12)),
     X              (EX2X,EXST(13)),      (EX2Y,EXST(14)),
     X              (EXR1,EXST(15)),      (EXR2,EXST(16)),
     X              (EXR3,EXST(17)),      (EXFG,EXST(18))
C
      INTEGER*4     IARY(71)
      CHARACTER*256 CARY
      EQUIVALENCE   (IARY(8),CARY)
C
      INTEGER       INT1
C
      DATA          INST/18,1,6, 1,1,'PASS','LS  ',
     X                      2,4, 2,0,'XMIN',
     X                      2,4, 3,0,'YMIN',
     X                      2,4, 4,0,'XMAX',
     X                      2,4, 5,0,'YMAX',
     X                      3,4, 6,0,'XSIZ',
     X                      3,4, 7,0,'YSIZ',
     X                      2,9, 8,0,'EXTE','NSIO','N   ',
     X                      2,6, 9,0,'CHR1','SZ  ',
     X                      2,6,10,0,'CHR1','XO  ',
     X                      2,6,11,0,'CHR1','YO  ',
     X                      2,6,12,0,'CHR2','SZ  ',
     X                      2,6,13,0,'CHR2','XO  ',
     X                      2,6,14,0,'CHR2','YO  ',
     X                      1,5,15,1,'CHR9','0   ',
     X                      1,6,16,1,'CHR1','80  ',
     X                      1,6,17,1,'CHR2','70  ',
     X                      2,4,18,0,'FLAG'/
C
C  CHECK OPERATION FLAG AND BRANCH TO THE CORRECT SECTION.
      INT1=DDIN(1)
      IF ((INT1.LT.1).OR.(INT1.GT.10)) GO TO 902
      GO TO (101,151,201,251,301,351,401,451,501,551),INT1
C
C  OPERATION 1: OPEN THE GRAPHIC DEVICE.
  101 EXLS=0
      EXXN=0
      EXYN=0
      EXXM=1023
      EXYM=1023
      EXXZ=25.4
      EXYZ=25.4
      EXEX=0
      EX1Z=0
      EX1X=0
      EX1Y=0
      EX2Z=0
      EX2X=0
      EX2Y=0
      EXR1=0
      EXR2=0
      EXR3=0
      EXFG=1
      CALL UGOPTN(DDST,INST,EXST)
      DDALX=DDXZZ
      CALL UGZ005(DDXRY,DDACX)
      DDAAT='PDEVLIN '
      DDABD(1,1)=EXXN
      DDABD(1,2)=EXXM
      DDABD(2,1)=EXYN
      DDABD(2,2)=EXYM
      IF (EXEX.EQ.1) THEN
        DDABE(1,1)=1
      ELSE IF (EXEX.EQ.2) THEN
        DDABE(2,1)=1
      ELSE IF (EXEX.EQ.3) THEN
        DDABE(1,2)=1
      ELSE IF (EXEX.EQ.4) THEN
        DDABE(2,2)=1
      END IF
      DDABX=EXXZ/REAL(EXXM-EXXN)
      DDABY=EXYZ/REAL(EXYM-EXYN)
      DDXID='DDA/PL00'
      DDXDC=0
      DDXLS=EXLS
      DDXC1(1)=EX1Z
      DDXC1(2)=EX1X
      DDXC1(3)=EX1Y
      DDXC2(1)=EX2Z
      DDXC2(2)=EX2X
      DDXC2(3)=EX2Y
      DDXCR(1)=EXR1
      DDXCR(2)=EXR2
      DDXCR(3)=EXR3
      DDXIO=EXFG
      IARY(1)=2
      IARY(2)=1
      CALL UGXLIN(DDXIO,IARY)
      GO TO 901
C
C  OPERATION 2: CLOSE THE GRAPHIC DEVICE.
  151 IARY(1)=2
      IARY(2)=2
      CALL UGXLIN(DDXIO,IARY)
      GO TO 901
C
C  OPERATION 3: CLEAR THE SCREEN ON THE GRAPHIC DEVICE.
  201 IF (DDIN(2).NE.0) GO TO 902
      DDXDC=1
      GO TO 901
C
C  OPERATION 4: MANIPULATE THE SCREEN ON THE GRAPHIC DEVICE.
  251 GO TO 901
C
C  OPERATION 5: BEGIN A NEW GRAPHIC SEGMENT.
  301 IF (DDXDC.NE.0) THEN
        IARY(1)=2
        IARY(2)=3
        CALL UGXLIN(DDXIO,IARY)
        DDXDC=0
      END IF
      DDEX(2)=0
      GO TO 901
C
C  OPERATION 6: TERMINATE A GRAPHIC SEGMENT.
  351 GO TO 901
C
C  OPERATION 7: MANIPULATE A GRAPHIC SEGMENT.
  401 GO TO 902
C
C  OPERATION 8: INQUIRE ABOUT A GRAPHIC PRIMITIVE.
  451 IARY(1)=6
      IARY(2)=4
      IARY(3)=DDIN(3)
      IARY(4)=DDIN(4)
      IARY(5)=DDIN(5)
      INT1=DDIN(2)
      IF ((INT1.LT.1).OR.(INT1.GT.3)) GO TO 902
      GO TO (461,471,481),INT1
  461 IARY(6)=1
      GO TO 491
  471 IF ((DDXLS.EQ.0).AND.(DDIN(7).NE.1)) GO TO 902
      IARY(6)=DDIN(7)
      GO TO 491
  481 IF (DDXC1(1).EQ.0) GO TO 902
      IF ((DDIN(8).LT.5).OR.(DDIN(8).GT.355)) THEN
        DDXCA=0
      ELSE IF ((DDIN(8).GT.85).AND.(DDIN(8).LT.95).AND.
     X         (DDXCR(1).NE.0)) THEN
        DDXCA=90
      ELSE IF ((DDIN(8).GT.175).AND.(DDIN(8).LT.185).AND.
     X         (DDXCR(2).NE.0)) THEN
        DDXCA=180
      ELSE IF ((DDIN(8).GT.265).AND.(DDIN(8).LT.275).AND.
     X         (DDXCR(3).NE.0)) THEN
        DDXCA=270
      ELSE
        GO TO 902
      END IF
      IF (DDXC2(1).EQ.0) THEN
        IF ((DDIN(7).LT.(DDXC1(1)/2)).AND.
     X      (DDIN(9).NE.2)) GO TO 902
        IF ((DDIN(7).GT.(3*DDXC1(1)/2)).AND.
     X      (DDIN(9).NE.2)) GO TO 902
        GO TO 482
      ELSE
        IF ((DDIN(7).LT.(DDXC1(1)/2)).AND.
     X      (DDIN(9).NE.2)) GO TO 902
        IF ((DDIN(7).GT.(3*DDXC2(1)/2)).AND.
     X      (DDIN(9).NE.2)) GO TO 902
        IF (DDIN(7).LT.((DDXC1(1)+DDXC2(1))/2)) THEN
          GO TO 482
        ELSE
          GO TO 483
        END IF
      END IF
  482 DDXCZ=1
      DDEX(2)=DDXC1(2)
      DDEX(3)=DDXC1(3)
      DDEX(4)=DDXC1(1)
      DDEX(5)=0
      GO TO 484
  483 DDXCZ=2
      DDEX(2)=DDXC2(2)
      DDEX(3)=DDXC2(3)
      DDEX(4)=DDXC2(1)
      DDEX(5)=0
  484 IF (DDXCA.EQ.90) THEN
        INT1=DDEX(2)
        DDEX(2)=-DDEX(3)
        DDEX(3)=INT1
        DDEX(5)=DDEX(4)
        DDEX(4)=0
      ELSE IF (DDXCA.EQ.180) THEN
        DDEX(2)=-DDEX(2)
        DDEX(3)=-DDEX(3)
        DDEX(4)=-DDEX(4)
      ELSE IF (DDXCA.EQ.270) THEN
        INT1=DDEX(2)
        DDEX(2)=DDEX(3)
        DDEX(3)=-INT1
        DDEX(5)=-DDEX(4)
        DDEX(4)=0
      END IF
      IARY(6)=1
  491 CALL UGXLIN(DDXIO,IARY)
      GO TO 901
C
C  OPERATION 9: DISPLAY A GRAPHIC PRIMITIVE.
  501 INT1=DDIN(2)
      IF ((INT1.LT.1).OR.(INT1.GT.3)) GO TO 902
      GO TO (511,521,531),INT1
  511 IARY(1)=4
      IARY(2)=5
      IARY(3)=DDIN(3)
      IARY(4)=DDIN(4)
      CALL UGXLIN(DDXIO,IARY)
      GO TO 901
  521 IARY(1)=5
      IARY(2)=6
      IARY(3)=DDIN(3)
      IARY(4)=DDIN(4)
      IARY(5)=DDIN(5)
      CALL UGXLIN(DDXIO,IARY)
      GO TO 901
  531 IARY(1)=((LEN(DDST)+3)/4)+7
      IARY(2)=7
      IARY(3)=DDIN(3)
      IARY(4)=DDIN(4)
      IARY(5)=DDXCZ
      IARY(6)=DDXCA
      IARY(7)=LEN(DDST)
      DO 532 INT1=1,LEN(CARY)
        CARY(INT1:INT1)=' '
  532 CONTINUE
      CARY(1:LEN(DDST))=DDST
      CALL UGXLIN(DDXIO,IARY)
      GO TO 901
C
C  OPERATION 10: PROCESS MISCELLANEOUS CONTROL FUNCTIONS.
  551 GO TO 902
C
C  SET ERROR INDICATOR AND RETURN TO CALLER.
  901 DDEX(1)=0
      GO TO 903
  902 DDEX(1)=1
  903 RETURN
C
      END


      SUBROUTINE UGE001(FGDR,FGEQ,FGSC,FGMS,XCTR,YCTR,CSIZ,CANG,YFAC,
     X                  DATA,ERFG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                 CHARACTER STROKE GENERATOR MODULE                 *
C *                                                                   *
C *  THIS IS THE FIRST OF TWO SUBROUTINES THAT MAY BE USED TO BREAK   *
C *  A PRIMARY/SECONDARY PAIR OF CHARACTER STRINGS DOWN INTO          *
C *  INDIVIDUAL STROKES.  TWO DISTINCT OPERATIONS MAY BE PERFORMED:   *
C *    1.  GENERATE THE ACTUAL STROKE END POINTS AND RETURN THEM.     *
C *    2.  SCAN THE CHARACTERS AND RETURN THE COORDINATES OF THE END  *
C *        OF THE STRING.  THE END OF STRING MAY BE:                  *
C *        A.  THE CENTER OF THE LAST CHARACTER.                      *
C *        B.  THE CENTER OF THE NEXT CHARACTER.                      *
C *  SUBROUTINE UGE001 IS USED TO INITIALIZE THE PROCESS FOR A NEW    *
C *  PAIR OF STRINGS.  SUBROUTINE UGE002 IS USED TO OBTAIN THE NEXT   *
C *  STROKE END POINT.  IF THE STROKES ARE ONLY BEING SCANNED,        *
C *  UGE002 NEED ONLY BE CALLED ONCE FOLLOWING THE CALL TO UGE001.    *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO INITIALIZE THE CHARACTER STROKE   *
C *  GENERATOR MODULE.                                                *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGE001(FGDR,FGEQ,FGSC,FGMS,XCTR,YCTR,CSIZ,CANG,YFAC,      *
C *                DATA,ERFG)                                         *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    FGDR  DRAW FLAG: 0 MEANS GENERATE STROKES; 1 MEANS SCAN/LAST;  *
C *          AND 2 MEANS SCAN/NEXT.                                   *
C *    FGEQ  EQUAL SPACING FLAG: 0 MEANS EQUAL SPACING; AND 1 MEANS   *
C *          PROPORTIONAL SPACING IF POSSIBLE.                        *
C *    FGSC  SPECIAL CHARACTER FLAG: 0 MEANS DO NOT PROCESS           *
C *          SUPERSCRIPT, SUBSCRIPT, ETC. CONTROL CHARACTERS; 1       *
C *          MEANS PROCESS THESE CHARACTERS.                          *
C *    FGMS  MISSING SECONDARY CHARACTERS FLAG: 0 MEANS CHRS WILL BE  *
C *          A DUMMY ARGUMENT; 1 MEANS IT WILL BE GIVEN.              *
C *    XCTR  THE X COORDINATE OF THE CENTER OF THE FIRST CHARACTER.   *
C *    YCTR  THE Y COORDINATE OF THE CENTER OF THE FIRST CHARACTER.   *
C *    CSIZ  THE SIZE OF THE CHARACTERS.                              *
C *    CANG  THE ANGLE THE CHARACTERS MAKE WITH THE HORIZONTAL.       *
C *    YFAC  THE Y FACTOR MULTIPLIER FOR THE OUTPUT.                  *
C *    DATA  THE BLOCK OF DATA DEFINING THE CHARACTER SET.            *
C *    ERFG  A FLAG WHICH WILL BE NON-ZERO IF DATA IS INVALID.        *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       FGDR,FGEQ,FGSC,FGMS
      REAL          XCTR,YCTR,CSIZ,CANG,YFAC
      INTEGER*2     DATA(*)
      INTEGER       ERFG
C
      INCLUDE        'include/uge000.for'
C
C  INITIALIZE THE CHARACTER STROKE GENERATOR MODULE.
      NCHR=DATA(6)
      IF (NCHR.LE.0) THEN
        ERFG=1
      ELSE
        IFDR=FGDR
        IFEQ=FGEQ
        IFSC=FGSC
        IFMS=FGMS
        CHRO(1)=XCTR
        CHRO(2)=YCTR
        SIZE=CSIZ
        SNCS(1)=SIN(CANG/57.2957795)
        SNCS(2)=COS(CANG/57.2957795)
        XYFC=YFAC
        CSPC=REAL(DATA(7)+0)
        STEP=SIZE/CSPC
        XSZC=1.0
        SAV1(1)=CHRO(1)
        SAV1(2)=CHRO(2)
        SAV1(3)=STEP
        SAV1(4)=XSZC
        SAV2(1)=CHRO(1)
        SAV2(2)=CHRO(2)
        SAV2(3)=STEP
        SAV2(4)=XSZC
        SAV3(1)=CHRO(1)
        SAV3(2)=CHRO(2)
        SAV3(3)=STEP
        SAV3(4)=XSZC
        SAV4(1)=CHRO(1)
        SAV4(2)=CHRO(2)
        SAV4(3)=STEP
        SAV4(4)=XSZC
        ICHR=1
        PTCT=8
        PTOT=PTCT+NCHR
        PTST=PTOT+NCHR
        ISTR=1
        NSTR=0
        ERFG=0
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE UGE002(CHRP,CHRS,CHRN,DATA,BBIT,XCRD,YCRD,SZCH)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                 CHARACTER STROKE GENERATOR MODULE                 *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO RETRIEVE A POINT FROM THE         *
C *  CHARACTER STROKE GENERATOR MODULE.                               *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGE002(CHRP,CHRS,CHRN,DATA,BBIT,XCRD,YCRD,SZCH)           *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    CHRP  THE PRIMARY CHARACTER STRING.                            *
C *    CHRS  THE SECONDARY CHARACTER STRING.                          *
C *    CHRN  THE NUMBER OF CHARACTERS IN THE STRINGS.                 *
C *    DATA  THE BLOCK OF DATA DEFINING THE CHARACTER SET.            *
C *    BBIT  THE BLANKING BIT OR TERMINATION FLAG: 0 MEANS MOVE       *
C *          WITHOUT DRAWING, 1 MEANS DRAW, AND -1 MEANS NO MORE      *
C *          DATA IS AVAILABLE.                                       *
C *    XCRD  THE X COORDINATE OF A STROKE END POINT OR THE END OF     *
C *          STRING X COORDINATE FOR A SCAN.                          *
C *    YCRD  THE Y COORDINATE OF A STROKE END POINT OR THE END OF     *
C *          STRING Y COORDINATE FOR A SCAN.                          *
C *    SZCH  THE SIZE CHANGE FACTOR.  THIS IS ONLY AVAILABLE WHEN     *
C *          BBIT IS -1.                                              *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      CHARACTER*(*) CHRP,CHRS
      INTEGER       CHRN
      INTEGER*2     DATA(*)
      INTEGER       BBIT
      REAL          XCRD,YCRD,SZCH
C
      INCLUDE        'include/uge000.for'
C
      INTEGER*2     CHAR
      CHARACTER*2   CHRX
      EQUIVALENCE   (CHAR,CHRX)
C
      INTEGER       INT1,INT2,INT3
c P. Nason; Fix for reverse byte ordering
      integer * 2   itmp
      character * 2 ctmp
      equivalence   (itmp,ctmp)
C
C  CHECK TO SEE IF THE CHARACTER SET IS AVAILABLE.
      IF (NCHR.LE.0) GO TO 401
C
C  PROCESS THE NEXT STROKE IF ANY ARE AVAILABLE.
  101 IF (ISTR.GT.NSTR) THEN
        IF (NSTR.GT.0) THEN
          IF (IFEQ.EQ.0) CALL UGE003(-0.5*XDSP*STEP,0.0)
          CALL UGE003(CSPC*STEP,0.0)
        END IF
      ELSE
        INT1=DATA(ISTR)
        ISTR=ISTR+1
        BBIT=INT1/16384
        INT1=INT1-16384*BBIT
        CALL UGE003(STEP*REAL((INT1/128)-64),
     X              STEP*REAL(MOD(INT1,128)-64))
        IF ((IFDR.EQ.0).AND.(ISTR.LE.NSTR)) GO TO 402
        GO TO 101
      END IF
C
C  SELECT THE NEXT CHARACTER FROM THE CHARACTER STRINGS IF POSSIBLE.
  201 IF (ICHR.GT.CHRN) GO TO 401
      CHRX(1:1)=CHRP(ICHR:ICHR)
      IF (IFMS.EQ.0) THEN
        CHRX(2:2)=' '
      ELSE
        CHRX(2:2)=CHRS(ICHR:ICHR)
      END IF
      ICHR=ICHR+1
C
C  CHECK FOR SPECIAL CHARACTER PROCESSING.
      IF (IFSC.NE.0) THEN
        IF (CHRX(2:2).EQ.'X') THEN
          IF (CHRX(1:1).EQ.'0') THEN
C           ENTER SUBSCRIPT MODE.
            CALL UGE003(-0.111111111*CSPC*STEP,
     X                  -0.500000000*CSPC*STEP)
            STEP= 0.666666667*STEP
            XSZC= 0.666666667*XSZC
            GO TO 201
          ELSE IF (CHRX(1:1).EQ.'1') THEN
C           LEAVE SUBSCRIPT MODE.
            CALL UGE003( 0.333333333*CSPC*STEP,
     X                   0.750000000*CSPC*STEP)
            STEP= 1.500000000*STEP
            XSZC= 1.500000000*XSZC
            GO TO 201
          ELSE IF (CHRX(1:1).EQ.'2') THEN
C           ENTER SUPERSCRIPT MODE.
            CALL UGE003(-0.111111111*CSPC*STEP,
     X                   0.500000000*CSPC*STEP)
            STEP= 0.666666667*STEP
            XSZC= 0.666666667*XSZC
            GO TO 201
          ELSE IF (CHRX(1:1).EQ.'3') THEN
C           LEAVE SUPERSCRIPT MODE.
            CALL UGE003( 0.333333333*CSPC*STEP,
     X                  -0.750000000*CSPC*STEP)
            STEP= 1.500000000*STEP
            XSZC= 1.500000000*XSZC
            GO TO 201
          END IF
        ELSE IF (CHRX(2:2).EQ.'Y') THEN
          IF (CHRX(1:1).EQ.'0') THEN
C           INCREASE SIZE BY ONE HALF.
            CALL UGE003( 0.166666667*CSPC*STEP,
     X                   0.250000000*CSPC*STEP)
            STEP= 1.500000000*STEP
            XSZC= 1.500000000*XSZC
            GO TO 201
          ELSE IF (CHRX(1:1).EQ.'1') THEN
C           DECREASE SIZE BY ONE THIRD.
            CALL UGE003(-0.111111111*CSPC*STEP,
     X                  -0.166666667*CSPC*STEP)
            STEP= 0.666666667*STEP
            XSZC= 0.666666667*XSZC
            GO TO 201
          END IF
        ELSE IF (CHRX(2:2).EQ.'Z') THEN
          IF (CHRX(1:1).EQ.'0') THEN
C           PUT CURRENT STATE IN FIRST SAVE AREA.
            SAV1(1)=CHRO(1)
            SAV1(2)=CHRO(2)
            SAV1(3)=STEP
            SAV1(4)=XSZC
            GO TO 201
          ELSE IF (CHRX(1:1).EQ.'1') THEN
C           RESTORE STATE FROM FIRST SAVE AREA.
            CHRO(1)=SAV1(1)
            CHRO(2)=SAV1(2)
            STEP=SAV1(3)
            XSZC=SAV1(4)
            GO TO 201
          ELSE IF (CHRX(1:1).EQ.'2') THEN
C           PUT CURRENT STATE IN SECOND SAVE AREA.
            SAV2(1)=CHRO(1)
            SAV2(2)=CHRO(2)
            SAV2(3)=STEP
            SAV2(4)=XSZC
            GO TO 201
          ELSE IF (CHRX(1:1).EQ.'3') THEN
C           RESTORE STATE FROM SECOND SAVE AREA.
            CHRO(1)=SAV2(1)
            CHRO(2)=SAV2(2)
            STEP=SAV2(3)
            XSZC=SAV2(4)
            GO TO 201
          ELSE IF (CHRX(1:1).EQ.'4') THEN
C           PUT CURRENT STATE IN THIRD SAVE AREA.
            SAV3(1)=CHRO(1)
            SAV3(2)=CHRO(2)
            SAV3(3)=STEP
            SAV3(4)=XSZC
            GO TO 201
          ELSE IF (CHRX(1:1).EQ.'5') THEN
C           RESTORE STATE FROM THIRD SAVE AREA.
            CHRO(1)=SAV3(1)
            CHRO(2)=SAV3(2)
            STEP=SAV3(3)
            XSZC=SAV3(4)
            GO TO 201
          ELSE IF (CHRX(1:1).EQ.'6') THEN
C           PUT CURRENT STATE IN FOURTH SAVE AREA.
            SAV4(1)=CHRO(1)
            SAV4(2)=CHRO(2)
            SAV4(3)=STEP
            SAV4(4)=XSZC
            GO TO 201
          ELSE IF (CHRX(1:1).EQ.'7') THEN
C           RESTORE STATE FROM FOURTH SAVE AREA.
            CHRO(1)=SAV4(1)
            CHRO(2)=SAV4(2)
            STEP=SAV4(3)
            XSZC=SAV4(4)
            GO TO 201
          END IF
        END IF
      END IF
C
C  DO A BINARY SEARCH FOR THE CHARACTER IN THE DATA BLOCK.
      INT1=0
      INT2=NCHR-2
      IF (DATA(PTCT+INT1).NE.CHAR) THEN
  301   IF (DATA(PTCT+INT2).NE.CHAR) THEN
  302     IF (INT2.GT.(INT1+1)) THEN
            INT3=(INT1+INT2)/2
c P. Nason, fix for possible reverse byte ordering
            itmp = DATA(PTCT+INT3)
            if(ctmp(2:2).gt.chrx(2:2).or.
     #        (ctmp(2:2).eq.chrx(2:2).and.ctmp(1:1).ge.chrx(1:1))
     #        ) then
              INT2=INT3
              GO TO 301
            ELSE
              INT1=INT3
              GO TO 302
            END IF
          ELSE
            INT2=NCHR-1
          END IF
        END IF
      ELSE
        INT2=INT1
      END IF
      INT1=DATA(PTOT+INT2)+PTST-1
      INT2=DATA(INT1)
      XDSP=REAL(MOD(INT2,128)-64)
      ISTR=INT1+1
      NSTR=(INT2/128)+ISTR-1
C
C  PROCESS THE CHARACTER.
      IF (IFEQ.EQ.0) CALL UGE003(-0.5*XDSP*STEP,0.0)
      GO TO 101
C
C  SIGNAL END OF PROCESSING AND RETURN TO CALLING SUBROUTINE.
  401 IF (IFDR.EQ.1) CALL UGE003(-CSPC*STEP,0.0)
      BBIT=-1
      SZCH=XSZC
  402 XCRD=CHRO(1)
      YCRD=CHRO(2)
      RETURN
C
      END
      SUBROUTINE UGE003(XCRD,YCRD)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                 CHARACTER STROKE GENERATOR MODULE                 *
C *                                                                   *
C *  THIS SUBROUTINE IS USED BY THE CHARACTER STROKE GENERATOR TO     *
C *  TRANSFORM POINTS.  THE RESULT OF THE TRANSFORMATION IS SAVED IN  *
C *  A COMMON BLOCK THAT IS SHARED BY THE OTHER SUBROUTINES IN THE    *
C *  CHARACTER STROKE GENERATOR MODULE.                               *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGE003(XCRD,YCRD)                                         *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    XCRD  THE X COORDINATE OF THE POINT TO BE TRANSFORMED.         *
C *    YCRD  THE Y COORDINATE OF THE POINT TO BE TRANSFORMED.         *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      REAL          XCRD,YCRD
C
      INCLUDE        'include/uge000.for'
C
C  TRANSFORM THE POINT.
      CHRO(1)=     (SNCS(2)*XCRD-SNCS(1)*YCRD)+CHRO(1)
      CHRO(2)=XYFC*(SNCS(1)*XCRD+SNCS(2)*YCRD)+CHRO(2)
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END


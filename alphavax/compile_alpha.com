$! goto link
$ cc ugz006,ugz007
$ define ugsystem [.include]
$ for/noop   tdvax.f, tdsourcen.f, txxug77.f, postscr.f, -
        postscr1.f, seq4010.f, sdd4010.f, tek4010.f, xwindow.f, simplex.f, -
        sddxwdo.f,duplex.f, nucleus.f
$ for/noop   ug3lin.f, ug3mrk.f, ug3pln.f, ug3pmk.f, ug3trn.f, -
        ug3txt.f, ug3wrd.f, ugcntr.f, ugcnvf.f, ug2dhg.f, ug2dhp.f, -
        ugb001.f, ugb002.f, ugb003.f, ugb004.f, -
        ugb005.f, ugb006.f, ugb007.f, ugb008.f, ugb009.f, ugb010.f, -
        ugb011.f, ugb012.f, ugb013.f, ugb014.f, ugb015.f, ugc001.f, -
        ugclos.f, ugctol.f, ugcw01.f, ugd001.f, -
        ugddat.f, ugdefl.f, ugdsab.f, ugdspc.f, uge001.f, ugectl.f, -
        ugenab.f, ugevnt.f, ugf001.f, ugfont.f, ugg001.f, uggd01.f, -
        uggi01.f, uggr01.f, uggs01.f, ugin01.f, uginfo.f, uginit.f, -
        ugix01.f, uglgax.f, uglgdx.f, ugline.f, uglnax.f, uglndx.f
$ for/noop    ugmark.f, ugmctl.f, ugmesh.f, ugmt01.f, ugopen.f, -
        ugoptn.f, ugpfil.f, ugpi01.f, ugpict.f, ugpl01.f, ugplin.f, -
        ugpm01.f, ugpmrk.f, ugproj.f, ugps01.f, ugpu01.f, -
        ugpx01.f, ugqctr.f, ugrerr.f, ugsa01.f, ugsb01.f, ugsc01.f, -
        ugscin.f, ugsd01.f, ugse01.f, ugshld.f, ugslct.f, ugsx01.f, -
        ugta01.f, ugtd01.f, ugtext.f, ugtran.f, ugts01.f, ugtx01.f, -
        ugud01.f, ugus01.f, ugux01.f, ugvf01.f, ugvi01.f, ugvs01.f, -
        ugwa01.f, ugwb01.f, ugwc01.f, ugwd01.f, ugwdow.f, ugwe01.f, -
        ugwrit.f, ugwz01.f, ugxa01.f, ugxb01.f, ugxc01.f, -
        ugxgws.f, ugxhch.f, ugxs01.f, ugxtxt.f, ugz001.f, -
        ugz002.f, ugz003.f, ugz004.f, ugz005.f
$ for/noop   -
        ugzz01.f, -
        ugpr01.f, ugxerr.f, ugxi01.f, uga13dum.f

$ for/noop kludge.f
 
$ lib/create ugsys.olb -
   ug3lin.obj, ug3mrk.obj, ug3pln.obj, ug3pmk.obj, ug3trn.obj, -
   ug3txt.obj, ug3wrd.obj, ugcntr.obj, ugcnvf.obj, ug2dhg.obj, ug2dhp.obj, -
   ugb001.obj, ugb002.obj, ugb003.obj, ugb004.obj, -
   ugb005.obj, ugb006.obj, ugb007.obj, ugb008.obj, ugb009.obj, ugb010.obj, -
   ugb011.obj, ugb012.obj, ugb013.obj, ugb014.obj, ugb015.obj, ugc001.obj, -
   ugclos.obj, ugctol.obj, ugcw01.obj, ugd001.obj, -
   ugddat.obj, ugdefl.obj, ugdsab.obj, ugdspc.obj, uge001.obj, ugectl.obj, -
   ugenab.obj, ugevnt.obj, ugf001.obj, ugfont.obj, ugg001.obj, uggd01.obj, -
   uggi01.obj, uggr01.obj, uggs01.obj, ugin01.obj, uginfo.obj, uginit.obj, -
   ugix01.obj, uglgax.obj, uglgdx.obj, ugline.obj, uglnax.obj, uglndx.obj, -
   ugmark.obj, ugmctl.obj, ugmesh.obj, ugmt01.obj, ugopen.obj, -
   ugoptn.obj, ugpfil.obj, ugpi01.obj, ugpict.obj, ugpl01.obj, ugplin.obj, -
   ugpm01.obj, ugpmrk.obj, ugproj.obj, ugps01.obj, ugpu01.obj
$ lib/ins ugsys.olb -
   ugpx01.obj, ugqctr.obj, ugrerr.obj, ugsa01.obj, ugsb01.obj, ugsc01.obj, -
   ugscin.obj, ugsd01.obj, ugse01.obj, ugshld.obj, ugslct.obj, ugsx01.obj, -
   ugta01.obj, ugtd01.obj, ugtext.obj, ugtran.obj, ugts01.obj, ugtx01.obj, -
   ugud01.obj, ugus01.obj, ugux01.obj, ugvf01.obj, ugvi01.obj, ugvs01.obj, -
   ugwa01.obj, ugwb01.obj, ugwc01.obj, ugwd01.obj, ugwdow.obj, ugwe01.obj, -
   ugwrit.obj, ugwz01.obj, ugxa01.obj, ugxb01.obj, ugxc01.obj, -
   ugxgws.obj, ugxhch.obj, ugxs01.obj, ugxtxt.obj, ugz001.obj, -
   ugz002.obj, ugz003.obj, ugz004.obj, ugz005.obj, ugz006.obj, ugz007.obj, -
   ugzz01.obj,  -
   ugpr01.obj, ugxerr.obj, ugxi01.obj, uga13dum.obj, kludge.obj
$link:
$ link tdvax.obj, tdsourcen.obj, txxug77.obj, postscr.obj, -
   postscr1.obj, tek4010.obj, seq4010.obj, sdd4010.obj,xwindow.obj,sddxwdo.obj,-
   nucleus.obj+simplex.obj+duplex.obj,ugsys/lib, sys$input:/opt
             SYS$LIBRARY:DECW$XLIBSHR/SHARE

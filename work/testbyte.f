      integer * 4 inum
      character * 1 str(4)
      equivalence (inum,str,rnum)
c system dependent values for masks.
c msk1 must have only one bit on, msk2 = inot(msk1),
c and the relevant bit must be irrelevant in real *4 numbers
       DATA          MSK1/ 1/
       DATA          MSK2/-2/
c      data msk1/z00010000/
c      data msk2/zfffeffff/
 1     write(*,*) ' enter inum'
      read(*,*) inum
      write(*,*) (ichar(str(k)),k=1,4),rnum
      inum=msk1
      write(*,*) (ichar(str(k)),k=1,4),rnum
      inum=msk2
      write(*,*) (ichar(str(k)),k=1,4),rnum
      goto 1
      end


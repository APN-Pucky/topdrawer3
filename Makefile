include Makefile.$(shell uname)

ALLFILES = README AAA_tdsun.doc Makefile Makefile.SunOS \
      Makefile.Linux Makefile.AIX Makefile.HP-UX Makefile.OSF1 Makefile.Darwin \
      bill.johnson \
      td.mak td.mak_alpha td.mak_decstation td.mak_sun4 \
      tdintro.listing tdintro.top tdmanual.listing tdmanual.top \
      libstop.f libstop_fix.f mskdata.f ntek4010.f nteksys.f \
      nteksys_fix.f nucleus.f old_uge001.f pdevlin.f npostscr.f postscr.f\
      postscr1.f sdd4010.f seq4010.f simplex.f duplex.f  \
      tdargs.c kludge.c old_ugz006.c \
      tdsourcen.f tdunix.f tdvax.f tek4010.f topdrawer_file.description \
      txxug77.f ug2dhg.f ug2dhp.f ug3lin.f ug3mrk.f ug3pln.f ug3pmk.f \
      ug3trn.f ug3txt.f ug3wrd.f uga13dum.f ugb001.f ugb002.f ugb003.f \
      ugb004.f ugb005.f ugb006.f ugb007.f ugb008.f ugb009.f ugb010.f ugb011.f \
      ugb012.f ugb013.f ugb014.f ugb015.f ugc001.f ugclos.f ugcntr.f ugcnvf.f \
      ugctol.f ugcw01.f ugd001.f ugddat.f ugdefl.f ugdsab.f ugdspc.f uge001.f \
      ugectl.f ugenab.f ugevnt.f ugf001.f ugfont.f ugg001.f uggd01.f uggi01.f \
      uggr01.f uggs01.f ugin01.f uginfo.f uginit.f ugix01.f uglgax.f uglgdx.f \
      ugline.f uglnax.f uglndx.f ugmark.f ugmctl.f ugmesh.f ugmt01.f ugopen.f \
      ugoptn.f ugpfil.f ugpi01.f ugpict.f ugpl01.f ugplin.f ugpm01.f ugpmrk.f \
      ugpr01.f ugproj.f ugps01.f ugpu01.f ugpx01.f ugqctr.f ugrerr.f ugsa01.f \
      ugsb01.f ugsc01.f ugscin.f ugsd01.f ugse01.f ugshld.f ugslct.f ugsx01.f \
      ugta01.f ugtd01.f ugtext.f ugtran.f ugts01.f ugtx01.f ugud01.f ugus01.f \
      ugux01.f ugvf01.f ugvi01.f ugvs01.f ugwa01.f ugwb01.f ugwc01.f ugwd01.f \
      ugwdow.f ugwe01.f ugwrit.f ugwz01.f ugxa01.f ugxb01.f ugxc01.f ugxerr.f \
      ugxgws.f ugxhch.f ugxi01.f ugxs01.f ugxtxt.f ugz001.f ugz001_fix.f \
      ugz002.f ugz003.f ugz003_fix.f ugz004.f ugz005.f ugz006.c ugzz01.f \
      alphavax/aaa_alpha.readme alphavax/aaa_tdsun.doc alphavax/aaaread.me \
      alphavax/compile_alpha.com alphavax/fixes.com alphavax/kludge.f \
      alphavax/mskdata.f alphavax/postscr1.f alphavax/sddxwdo-6-2.f \
      alphavax/sddxwdo.f alphavax/testbyte.for alphavax/ugwdow.f \
      alphavax/ugz006.c alphavax/ugz007.c alphavax/xwindow.f \
      include/ugc000.for   include/ugd000.for   include/ugddacbk.for \
      include/ugddxgin.for include/ugddxgrn.for include/ugddxgsd.for \
      include/ugddxgsq.for include/ugddxim3.for include/ugddximx.for \
      include/ugddxmet.for include/ugddxpdi.for include/ugddxpdl.for \
      include/ugddxpds.for include/ugddxpdu.for include/ugddxprx.for \
      include/ugddxpsc.for include/ugddxskb.for include/ugddxskc.for \
      include/ugddxskd.for include/ugddxske.for include/ugddxsss.for \
      include/ugddxtal.for include/ugddxtin.for include/ugddxtiz.for \
      include/ugddxtka.for include/ugddxtkb.for include/ugddxtkc.for \
      include/ugddxtkd.for include/ugddxtke.for include/ugddxtkz.for \
      include/ugddxtsd.for include/ugddxtsq.for include/ugddxtxa.for \
      include/ugddxtxb.for include/ugddxtxc.for include/ugddxuin.for \
      include/ugddxusd.for include/ugddxusq.for include/ugddxvi2.for \
      include/ugddxvpf.for include/ugddxvs2.for include/ugddxxwi.for \
      include/ugddxxws.for include/uge000.for   include/ugemscbk.for \
      include/ugerrcbk.for include/ugf000.for   include/ugg000.for \
      include/ugioparm.for include/ugioparm_fix.for include/ugmcacbk.for \
      include/ugpotcbk.for include/ugpotdcl.for include/ugsyparm.for \
      include/ugsyparm_fix.for \
      test/README test/f24.top test/f24_save.ps test/f24_save.tek \
      work/disp_bits.f work/testbyte.f work/testtek.f work/test_token.f \
      work/testargs.f

FLSO =	tdargs.o tdunix.o tdsourcen.o txxug77.o npostscr.o \
        postscr1.o ntek4010.o nteksys_fix.o seq4010.o simplex.o \
        duplex.o nucleus.o

FLIB =  ug3lin.o ug3mrk.o ug3pln.o ug3pmk.o ug3trn.o \
        ug3txt.o ug3wrd.o ugcntr.o ugcnvf.o ug2dhg.o ug2dhp.o \
        ugb001.o ugb002.o ugb003.o ugb004.o \
        ugb005.o ugb006.o ugb007.o ugb008.o ugb009.o ugb010.o \
        ugb011.o ugb012.o ugb013.o ugb014.o ugb015.o ugc001.o \
        ugclos.o ugctol.o ugcw01.o ugd001.o \
        ugddat.o ugdefl.o ugdsab.o ugdspc.o uge001.o ugectl.o \
        ugenab.o ugevnt.o ugf001.o ugfont.o ugg001.o uggd01.o \
        uggi01.o uggr01.o uggs01.o ugin01.o uginfo.o uginit.o \
        ugix01.o uglgax.o uglgdx.o ugline.o uglnax.o uglndx.o \
        ugmark.o ugmctl.o ugmesh.o ugmt01.o ugopen.o \
        ugoptn.o ugpfil.o ugpi01.o ugpict.o ugpl01.o ugplin.o \
        ugpm01.o ugpmrk.o ugproj.o ugps01.o ugpu01.o \
        ugpx01.o ugqctr.o ugrerr.o ugsa01.o ugsb01.o ugsc01.o \
        ugscin.o ugsd01.o ugse01.o ugshld.o ugslct.o ugsx01.o \
        ugta01.o ugtd01.o ugtext.o ugtran.o ugts01.o ugtx01.o \
        ugud01.o ugus01.o ugux01.o ugvf01.o ugvi01.o ugvs01.o \
        ugwa01.o ugwb01.o ugwc01.o ugwd01.o ugwdow.o ugwe01.o \
        ugwrit.o ugwz01.o ugxa01.o ugxb01.o ugxc01.o \
        ugxgws.o ugxhch.o ugxs01.o ugxtxt.o ugz001_fix.o \
        ugz002.o ugz003_fix.o ugz004.o ugz005.o old_ugz006.o \
        ugzz01.o libstop_fix.o \
        ugpr01.o ugxerr.o ugxi01.o uga13dum.o kludge.o $(EXTRAS)

FFLAGS := $(FFLAGS) -std=legacy

.f.o:
	$(FF)  $(DEBUG) $(FFLAGS) -c $*.f

.c.o:
	$(CC) $(DEBUG) -c $*.c

td:	$(FLSO) libug.a
	$(FCLD) $(DEBUG) $(FCLDFLAGS) -o td $(FLSO) libug.a $(FCLB)

libug.a:include/ugioparm_fix.for include/ugsyparm_fix.for $(FLIB)
	ar r libug.a $(FLIB)
	ranlib libug.a

clean:
	-rm *.o libug.a *~

tgz:
	tar zcvf td.tgz $(ALLFILES)

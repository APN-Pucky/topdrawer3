      BLOCK DATA B000006
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                THE EXTENDED/SIMPLEX CHARACTER SET                 *
C *                                                                   *
C *  THIS BLOCK OF DATA DEFINES A DUMMY VERSION OF THE EXTENDED/      *
C *  SIMPLEX CHARACTER SET.  IT WILL BE INCORPORATED INTO THE LOAD    *
C *  MODULE IF THE USER DOES NOT INCLUDE THE ACTUAL CHARACTER SET.    *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
c      SAVE          /UGA013/
C  THE DECLARATION OF THE COMMON BLOCK.
      COMMON        /UGA013/
     X              CHRID,CHRTP,
     X              CHRNC
C
C  SYMBOL STROKE TABLE IDENTIFICATION.
      CHARACTER*8   CHRID
C  SYMBOL STROKE TABLE TYPE.
      INTEGER*2     CHRTP
C  NUMBER OF CHARACTERS.
      INTEGER*2     CHRNC
C
      DATA          CHRID/'SIMPLEX '/
      DATA          CHRTP/    3/
      DATA          CHRNC/   -1/
C
      END
      BLOCK DATA B000007
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                 THE EXTENDED/DUPLEX CHARACTER SET                 *
C *                                                                   *
C *  THIS BLOCK OF DATA DEFINES A DUMMY VERSION OF THE EXTENDED/      *
C *  DUPLEX CHARACTER SET.  IT WILL BE INCORPORATED INTO THE LOAD     *
C *  MODULE IF THE USER DOES NOT INCLUDE THE ACTUAL CHARACTER SET.    *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      SAVE          /UGA014/
C  THE DECLARATION OF THE COMMON BLOCK.
      COMMON        /UGA014/
     X              CHRID,CHRTP,
     X              CHRNC
C
C  SYMBOL STROKE TABLE IDENTIFICATION.
      CHARACTER*8   CHRID
C  SYMBOL STROKE TABLE TYPE.
      INTEGER*2     CHRTP
C  NUMBER OF CHARACTERS.
      INTEGER*2     CHRNC
C
      DATA          CHRID/'DUPLEX  '/
      DATA          CHRTP/    4/
      DATA          CHRNC/   -1/
C
      END

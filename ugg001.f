      SUBROUTINE    UGG001(PLAN)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *             THREE-DIMENSIONAL LINE SCISSORING MODULE              *
C *                                                                   *
C *  THIS IS THE FIRST OF FOUR SUBROUTINES THAT MAY BE USED TO        *
C *  SCISSOR THREE-DIMENSIONAL LINE SEGMENTS.  THE LINE SEGMENTS MAY  *
C *  BE SCISSORED AGAINST A PLANE OR A RECTANGULAR PARALLELOPIPED.    *
C *  SUBROUTINE UGG001 IS USED TO INITIALIZE FOR SCISSORING AGAINST   *
C *  A PLANE WHILE UGG002 IS USED TO INITIALIZE FOR SCISSORING        *
C *  AGAINST A RECTANGULAR PARALLELOPIPED.  SUBROUTINE UGG003 IS      *
C *  USED TO SUPPLY A LINE SEGMENT END POINT TO THE SCISSORING        *
C *  MODULE AND SUBROUTINE UGG004 IS USED TO RETRIEVE A LINE SEGMENT  *
C *  END POINTS FROM THE MODULE.  THE NORMAL SEQUENCE OF CALLS IS     *
C *  THE FOLLOWING: (1) UGG001 OR UGG002 IS CALLED TO INITIALIZE      *
C *  PROCESSING, (2) UGG003 IS CALLED TO SUPPLY AN END POINT TO THE   *
C *  MODULES, (3) UGG004 IS CALLED REPEATEDLY TO RETRIEVE END POINTS  *
C *  OF LINES UNTIL IT SIGNALS THAT NO MORE DATA IS AVAILABLE, AND    *
C *  (4) STEP 2 IS REPEATED UNTIL NO MORE INPUT IS AVAILABLE.         *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO INITIALIZE THE SCISSORING OF      *
C *  THREE-DIMENSIONAL LINE SEGMENTS AGAINST A PLANE.                 *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGG001(PLAN)                                              *
C *                                                                   *
C *  THE PARAMETER IN THE CALLING SEQUENCE IS:                        *
C *    PLAN  THE EQUATION OF THE SCISSORING PLANE.  THE POINTS ON     *
C *          THE POSITIVE SIDE OF THE PLANE ARE THE ONES WHICH WILL   *
C *          BE SAVED.                                                *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      REAL          PLAN(4)
C
      INCLUDE        'include/ugg000.for'
C
C  INITIALIZE FOR SCISSORING AGAINST A PLANE.
      ITYP=0
      EQUN(1)=PLAN(1)
      EQUN(2)=PLAN(2)
      EQUN(3)=PLAN(3)
      EQUN(4)=PLAN(4)
      IFLG=0
      NAVL=0
      KAVL=0
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE    UGG002(SLIM)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *             THREE-DIMENSIONAL LINE SCISSORING MODULE              *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO INITIALIZE THE SCISSORING OF      *
C *  THREE-DIMENSIONAL LINE SEGMENTS AGAINST A RECTANGULAR            *
C *  PARALLELOPIPED.                                                  *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGG002(SLIM)                                              *
C *                                                                   *
C *  THE PARAMETER IN THE CALLING SEQUENCE IS:                        *
C *    SLIM  A ARRAY OF DIMENSION 3 BY 2 WHICH CONTAINS THE           *
C *          SCISSORING LIMITS.  SLIM(1,1) IS THE LOW X VALUE,        *
C *          SLIM(2,1) IS THE LOW Y VALUE, SLIM(3,1) IS THE LOW Z     *
C *          VALUE, SLIM(1,2) IS THE HIGH X VALUE, SLIM(2,2) IS THE   *
C *          HIGH Y VALUE, AND SLIM(3,2) IS THE HIGH Z VALUE.         *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      REAL          SLIM(3,2)
C
      INCLUDE        'include/ugg000.for'
C
C  INITIALIZE FOR SCISSORING AGAINST A RECTANGULAR PARALLELOPIPED.
      ITYP=1
      LIMS(1,1)=SLIM(1,1)
      LIMS(1,2)=SLIM(1,2)
      LIMS(2,1)=SLIM(2,1)
      LIMS(2,2)=SLIM(2,2)
      LIMS(3,1)=SLIM(3,1)
      LIMS(3,2)=SLIM(3,2)
      IFLG=0
      NAVL=0
      KAVL=0
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE    UGG003(BBIT,XCRD,YCRD,ZCRD)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *             THREE-DIMENSIONAL LINE SCISSORING MODULE              *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO SUPPLY A POINT TO THE             *
C *  THREE-DIMENSIONAL LINE SCISSORING MODULE.                        *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGG003(BBIT,XCRD,YCRD,ZCRD)                               *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    BBIT  THE BLANKING BIT: 0 MEANS MOVE WITHOUT DRAWING AND 1     *
C *          MEANS DRAW.                                              *
C *    XCRD  THE X COORDINATE OF A LINE END POINT.                    *
C *    YCRD  THE Y COORDINATE OF A LINE END POINT.                    *
C *    ZCRD  THE Z COORDINATE OF A LINE END POINT.                    *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       BBIT
      REAL          XCRD,YCRD,ZCRD
C
      INCLUDE        'include/ugg000.for'
C
C  TEMPORARY X, Y, AND Z COORDINATES.
      REAL          TXCD,TYCD,TZCD
C  BOUNDARY CROSSING FLAGS.
      INTEGER       FLG1,FLG2,FLGT
      REAL          PVL1,PVL2,PVLS
C
C  ACCEPT AN END POINT TO BE SCISSORED.
      KAVL=0
      NAVL=0
      XPNT(BBIT+1)=XCRD
      YPNT(BBIT+1)=YCRD
      ZPNT(BBIT+1)=ZCRD
      IF (BBIT.EQ.0) THEN
        IFLG=0
        GO TO 301
      END IF
C
C  MOVE LINE INTO OUTPUT ARRAY.
      NAVL=0
      XSEG(1)=XPNT(1)
      XSEG(2)=XPNT(2)
      YSEG(1)=YPNT(1)
      YSEG(2)=YPNT(2)
      ZSEG(1)=ZPNT(1)
      ZSEG(2)=ZPNT(2)
C
C  INITIALIZE THE BOUNDARY CROSSING FLAGS.
      IF (ITYP.EQ.0) THEN
        PVL1=EQUN(1)*XSEG(1)+EQUN(2)*YSEG(1)+EQUN(3)*ZSEG(1)+EQUN(4)
        PVL2=EQUN(1)*XSEG(2)+EQUN(2)*YSEG(2)+EQUN(3)*ZSEG(2)+EQUN(4)
      ELSE
        CALL UGG005(LIMS,XSEG(1),YSEG(1),ZSEG(1),FLG1)
        CALL UGG005(LIMS,XSEG(2),YSEG(2),ZSEG(2),FLG2)
      END IF
C
C  NOW DO THE ACTUAL SCISSORING.
      IF (ITYP.EQ.0) THEN
        IF ((PVL1.LT.0.0).OR.(PVL2.LT.0.0)) THEN
          IF ((PVL1.LT.0.0).AND.(PVL2.LT.0.0)) GO TO 201
          PVLS=ABS(PVL1)+ABS(PVL2)
          TXCD=(ABS(PVL2)*XSEG(1)+ABS(PVL1)*XSEG(2))/PVLS
          TYCD=(ABS(PVL2)*YSEG(1)+ABS(PVL1)*YSEG(2))/PVLS
          TZCD=(ABS(PVL2)*ZSEG(1)+ABS(PVL1)*ZSEG(2))/PVLS
          IF (PVL1.LT.0.0) THEN
            XSEG(1)=TXCD
            YSEG(1)=TYCD
            ZSEG(1)=TZCD
          ELSE
            XSEG(2)=TXCD
            YSEG(2)=TYCD
            ZSEG(2)=TZCD
          END IF
        END IF
      ELSE
  101   IF ((FLG1.NE.0).OR.(FLG2.NE.0)) THEN
          IF (IAND(FLG1,FLG2).NE.0) GO TO 201
          IF (FLG1.NE.0) THEN
            FLGT=FLG1
          ELSE
            FLGT=FLG2
          END IF
          IF (IAND(FLGT,1).NE.0) THEN
            TXCD=LIMS(1,1)
            TYCD=((YSEG(2)-YSEG(1))*(TXCD-XSEG(1))/
     X            (XSEG(2)-XSEG(1)))+YSEG(1)
            TZCD=((ZSEG(2)-ZSEG(1))*(TXCD-XSEG(1))/
     X            (XSEG(2)-XSEG(1)))+ZSEG(1)
          ELSE IF (IAND(FLGT,2).NE.0) THEN
            TXCD=LIMS(1,2)
            TYCD=((YSEG(2)-YSEG(1))*(TXCD-XSEG(1))/
     X            (XSEG(2)-XSEG(1)))+YSEG(1)
            TZCD=((ZSEG(2)-ZSEG(1))*(TXCD-XSEG(1))/
     X            (XSEG(2)-XSEG(1)))+ZSEG(1)
          ELSE IF (IAND(FLGT,4).NE.0) THEN
            TYCD=LIMS(2,1)
            TXCD=((XSEG(2)-XSEG(1))*(TYCD-YSEG(1))/
     X            (YSEG(2)-YSEG(1)))+XSEG(1)
            TZCD=((ZSEG(2)-ZSEG(1))*(TYCD-YSEG(1))/
     X            (YSEG(2)-YSEG(1)))+ZSEG(1)
          ELSE IF (IAND(FLGT,8).NE.0) THEN
            TYCD=LIMS(2,2)
            TXCD=((XSEG(2)-XSEG(1))*(TYCD-YSEG(1))/
     X            (YSEG(2)-YSEG(1)))+XSEG(1)
            TZCD=((ZSEG(2)-ZSEG(1))*(TYCD-YSEG(1))/
     X            (YSEG(2)-YSEG(1)))+ZSEG(1)
          ELSE IF (IAND(FLGT,16).NE.0) THEN
            TZCD=LIMS(3,1)
            TXCD=((XSEG(2)-XSEG(1))*(TZCD-ZSEG(1))/
     X            (ZSEG(2)-ZSEG(1)))+XSEG(1)
            TYCD=((YSEG(2)-YSEG(1))*(TZCD-ZSEG(1))/
     X            (ZSEG(2)-ZSEG(1)))+YSEG(1)
          ELSE
            TZCD=LIMS(3,2)
            TXCD=((XSEG(2)-XSEG(1))*(TZCD-ZSEG(1))/
     X            (ZSEG(2)-ZSEG(1)))+XSEG(1)
            TYCD=((YSEG(2)-YSEG(1))*(TZCD-ZSEG(1))/
     X            (ZSEG(2)-ZSEG(1)))+YSEG(1)
          END IF
          CALL UGG005(LIMS,TXCD,TYCD,TZCD,FLGT)
          IF (FLG1.NE.0) THEN
            XSEG(1)=TXCD
            YSEG(1)=TYCD
            ZSEG(1)=TZCD
            FLG1=FLGT
          ELSE
            XSEG(2)=TXCD
            YSEG(2)=TYCD
            ZSEG(2)=TZCD
            FLG2=FLGT
          END IF
          GO TO 101
        END IF
      END IF
      NAVL=2
C
C  SAVE THE ORIGINAL POINT.
  201 XPNT(1)=XPNT(2)
      YPNT(1)=YPNT(2)
      ZPNT(1)=ZPNT(2)
C
C  RETURN TO CALLING SUBROUTINE.
  301 RETURN
C
      END
      SUBROUTINE    UGG004(BBIT,XCRD,YCRD,ZCRD)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *             THREE-DIMENSIONAL LINE SCISSORING MODULE              *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO RETRIEVE A POINT FROM THE         *
C *  THREE-DIMENSIONAL LINE SCISSORING MODULE.                        *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGG004(BBIT,XCRD,YCRD,ZCRD)                               *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    BBIT  THE BLANKING BIT OR TERMINATION FLAG: 0 MEANS MOVE       *
C *          WITHOUT DRAWING, 1 MEANS DRAW, AND -1 MEANS NO MORE      *
C *          DATA IS AVAILABLE.                                       *
C *    XCRD  THE X COORDINATE OF A LINE END POINT.                    *
C *    YCRD  THE Y COORDINATE OF A LINE END POINT.                    *
C *    ZCRD  THE Z COORDINATE OF A LINE END POINT.                    *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       BBIT
      REAL          XCRD,YCRD,ZCRD
C
      INCLUDE        'include/ugg000.for'
C
C  PASS AN END POINT BACK TO THE CALLING MODULE.
  101 IF (KAVL.GE.NAVL) THEN
        BBIT=-1
      ELSE
        BBIT=MOD(KAVL,2)
        KAVL=KAVL+1
        XCRD=XSEG(KAVL)
        YCRD=YSEG(KAVL)
        ZCRD=ZSEG(KAVL)
        IF (IFLG.NE.0) THEN
          IF (BBIT.EQ.0) THEN
            IF (XCRD.EQ.PXCD) THEN
              IF (YCRD.EQ.PYCD) THEN
                IF (ZCRD.EQ.PZCD) GO TO 101
              END IF
            END IF
          END IF
        END IF
        IFLG=1
        PXCD=XCRD
        PYCD=YCRD
        PZCD=ZCRD
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE    UGG005(LIMS,XCRD,YCRD,ZCRD,FLAG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *             THREE-DIMENSIONAL LINE SCISSORING MODULE              *
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO ENCODE THE RELATION OF THE GIVEN      *
C *  POINT WITH THE RECTANGULAR PARALLELOPIPED SCISSORING LIMITS.     *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGG005(LIMS,XCRD,YCRD,ZCRD,FLAG)                          *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    LIMS  AN ARRAY OF CONTAINING THE LIMITS.                       *
C *    XCRD  THE GIVEN X COORDINATE.                                  *
C *    YCRD  THE GIVEN Y COORDINATE.                                  *
C *    ZCRD  THE GIVEN Z COORDINATE.                                  *
C *    FLAG  THE ENCODED FLAG.                                        *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      REAL          LIMS(3,2)
      REAL          XCRD,YCRD,ZCRD
      INTEGER       FLAG
C
C  GENERATE THE FLAG VALUE.
      FLAG=0
      IF (XCRD.LT.LIMS(1,1)) THEN
        FLAG=1
      ELSE IF (XCRD.GT.LIMS(1,2)) THEN
        FLAG=2
      END IF
      IF (YCRD.LT.LIMS(2,1)) THEN
        FLAG=FLAG+4
      ELSE IF (YCRD.GT.LIMS(2,2)) THEN
        FLAG=FLAG+8
      END IF
      IF (ZCRD.LT.LIMS(3,1)) THEN
        FLAG=FLAG+16
      ELSE IF (ZCRD.GT.LIMS(3,2)) THEN
        FLAG=FLAG+32
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END


      SUBROUTINE UGDSAB(OPTN)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                DISABLE AN INTERACTIVE CONTROL UNIT                *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO DISABLE AN INTERACTIVE CONTROL    *
C *  UNIT ON A GRAPHIC DEVICE.                                        *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGDSAB(OPTN)                                              *
C *                                                                   *
C *  THE PARAMETER IN THE CALLING SEQUENCE IS:                        *
C *    OPTN  THE OPTIONS LIST.                                        *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      CHARACTER*(*) OPTN
C
      INCLUDE        'include/ugddacbk.for'
C
      INCLUDE        'include/ugerrcbk.for'
C
      INTEGER*4     INST(36)
      INTEGER*4     EXST(DDAZ1),EXIC(DDAZ1)
      EQUIVALENCE   (EXIC(1),EXST(1))
C
      INTEGER       EFLG
      INTEGER       DDIN(3),DDEX(1)
C
      INTEGER       INT1
C
      DATA          INST/6,1,8,1,1,4HKEYB,4HOARD,
     X                     1,4,2,1,4HPICK,
     X                     1,6,3,1,4HBUTT,4HON  ,
     X                     1,6,4,1,4HSTRO,4HKE  ,
     X                     1,7,5,1,4HLOCA,4HTOR ,
     X                     1,8,6,1,4HVALU,4HATOR/
C
C  IS THERE AN INTERACTIVE ACTIVE DEVICE?
      EFLG=0
      IF (DDAAI.EQ.0) GO TO 401
      IF (DDAIL.NE.3) GO TO 402
C
C  SCAN THE OPTIONS LIST.
      DO 101 INT1=1,DDAZ1
        EXIC(INT1)=0
  101 CONTINUE
      CALL UGOPTN(OPTN,INST,EXST)
C
C  CHECK FOR DISABLE REQUESTS.
      DO 202 INT1=1,DDAZ1
        IF (EXIC(INT1).NE.0) THEN
          IF (DDAIC(INT1).EQ.0) GO TO 403
          IF (DDABC(INT1).NE.0) THEN
            DDABC(INT1)=0
            DDIN(1)=12
            DDIN(2)=0
            DDIN(3)=INT1
            CALL UGZ006(DDAAC,0,0,DDIN,' ',DDEX)
          END IF
        END IF
  201   CONTINUE
  202 CONTINUE
C
C  RESET ERROR INDICATORS AND RETURN TO CALLER.
      IF (EFLG.EQ.0) THEN
        UGELV=0
        UGENM='        '
        UGEIX=0
      END IF
  301 RETURN
C
C  REPORT ERRORS TO THE UNIFIED GRAPHICS SYSTEM ERROR PROCESSOR.
  401 CALL UGRERR(3,'UGDSAB  ',12)
      GO TO 301
  402 CALL UGRERR(2,'UGDSAB  ',13)
      GO TO 301
  403 CALL UGRERR(2,'UGDSAB  ',13)
      EFLG=1
      GO TO 201
C
      END


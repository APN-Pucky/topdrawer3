include td.mak_$(HOSTTYPE)

FLSO =	tdargs.o tdunix.o tdsourcen.o txxug77.o postscr.o \
        postscr1.o ntek4010.o nteksys.o seq4010.o simplex.o duplex.o nucleus.o
FLIB =  ug3lin.o ug3mrk.o ug3pln.o ug3pmk.o ug3trn.o \
        ug3txt.o ug3wrd.o ugcntr.o ugcnvf.o ug2dhg.o ug2dhp.o \
        ugb001.o ugb002.o ugb003.o ugb004.o \
        ugb005.o ugb006.o ugb007.o ugb008.o ugb009.o ugb010.o \
        ugb011.o ugb012.o ugb013.o ugb014.o ugb015.o ugc001.o \
        ugclos.o ugctol.o ugcw01.o ugd001.o \
        ugddat.o ugdefl.o ugdsab.o ugdspc.o uge001.o ugectl.o \
        ugenab.o ugevnt.o ugf001.o ugfont.o ugg001.o uggd01.o \
        uggi01.o uggr01.o uggs01.o ugin01.o uginfo.o uginit.o \
        ugix01.o uglgax.o uglgdx.o ugline.o uglnax.o uglndx.o \
        ugmark.o ugmctl.o ugmesh.o ugmt01.o ugopen.o \
        ugoptn.o ugpfil.o ugpi01.o ugpict.o ugpl01.o ugplin.o \
        ugpm01.o ugpmrk.o ugproj.o ugps01.o ugpu01.o \
        ugpx01.o ugqctr.o ugrerr.o ugsa01.o ugsb01.o ugsc01.o \
        ugscin.o ugsd01.o ugse01.o ugshld.o ugslct.o ugsx01.o \
        ugta01.o ugtd01.o ugtext.o ugtran.o ugts01.o ugtx01.o \
        ugud01.o ugus01.o ugux01.o ugvf01.o ugvi01.o ugvs01.o \
        ugwa01.o ugwb01.o ugwc01.o ugwd01.o ugwdow.o ugwe01.o \
        ugwrit.o ugwz01.o ugxa01.o ugxb01.o ugxc01.o \
        ugxgws.o ugxhch.o ugxs01.o ugxtxt.o ugz001.o \
        ugz002.o ugz003.o ugz004.o ugz005.o old_ugz006.o \
        ugzz01.o libstop.o \
        ugpr01.o ugxerr.o ugxi01.o uga13dum.o kludge.o

.f.o:
	f77 $(FFLAGS) -c $*.f

.c.o:
	cc -c $*.c

libug.a: $(FLIB)
	ar r libug.a $?
	ranlib libug.a

td:	$(FLSO) libug.a
	$(FCLD) -o td $(FLSO) libug.a $(FCLB)

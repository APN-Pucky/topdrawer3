c/*******************  THE UNIFIED GRAPHICS SYSTEM c *******************
c *         SUBROUTINE TO DETERMINE A DATA ELEMENT'S ADDRESS           *
c *                                                                    *
c *  THIS SUBROUTINE MAY BE USED TO DETERMINE THE ADDRESS OF A DATA    *
c *  ELEMENT (NOT A CHARACTER STRING) WITHIN THE CURRENT LOAD          *
c *  MODULE.                                                           *
c *                                                                    *
c *  THE CALLING SEQUENCE IS:                                          *
c *    CALL UGZ005(DATA,DADR)                                          *
c *                                                                    *
c *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                       *
c *    DATA  THE DATA ELEMENT WHOSE ADDRESS IS NEEDED.                 *
c *    DADR  THE ADDRESS OF THE DATA ELEMENT.                          *
c *                                                                    *
c *                          ROBERT C. BEACH                           *
c *                    COMPUTATION RESEARCH GROUP                      *
c *                STANFORD LINEAR ACCELERATOR CENTER                  *
c *                                                                    *
c *********************************************************************/
      subroutine UGZ005(data,addr)
      integer data,addr
      addr = kludg1(data)
      end


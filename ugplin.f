      SUBROUTINE UGPLIN(OPTN,XARY,YARY,NPTS,BBTS,NBTS,SEGM)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *            ADD AN ARRAY OF LINES TO A GRAPHIC SEGMENT             *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO ADD AN ARRAY OF LINES TO A        *
C *  GRAPHIC SEGMENT.  THE LINES MAY BE EITHER BLANKED OR DRAWN.      *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGPLIN(OPTN,XARY,YARY,NPTS,BBTS,NBTS,SEGM)                *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    OPTN  THE OPTIONS LIST.                                        *
C *    XARY  THE X COORDINATES OF THE LINE END POINTS.                *
C *    YARY  THE Y COORDINATES OF THE LINE END POINTS.                *
C *    NPTS  THE NUMBER OF END POINTS IN THE ARRAYS.                  *
C *    BBTS  THE BLANKING BIT ARRAY.                                  *
C *    NBTS  THE NUMBER OF BLANKING BITS IN THE ARRAY.                *
C *    SEGM  THE GRAPHIC SEGMENT WHICH WILL HAVE THE LINE END POINTS  *
C *          ADDED TO IT.                                             *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      CHARACTER*(*) OPTN
      REAL          XARY(*),YARY(*)
      INTEGER       NPTS
      INTEGER*4     BBTS(*)
      INTEGER       NBTS
      INTEGER*4     SEGM(*)
C
      INCLUDE        'include/ugerrcbk.for'
C
      INCLUDE        'include/ugpotcbk.for'
C
      INCLUDE        'include/ugpotdcl.for'
C
C  LENGTH OF MODE BLOCK FOR LINE DATA.
      INTEGER       NMOD
      PARAMETER     (NMOD=7)
C
      INTEGER*4     MODE(NMOD)
      INTEGER       RTRY,IBBT,IBTS,KBTS
      INTEGER*4     BITS(32)
      INTEGER*4     MSK1,MSK2
      LOGICAL       BTFG
C
      INTEGER*4     OVI4
      REAL*4        OVR4
      EQUIVALENCE   (OVI4,OVR4)
      INTEGER       INT1,INT2,INT3,INT4
C
c      DATA          BITS/Z80000000,Z40000000,Z20000000,Z10000000,
c     X                   Z08000000,Z04000000,Z02000000,Z01000000,
c     X                   Z00800000,Z00400000,Z00200000,Z00100000,
c     X                   Z00080000,Z00040000,Z00020000,Z00010000,
c     X                   Z00008000,Z00004000,Z00002000,Z00001000,
c     X                   Z00000800,Z00000400,Z00000200,Z00000100,
c     X                   Z00000080,Z00000040,Z00000020,Z00000010,
c     X                   Z00000008,Z00000004,Z00000002,Z00000001/
c      DATA          MSK1/Z00010000/
c      DATA          MSK2/ZFFFEFFFF/
      DATA BITS/
     X -2147483648,  1073741824,   536870912,   268435456,
     X   134217728,    67108864,    33554432,    16777216,
     X     8388608,     4194304,     2097152,     1048576,
     X      524288,      262144,      131072,       65536,
     X       32768,       16384,        8192,        4096,
     X        2048,        1024,         512,         256,
     X         128,          64,          32,          16,
     X           8,           4,           2,           1
     X /
      include 'mskdata.f'
C
C  SCAN THE OPTIONS LIST.
      EXIL=POTIL
      EXCR=POTCR
      EXBL=POTBL
      EXPI=POTPI
      EXLS=POTLS
      CALL UGOPTN(OPTN,POTST,EXST)
C
C  CONSTRUCT THE MODE SPECIFICATION.
      IF (NPTS.LT.1) GO TO 201
      MODE(1)=2
      MODE(2)=NMOD+2*NPTS
      MODE(3)=EXIL
      MODE(4)=EXCR
      MODE(5)=EXBL
      MODE(6)=EXPI
      MODE(7)=EXLS
C
C  INSERT THE MODE SPECIFICATION AND THE DATA INTO THE SEGMENT.
  101 RTRY=SEGM(1)
      CALL UGB002(MODE,NMOD,2*NPTS,SEGM,INT1)
      IF (INT1.EQ.0) GO TO 301
      IBTS=NBTS
      BTFG=.TRUE.
      IF (IBTS.LE.0) THEN
        IBTS=-IBTS
        BTFG=.FALSE.
      END IF
      KBTS=0
      DO 102 INT2=1,NPTS
        OVR4=XARY(INT2)
        SEGM(INT1)=OVI4
        OVR4=YARY(INT2)
        SEGM(INT1+1)=OVI4
        IBBT=0
        IF (KBTS.NE.0) THEN
          IF (BTFG) THEN
            IF (BBTS(KBTS).NE.0) IBBT=MSK1
          ELSE
            INT3=1+(KBTS-1)/32
            INT4=MOD(KBTS-1,32)+1
            IF (IAND(BBTS(INT3),BITS(INT4)).NE.0) IBBT=MSK1
          END IF
        END IF
        SEGM(INT1+1)=IOR(IAND(SEGM(INT1+1),MSK2),IBBT)
        IF (KBTS.GE.IBTS) THEN
          KBTS=1
        ELSE
          KBTS=KBTS+1
        END IF
        INT1=INT1+2
  102 CONTINUE
C
C  RESET ERROR INDICATORS AND RETURN TO CALLER.
  201 UGELV=0
      UGENM='        '
      UGEIX=0
  202 RETURN
C
C  REPORT ERRORS TO THE UNIFIED GRAPHICS SYSTEM ERROR PROCESSOR.
  301 CALL UGRERR(2,'UGPLIN  ',11)
      IF (SEGM(1).NE.RTRY) GO TO 101
      GO TO 202
C
      END


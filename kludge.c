/* This is a file written by Mike Seymour to make the pointer referencing/
   dereferencing of the U.G. system more machine independent - the previous
   version assumed that a pointer can fit in a fortran INTEGER, which it
   can't on alpha machines for example.

   Usage is:

   int kludg1_(void* ptr);
       Returns an integer which uniquely identifies the address in ptr.
       If it runs out of room, it returns zero.
   void *kludg2_(int* iptr);
       Returns the pointer which is identified by the integer
       associated with iptr.
       If it is given an integer which was not allocated by kludg1,
       it returns the null pointer.

   Therefore the fortran code
      INTEGER A,B
      A=%LOC(B)
   can be replaced by
      A=KLUDG1(B)
   and the C code
      int *a,*b;
      b=*a;
   by
      b=kludg2_(a);

   Note that kludg1_ assumes that not many different pointers are needed
   (which is the case for U.G.), and could become very inefficient if it
   is given too many pointers (ie if NMAX is increased much), while
   kludg2_ is always efficient.
*/
#include <stdio.h>


#define NMAX 100

void *ptrlist[NMAX];
int   ptrnum = 0;

int kludg1_(ptr)
     void* ptr;
{
  int i;

/* If that address has already been stored, return its label */
  for (i = 0 ; i<ptrnum ; i++)
    if (ptr == ptrlist[i])
      return i+1;

/* If we've run out of room, warn and return zero */
  if (ptrnum == NMAX) {
    printf("%s%s","warning: kludg1 asked for more room than it has.\n",
                  "         increase NMAX and recompile.\n");
    return 0;
  }

/* If not, store it and return the new label */
  ptrlist[ptrnum] = ptr;
  return ++ptrnum;
}

void *kludg2_(iptr)
     int* iptr;
{

/* If kludg1 did not store the int, return the null pointer */
  if (*iptr <= 0  ||  *iptr > ptrnum) {
    printf("%s%s","warning: kludg2 asked to dereference a pointer which",
                  " kludg1 did not allocate.\n");
    return 0;
  }

  return ptrlist[*iptr-1];
}

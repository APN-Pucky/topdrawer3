      SUBROUTINE UGCNTR(OPTN,LSUB,TSUB,ARAY,MDIM,NDIM,
     X                  LOCR,HICR,NCTR,WKAR,LDIM)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                      CONTOUR PLOT SUBROUTINE                      *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO GENERATE A DESCRIPTION OF A       *
C *  CONTOUR PLOT.  A PAIR OF USER SUPPLIED SUBROUTINES ARE CALLED    *
C *  TO PROCESS THE LINE SEGMENT END POINTS AND THE TEXT.  THE        *
C *  ALGORITHM USED HERE PRODUCES THE CONTOURS AS CONCATENATED LINE   *
C *  SEGMENTS.                                                        *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGCNTR(OPTN,LSUB,TSUB,ARAY,MDIM,NDIM,                     *
C *                LOCR,HICR,NCTR,WKAR,LDIM)                          *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    OPTN  THE OPTIONS LIST.                                        *
C *    LSUB  THE LINE SEGMENT END POINT SUBROUTINE.                   *
C *    TSUB  THE LABEL SUBROUTINE.                                    *
C *    ARAY  THE ARRAY DEFINING THE CONTOUR DATA.                     *
C *    MDIM  THE EXTENT OF THE CONTOUR DATA IN THE X DIRECTION.       *
C *    NDIM  THE EXTENT OF THE CONTOUR DATA IN THE Y DIRECTION.       *
C *    LOCR  THE SMALLEST PRIMARY CONTOUR VALUE.                      *
C *    HICR  THE HIGHEST PRIMARY CONTOUR VALUE.                       *
C *    NCTR  THE NUMBER OF CONTOUR VALUES.                            *
C *    WKAR  A WORK ARRAY.                                            *
C *    LDIM  THE NUMBER OF WORDS IN WKAR.                             *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      CHARACTER*(*) OPTN
      EXTERNAL      LSUB,TSUB
      REAL          ARAY(MDIM,NDIM)
      INTEGER       MDIM,NDIM
      REAL          LOCR,HICR
      INTEGER       NCTR
      INTEGER*4     WKAR(*)
      INTEGER       LDIM
C
      INCLUDE        'include/ugerrcbk.for'
C
      INTEGER*4     INST(12)
      INTEGER*4     EXST(2),EXSC
      REAL*4        EXTL
      EQUIVALENCE   (EXSC,EXST(1)),      (EXTL,EXST(2))
C
      INTEGER       KDIM
      INTEGER       NCNT,ICNT,MCNT,IROW,ICOL
      REAL          ZCNT
C
      INTEGER       INT1
C
      DATA          INST/2,2,4,1,0,4HNSCL,
     X                     3,5,2,0,4HTOLE,4HR   /
C
C  SCAN THE OPTIONS LIST.
      EXSC=0
      EXTL=0.0001
      CALL UGOPTN(OPTN,INST,EXST)
C
C  INITIALIZE CONTOUR GENERATION.
      IF ((MDIM.LT.3).OR.(NDIM.LT.3)) GO TO 301
      KDIM=((MDIM-2)*(NDIM-1)+(NDIM-2)+15)/15
      IF (LDIM.LT.KDIM) GO TO 302
      INT1=MAX(NCTR,2)
      NCNT=INT1+(INT1-1)*EXSC
C
C  LOOP FOR EACH CONTOUR.
      DO 106 ICNT=1,NCNT
        ZCNT=LOCR+REAL(ICNT-1)*(HICR-LOCR)/REAL(NCNT-1)
        MCNT=MOD(ICNT-1,EXSC+1)
C  CLEAR SEGMENT BIT MAP.
        DO 101 INT1=1,KDIM
          WKAR(INT1)=0
  101   CONTINUE
C  PROCESS LOWER AND UPPER BOUNDARY.
        DO 102 ICOL=3,NDIM
          CALL UGCNT1(LSUB,TSUB,1,3,ICOL,ARAY,MDIM,NDIM,
     X                ZCNT,WKAR,1,MCNT,EXTL,INT1)
          IF (INT1.NE.0) GO TO 303
          CALL UGCNT1(LSUB,TSUB,3,MDIM,ICOL,ARAY,MDIM,NDIM,
     X                ZCNT,WKAR,1,MCNT,EXTL,INT1)
          IF (INT1.NE.0) GO TO 303
  102   CONTINUE
C  PROCESS LEFT AND RIGHT BOUNDARY.
        DO 103 IROW=3,MDIM
          CALL UGCNT1(LSUB,TSUB,0,IROW,3,ARAY,MDIM,NDIM,
     X                ZCNT,WKAR,1,MCNT,EXTL,INT1)
          IF (INT1.NE.0) GO TO 303
          CALL UGCNT1(LSUB,TSUB,2,IROW,NDIM,ARAY,MDIM,NDIM,
     X                ZCNT,WKAR,1,MCNT,EXTL,INT1)
          IF (INT1.NE.0) GO TO 303
  103   CONTINUE
C  PROCESS INTERIOR SIDES OF SURFACE PATCHES.
        IF (NDIM.GE.4) THEN
          DO 105 ICOL=4,NDIM
            DO 104 IROW=3,MDIM
              CALL UGCNT1(LSUB,TSUB,0,IROW,ICOL,ARAY,MDIM,NDIM,
     X                    ZCNT,WKAR,0,MCNT,EXTL,INT1)
              IF (INT1.NE.0) GO TO 303
  104       CONTINUE
  105     CONTINUE
        END IF
  106 CONTINUE
C
C  RESET ERROR INDICATORS AND RETURN TO CALLER.
      UGELV=0
      UGENM='        '
      UGEIX=0
  201 RETURN
C
C  REPORT ERRORS TO THE UNIFIED GRAPHICS SYSTEM ERROR PROCESSOR.
  301 CALL UGRERR(3,'UGCNTR  ',1)
      GO TO 201
  302 CALL UGRERR(3,'UGCNTR  ',2)
      GO TO 201
  303 CALL UGRERR(4,'UGCNTR  ',3)
      GO TO 201
C
      END
      SUBROUTINE UGCNT1(LSUB,TSUB,ISID,IROW,ICOL,ARAY,MDIM,NDIM,
     X                  ZCNT,WKAR,BDFG,MCNT,EXTL,EFLG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                  PROCESS A SIDE OF A SURFACE PATCH                *
C *                                                                   *
C *  THIS SUBROUTINE IS USED BY UGCNTR TO PROCESS A SIDE OF A         *
C *  SURFACE PATCH.  IF THE SIDE HAS NOT BEEN CHECKED BEFORE, THEN    *
C *  THE CONTOUR IS EXAMINED TO SEE IF IT CROSSES THE SIDE.  IF IT    *
C *  DOES NOT, THE SIDE IS MARKED AS HAVING BEEN CHECKED.  IF THE     *
C *  CONTOUR CROSSES THE SIDE, THE CONTOUR IS FOLLOWED UNTIL IT IS    *
C *  COMPLETE AND ALL AFFECTED SIDES ARE MARKED AS HAVING BEEN        *
C *  CHECKED.                                                         *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGCNT1(LSUB,TSUB,ISID,IROW,ICOL,ARAY,MDIM,NDIM,           *
C *                ZCNT,WKAR,BDFG,MCNT,EXTL,EFLG)                     *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    LSUB  THE LINE SEGMENT END POINT SUBROUTINE.                   *
C *    TSUB  THE LABEL SUBROUTINE.                                    *
C *    ISID  INDEX OF THE SIDE BEING PROCESSED.                       *
C *    IROW  INDEX OF THE ROW BEING PROCESSED.                        *
C *    ICOL  INDEX OF THE COLUMN BEING PROCESSED.                     *
C *    ARRY  THE ARRAY DEFINING THE CONTOUR DATA.                     *
C *    MDIM  THE EXTENT OF THE CONTOUR DATA IN THE X DIRECTION.       *
C *    NDIM  THE EXTENT OF THE CONTOUR DATA IN THE Y DIRECTION.       *
C *    ZCNT  VALUE OF THE CONTOUR LINE.                               *
C *    WKAR  A WORK ARRAY.                                            *
C *    BDFG  A FLAG INDICATING IF A BOUNDARY IS BEING PROCESSED.      *
C *    MCNT  A FLAG INDICATING IF A PRIMARY CONTOUR IS BEING          *
C *          PROCESSED.                                               *
C *    EXTL  A TOLERANCE.                                             *
C *    EFLG  AN ERROR FLAG.                                           *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      EXTERNAL      LSUB,TSUB
      INTEGER       ISID,IROW,ICOL
      REAL          ARAY(MDIM,NDIM)
      INTEGER       MDIM,NDIM
      REAL          ZCNT
      INTEGER*4     WKAR(*)
      INTEGER       BDFG,MCNT
      REAL*4        EXTL
      INTEGER       EFLG
C
      INTEGER       JROW,JCOL,JSID
      REAL          XPNT,YPNT
      INTEGER       INFG,MKFG
C
C  INITIALIZE THE INTERNAL COUNTER.
      EFLG=0
      JROW=IROW
      JCOL=ICOL
      JSID=ISID
C  DO ANY CONTOURS BEGIN AT THIS SIDE?
      CALL UGCNT3(1,JSID,JROW,JCOL,NDIM,WKAR,MKFG)
      IF (MKFG.EQ.1) GO TO 201
      CALL UGCNT2(JSID,JROW,JCOL,ZCNT,XPNT,YPNT,ARAY,MDIM,NDIM,INFG)
      IF (INFG.NE.0) THEN
C  START DRAWING THE CONTOUR CURVE.
        IF ((BDFG.NE.0).AND.(MCNT.EQ.0)) CALL TSUB(XPNT,YPNT,ZCNT,JSID)
        CALL UGCNT4(LSUB,XPNT,YPNT,0,MCNT,EXTL)
C  FIND THE OTHER SIDE OF THE PATCH.
  101   JSID=MOD(JSID+2,4)
        CALL UGCNT3(1,JSID,JROW,JCOL,NDIM,WKAR,MKFG)
        IF (MKFG.NE.1) THEN
          CALL UGCNT2(JSID,JROW,JCOL,ZCNT,XPNT,YPNT,ARAY,MDIM,NDIM,INFG)
          IF (INFG.NE.0) GO TO 102
          CALL UGCNT3(0,JSID,JROW,JCOL,NDIM,WKAR,MKFG)
        END IF
        JSID=MOD(JSID+1,4)
        CALL UGCNT3(1,JSID,JROW,JCOL,NDIM,WKAR,MKFG)
        IF (MKFG.NE.1) THEN
          CALL UGCNT2(JSID,JROW,JCOL,ZCNT,XPNT,YPNT,ARAY,MDIM,NDIM,INFG)
          IF (INFG.NE.0) GO TO 102
          CALL UGCNT3(0,JSID,JROW,JCOL,NDIM,WKAR,MKFG)
        END IF
        JSID=MOD(JSID+2,4)
        CALL UGCNT3(1,JSID,JROW,JCOL,NDIM,WKAR,MKFG)
        IF (MKFG.EQ.1) GO TO 202
        CALL UGCNT2(JSID,JROW,JCOL,ZCNT,XPNT,YPNT,ARAY,MDIM,NDIM,INFG)
        IF (INFG.EQ.0) GO TO 202
C  DRAW CURRENT PART OF THE CONTOUR.
  102   CALL UGCNT4(LSUB,XPNT,YPNT,1,MCNT,EXTL)
C  MARK THE LINE PROCESSED.
        CALL UGCNT3(0,JSID,JROW,JCOL,NDIM,WKAR,MKFG)
C  FIND THE ADJACENT SURFACE PATCH.
        IF (JSID.EQ.3) THEN
          IF (JROW.GE.MDIM) GO TO 103
          JROW=JROW+1
          JSID=1
        ELSE IF (JSID.EQ.2) THEN
          IF (JCOL.GE.NDIM) GO TO 103
          JCOL=JCOL+1
          JSID=0
        ELSE IF (JSID.EQ.1) THEN
          IF (JROW.LE.3) GO TO 103
          JROW=JROW-1
          JSID=3
        ELSE
          IF (JCOL.LE.3) GO TO 103
          JCOL=JCOL-1
          JSID=2
        END IF
C  CHECK FOR CLOSURE OF THE CONTOUR LINE.
        IF ((JROW.EQ.IROW).AND.(JCOL.EQ.ICOL).AND.(JSID.EQ.ISID))
     X    GO TO 201
        GO TO 101
C  FINISH AN OPEN CURVE.
  103   IF ((BDFG.NE.0).AND.(MCNT.EQ.0)) CALL TSUB(XPNT,YPNT,ZCNT,JSID)
        JROW=IROW
        JCOL=ICOL
        JSID=ISID
      END IF
      CALL UGCNT3(0,JSID,JROW,JCOL,NDIM,WKAR,MKFG)
C
C  RETURN TO CALLING SUBROUTINE.
  201 RETURN
C  SET ERROR FLAG.
  202 EFLG=1
      GO TO 201
C
      END
      SUBROUTINE UGCNT2(ISID,IROW,ICOL,ZCNT,XCRD,YCRD,
     X                  ARAY,MDIM,NDIM,OFLG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                DETERMINE CONTOUR-SIDE INTERSECTION                *
C *                                                                   *
C *  THIS SUBROUTINE IS USED BY UGCNTR TO DETERMINE IF A SIDE IS      *
C *  INTERSECTED BY A CONTOUR LINE.                                   *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGCNT2(ISID,IROW,ICOL,ZCNT,XCRD,YCRD,                     *
C *                ARAY,MDIM,NDIM,OFLG)                               *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    ISID  INDEX OF THE SIDE BEING PROCESSED.                       *
C *    IROW  INDEX OF THE ROW BEING PROCESSED.                        *
C *    ICOL  INDEX OF THE COLUMN BEING PROCESSED.                     *
C *    ZCNT  VALUE OF THE CONTOUR LINE.                               *
C *    XCRD  X COORDINATE OF THE INTERSECTION.                        *
C *    YCRD  Y COORDINATE OF THE INTERSECTION.                        *
C *    ARAY  THE ARRAY DEFINING THE CONTOUR DATA.                     *
C *    MDIM  THE EXTENT OF THE CONTOUR DATA IN THE X DIRECTION.       *
C *    NDIM  THE EXTENT OF THE CONTOUR DATA IN THE Y DIRECTION.       *
C *    OFLG  AN OUTPUT FLAG INDICATING IF AN INTERSECTION WAS FOUND.  *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       ISID,IROW,ICOL
      REAL          ZCNT
      REAL          XCRD,YCRD
      REAL          ARAY(MDIM,NDIM)
      INTEGER       MDIM,NDIM
      INTEGER       OFLG
C
      REAL          IVR1,IVR2,DVR1,DVR2
      LOGICAL       VFLG
C
      REAL          FLT1
C
C  OBTAIN THE INDEPENDENT AND DEPENDENT VARIABLES.
      IF (ISID.EQ.0) THEN
        DVR1=ARAY(IROW-1,ICOL-1)
        DVR2=ARAY(IROW,ICOL-1)
        XCRD=ARAY(1,ICOL-1)
        VFLG=.TRUE.
      ELSE IF (ISID.EQ.1) THEN
        DVR1=ARAY(IROW-1,ICOL-1)
        DVR2=ARAY(IROW-1,ICOL)
        YCRD=ARAY(IROW-1,1)
        VFLG=.FALSE.
      ELSE IF (ISID.EQ.2) THEN
        DVR1=ARAY(IROW-1,ICOL)
        DVR2=ARAY(IROW,ICOL)
        XCRD=ARAY(1,ICOL)
        VFLG=.TRUE.
      ELSE
        DVR1=ARAY(IROW,ICOL-1)
        DVR2=ARAY(IROW,ICOL)
        YCRD=ARAY(IROW,1)
        VFLG=.FALSE.
      END IF
      IF (VFLG) THEN
        IVR1=ARAY(IROW-1,1)
        IVR2=ARAY(IROW,1)
      ELSE
        IVR1=ARAY(1,ICOL-1)
        IVR2=ARAY(1,ICOL)
      END IF
C
C  CHECK FOR AN INTERSECTION.
      IF (((DVR1.LT.ZCNT).AND.(DVR2.LT.ZCNT)).OR.
     X  ((DVR1.GE.ZCNT).AND.(DVR2.GE.ZCNT))) GO TO 101
C  COMPUTE THE OTHER COORDINATE.
      FLT1=IVR1+(ZCNT-DVR1)*(IVR2-IVR1)/(DVR2-DVR1)
      IF (VFLG) THEN
        YCRD=FLT1
      ELSE
        XCRD=FLT1
      END IF
C
C  RETURN WITH INTERSECTION.
      OFLG=1
      GO TO 102
C  RETURN WITHOUT INTERSECTION.
  101 OFLG=0
  102 RETURN
C
      END
      SUBROUTINE UGCNT3(FLAG,ISID,IROW,ICOL,NDIM,WKAR,OFLG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                   MARK/CHECK SIDES AS PROCESSED                   *
C *                                                                   *
C *  THIS SUBROUTINE IS USED BY UGCNTR TO MARK A GIVEN SIDE AS        *
C *  PROCESSED OR CHECK IF THE SIDE HAS BEEN PROCESSED.               *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGCNT3(FLAG,ISID,IROW,ICOL,NDIM,WKAR,OFLG)                *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    FLAG  0 MEANS MARK, 1 MEANS CHECK.                             *
C *    ISID  INDEX OF THE SIDE BEING PROCESSED.                       *
C *    IROW  INDEX OF THE ROW BEING PROCESSED.                        *
C *    ICOL  INDEX OF THE COLUMN BEING PROCESSED.                     *
C *    NDIM  THE EXTENT OF THE CONTOUR DATA IN THE Y DIRECTION.       *
C *    WKAR  A WORK ARRAY.                                            *
C *    OFLG  A RETURN VALUE INDICATING IF THE SIDE HAS BEEN           *
C *          PROCESSED.                                               *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       FLAG,ISID,IROW,ICOL,NDIM
      INTEGER*4     WKAR(*)
      INTEGER       OFLG
C
      INTEGER       KROW,KCOL,KSID
      INTEGER*4     PWRS(31)
      INTEGER       XMNB,XMNW,XMNO,XMWD,XMLP,XMUP,XMP0,XMP1
C
c      DATA          PWRS/Z00000001,Z00000002,Z00000004,Z00000008,
c     X                   Z00000010,Z00000020,Z00000040,Z00000080,
c     X                   Z00000100,Z00000200,Z00000400,Z00000800,
c     X                   Z00001000,Z00002000,Z00004000,Z00008000,
c     X                   Z00010000,Z00020000,Z00040000,Z00080000,
c     X                   Z00100000,Z00200000,Z00400000,Z00800000,
c     X                   Z01000000,Z02000000,Z04000000,Z08000000,
c     X                   Z10000000,Z20000000,Z40000000/
      DATA PWRS/
     X           1,           2,           4,           8,
     X          16,          32,          64,         128,
     X         256,         512,        1024,        2048,
     X        4096,        8192,       16384,       32768,
     X       65536,      131072,      262144,      524288,
     X     1048576,     2097152,     4194304,     8388608,
     X    16777216,    33554432,    67108864,   134217728,
     X   268435456,   536870912,  1073741824
     X /
C
C  PERFORM THE REQUIRED OPERATION.
      OFLG=FLAG
      KROW=IROW
      KCOL=ICOL
      KSID=ISID
      IF (KSID.EQ.2) THEN
        KSID=0
        KCOL=KCOL+1
      ELSE IF (KSID.EQ.3) THEN
        KSID=1
        KROW=KROW+1
      END IF
      XMNB=2*((KROW-3)*(NDIM-1)+(KCOL-3))+KSID
      XMNW=1+XMNB/30
      XMNO=1+MOD(XMNB,30)
      XMP0=PWRS(XMNO)
      XMWD=WKAR(XMNW)
      IF (OFLG.NE.0) THEN
        XMUP=XMWD/XMP0
        IF (MOD(XMUP,2).EQ.0) OFLG=0
      ELSE
        XMP1=PWRS(XMNO+1)
        XMLP=MOD(XMWD,XMP0)
        XMUP=XMWD/XMP1
        WKAR(XMNW)=XMUP*XMP1+XMP0+XMLP
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE UGCNT4(LSUB,XCRD,YCRD,BBIT,CFLG,TOLR)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                 DRAW A LINE TO THE CURRENT POINT                  *
C *                                                                   *
C *  THIS SUBROUTINE IS USED BY UGCNTR TO DRAW A LINE TO THE CURRENT  *
C *  POSITION.                                                        *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGCNT4(LSUB,XCRD,YCRD,BBIT,CFLG,TOLR)                     *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    LSUB  THE LINE SEGMENT END POINT SUBROUTINE.                   *
C *    XCRD  X COORDINATE OF THE CURRENT POINT.                       *
C *    YCRD  Y COORDINATE OF THE CURRENT POINT.                       *
C *    BBIT  THE BLANKING BIT.                                        *
C *    CFLG  A FLAG INDICATING A PRIMARY OR SECONDARY CONTOUR.        *
C *    TOLR  A TOLERANCE TO DETECT CONCURRENT POINTS.                 *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      EXTERNAL      LSUB
      REAL          XCRD,YCRD
      INTEGER       BBIT,CFLG
      REAL          TOLR
C
      SAVE          XSAV,YSAV
      REAL          XSAV,YSAV
C
C  DRAW TO THE GIVEN POINT.
      IF (BBIT.NE.0) THEN
        IF ((ABS(XCRD-XSAV)+ABS(YCRD-YSAV)).LT.TOLR) GO TO 101
      END IF
      IF (CFLG.EQ.0) THEN
        CALL LSUB(XCRD,YCRD,BBIT)
      ELSE
        CALL LSUB(XCRD,YCRD,BBIT+2)
      END IF
      XSAV=XCRD
      YSAV=YCRD
C
C  RETURN TO CALLING SUBROUTINE.
  101 RETURN
C
      END


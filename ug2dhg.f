      SUBROUTINE UG2DHG(OPTN,LSUB,ARAY,MDIM,NDIM,TRNS,WKAR,LDIM)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                2-D HISTOGRAM GENERATION SUBROUTINE                *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO GENERATE A DESCRIPTION OF A 2-D   *
C *  HISTOGRAM.  A USER SUPPLIED SUBROUTINE IS CALLED TO PROCESS THE  *
C *  LINE SEGMENT END POINTS.                                         *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UG2DHG(OPTN,LSUB,ARAY,MDIM,NDIM,TRNS,WKAR,LDIM)           *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    OPTN  THE OPTIONS LIST.                                        *
C *    LSUB  THE LINE SEGMENT END POINT SUBROUTINE.                   *
C *    ARAY  THE ARRAY DEFINING THE 2-D HISTOGRAM.                    *
C *    MDIM  THE EXTENT OF THE 2-D HISTOGRAM IN THE X DIRECTION.      *
C *    NDIM  THE EXTENT OF THE 2-D HISTOGRAM IN THE Y DIRECTION.      *
C *    TRNS  THE PROJECTION TRANSFORMATION.                           *
C *    WKAR  A WORK AREA.                                             *
C *    LDIM  THE NUMBER OF WORDS IN WKAR.                             *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      CHARACTER*(*) OPTN
      EXTERNAL      LSUB
      REAL          ARAY(MDIM,NDIM)
      INTEGER       MDIM,NDIM
      REAL          TRNS(31)
      REAL*4        WKAR(*)
      INTEGER       LDIM
C
      INCLUDE        'include/ugerrcbk.for'
C
      INTEGER*4     INST(7)
      INTEGER*4     EXST(1)
      REAL*4        EXTL
      EQUIVALENCE   (EXTL,EXST(1))
C
      REAL*4        HFB1
      INTEGER*2     HFP1(2)
      EQUIVALENCE   (HFP1(1),HFB1)
      INTEGER       HFCU,HFFL
C
      REAL          TOLR
      REAL          PNTX(2)
      LOGICAL       XLFG,XHFG,YLFG,YHFG
      INTEGER       V1FG
      INTEGER       V1LO,V1HI,V1DL,V2LO,V2HI,V2DL
      INTEGER       V1IX,V2IX
      REAL          PNT3(3)
      REAL          PT2A(2),PT2B(2),PT2C(2),PT2D(2),
     X              PT2E(2),PT2F(2),PT2G(2),PT2H(2),
     X              PT2I(2),PT2J(2),PT2K(2),PT2L(2)
      REAL          V1CL,V1FR,V2MN,V2MX,V3HI
      LOGICAL       VHFG,VLFG,LSFG
      LOGICAL       L1FG,L2FG,L3FG,L4FG
C
      LOGICAL       LGL1
      INTEGER       INT1,INT2
C
      DATA          INST/1,3,5,1,0,4HTOLE,4HR   /
C
C  SCAN THE OPTIONS LIST, INITIALIZE, AND CHECK THE INPUT..
      EXTL=0.00005
      CALL UGOPTN(OPTN,INST,EXST)
      TOLR=EXTL
      PNTX(1)=1E10
      PNTX(2)=1E10
      IF ((MDIM.LT.3).OR.(NDIM.LT.3)) GO TO 401
      IF ((TRNS(9).NE.0.0).OR.(TRNS(10).NE.0.0).OR.
     X    (TRNS(11).NE.0.0).OR.(TRNS(12).EQ.0.0)) GO TO 403
C
C  INITIALIZE THE HEIGHT FUNCTION.
      INT1=3*(MIN(LDIM,32767)/3)-2
      IF (INT1.LT.7) GO TO 402
      HFP1(1)=4
      HFP1(2)=0
      WKAR(1)=HFB1
      WKAR(2)=-1E10
      WKAR(3)=-1E10
      HFP1(1)=0
      HFP1(2)=1
      WKAR(4)=HFB1
      WKAR(5)=1E10
      WKAR(6)=-1E10
      DO 101 INT2=7,INT1,3
        IF (INT2.EQ.INT1) THEN
          HFP1(1)=0
        ELSE
          HFP1(1)=INT2+3
        END IF
        WKAR(INT2)=HFB1
  101 CONTINUE
      HFCU=1
      HFFL=7
C
C  COMPUTE THE NORMALIZED VIEW OF THE 2-D HISTOGRAM.
      XLFG=.FALSE.
      XHFG=.FALSE.
      YLFG=.FALSE.
      YHFG=.FALSE.
      IF (ABS(TRNS(22)).GT.ABS(TRNS(23))) THEN
        V1FG=1
        IF (TRNS(22).GT.0.0) THEN
          XLFG=.TRUE.
          V1LO=2
          V1HI=NDIM-1
          V1DL=1
        ELSE
          XHFG=.TRUE.
          V1LO=NDIM
          V1HI=3
          V1DL=-1
        END IF
        IF (TRNS(23).GT.0.0) THEN
          YLFG=.TRUE.
          V2LO=2
          V2HI=MDIM-1
          V2DL=1
        ELSE
          IF (TRNS(23).LT.0.0) YHFG=.TRUE.
          V2LO=MDIM
          V2HI=3
          V2DL=-1
        END IF
      ELSE
        V1FG=2
        IF (TRNS(23).GT.0.0) THEN
          YLFG=.TRUE.
          V1LO=2
          V1HI=MDIM-1
          V1DL=1
        ELSE
          YHFG=.TRUE.
          V1LO=MDIM
          V1HI=3
          V1DL=-1
        END IF
        IF (TRNS(22).GT.0.0) THEN
          XLFG=.TRUE.
          V2LO=2
          V2HI=NDIM-1
          V2DL=1
        ELSE
          IF (TRNS(22).LT.0.0) XHFG=.TRUE.
          V2LO=NDIM
          V2HI=3
          V2DL=-1
        END IF
      END IF
C
C  DRAW THE VISIBLE BASE OF THE COLUMNS.
      PNT3(3)=ARAY(1,1)
      IF (XLFG.OR.YLFG) THEN
        PNT3(1)=ARAY(1,2)
        PNT3(2)=ARAY(2,1)
        CALL UGPROJ(TRNS,PNT3,PT2A)
      END IF
      IF (YLFG.OR.XHFG) THEN
        PNT3(1)=ARAY(1,NDIM)
        PNT3(2)=ARAY(2,1)
        CALL UGPROJ(TRNS,PNT3,PT2B)
      END IF
      IF (XHFG.OR.YHFG) THEN
        PNT3(1)=ARAY(1,NDIM)
        PNT3(2)=ARAY(MDIM,1)
        CALL UGPROJ(TRNS,PNT3,PT2C)
      END IF
      IF (YHFG.OR.XLFG) THEN
        PNT3(1)=ARAY(1,2)
        PNT3(2)=ARAY(MDIM,1)
        CALL UGPROJ(TRNS,PNT3,PT2D)
      END IF
      IF (YLFG) THEN
        CALL UG2DH2(LSUB,PT2A,PT2B,PNTX)
        CALL UG2DH4(PT2A,PT2B,TOLR,WKAR,HFCU,HFFL,LGL1)
        IF (LGL1) GO TO 402
      END IF
      IF (XHFG) THEN
        CALL UG2DH2(LSUB,PT2B,PT2C,PNTX)
        CALL UG2DH4(PT2B,PT2C,TOLR,WKAR,HFCU,HFFL,LGL1)
        IF (LGL1) GO TO 402
      END IF
      IF (YHFG) THEN
        CALL UG2DH2(LSUB,PT2C,PT2D,PNTX)
        CALL UG2DH4(PT2C,PT2D,TOLR,WKAR,HFCU,HFFL,LGL1)
        IF (LGL1) GO TO 402
      END IF
      IF (XLFG) THEN
        CALL UG2DH2(LSUB,PT2D,PT2A,PNTX)
        CALL UG2DH4(PT2D,PT2A,TOLR,WKAR,HFCU,HFFL,LGL1)
        IF (LGL1) GO TO 402
      END IF
C
C  DRAW THE INDIVIDUAL COLUMNS.
      DO 202 V1IX=V1LO,V1HI,V1DL
        DO 201 V2IX=V2LO,V2HI,V2DL
C    OBTAIN THE V1-V2-V3 COORDINATES OF THE COLUMN.
          IF (V1FG.EQ.1) THEN
            V1CL=ARAY(1,V1IX)
            V1FR=ARAY(1,V1IX+V1DL)
            V2MN=MIN(ARAY(V2IX,1),ARAY(V2IX+V2DL,1))
            V2MX=MAX(ARAY(V2IX,1),ARAY(V2IX+V2DL,1))
            V3HI=ARAY(V2IX+MIN(0,V2DL),V1IX+MIN(0,V1DL))
          ELSE
            V1CL=ARAY(V1IX,1)
            V1FR=ARAY(V1IX+V1DL,1)
            V2MN=MIN(ARAY(1,V2IX),ARAY(1,V2IX+V2DL))
            V2MX=MAX(ARAY(1,V2IX),ARAY(1,V2IX+V2DL))
            V3HI=ARAY(V1IX+MIN(0,V1DL),V2IX+MIN(0,V2DL))
          END IF
C    DRAW THE VERTICAL LINES OF THE COLUMN.
          IF (TRNS(24-V1FG).GT.0.0) THEN
            VHFG=.FALSE.
          ELSE
            VHFG=.TRUE.
            PNT3(1)=V1FR
            PNT3(2)=V2MX
            PNT3(3)=ARAY(1,1)
            CALL UG2DH1(V1FG,PNT3,TRNS,PT2A)
            PNT3(3)=V3HI
            CALL UG2DH1(V1FG,PNT3,TRNS,PT2B)
            PT2B(1)=PT2A(1)
            CALL UG2DH3(PT2A,PT2B,TOLR,WKAR,HFCU,PT2C,PT2D,LSFG)
            IF (LSFG) CALL UG2DH2(LSUB,PT2C,PT2D,PNTX)
          END IF
          PNT3(1)=V1CL
          PNT3(2)=V2MX
          PNT3(3)=ARAY(1,1)
          CALL UG2DH1(V1FG,PNT3,TRNS,PT2A)
          PNT3(3)=V3HI
          CALL UG2DH1(V1FG,PNT3,TRNS,PT2B)
          PT2B(1)=PT2A(1)
          CALL UG2DH3(PT2A,PT2B,TOLR,WKAR,HFCU,PT2C,PT2D,LSFG)
          IF (LSFG) CALL UG2DH2(LSUB,PT2C,PT2D,PNTX)
          PNT3(1)=V1CL
          PNT3(2)=V2MN
          PNT3(3)=ARAY(1,1)
          CALL UG2DH1(V1FG,PNT3,TRNS,PT2A)
          PNT3(3)=V3HI
          CALL UG2DH1(V1FG,PNT3,TRNS,PT2B)
          PT2B(1)=PT2A(1)
          CALL UG2DH3(PT2A,PT2B,TOLR,WKAR,HFCU,PT2C,PT2D,LSFG)
          IF (LSFG) CALL UG2DH2(LSUB,PT2C,PT2D,PNTX)
          IF (TRNS(24-V1FG).LT.0.0) THEN
            VLFG=.FALSE.
          ELSE
            VLFG=.TRUE.
            PNT3(1)=V1FR
            PNT3(2)=V2MN
            PNT3(3)=ARAY(1,1)
            CALL UG2DH1(V1FG,PNT3,TRNS,PT2A)
            PNT3(3)=V3HI
            CALL UG2DH1(V1FG,PNT3,TRNS,PT2B)
            PT2B(1)=PT2A(1)
            CALL UG2DH3(PT2A,PT2B,TOLR,WKAR,HFCU,PT2C,PT2D,LSFG)
            IF (LSFG) CALL UG2DH2(LSUB,PT2C,PT2D,PNTX)
          END IF
C    DRAW THE TOP OF THE COLUMN.
          PNT3(1)=V1FR
          PNT3(2)=V2MN
          PNT3(3)=V3HI
          CALL UG2DH1(V1FG,PNT3,TRNS,PT2A)
          PNT3(1)=V1CL
          CALL UG2DH1(V1FG,PNT3,TRNS,PT2B)
          PNT3(2)=V2MX
          CALL UG2DH1(V1FG,PNT3,TRNS,PT2C)
          PNT3(1)=V1FR
          CALL UG2DH1(V1FG,PNT3,TRNS,PT2D)
          IF (VLFG.OR.(TRNS(24).LT.0.0)) THEN
            CALL UG2DH3(PT2A,PT2B,TOLR,WKAR,HFCU,PT2E,PT2F,L1FG)
            IF (L1FG) CALL UG2DH2(LSUB,PT2E,PT2F,PNTX)
          END IF
          CALL UG2DH3(PT2B,PT2C,TOLR,WKAR,HFCU,PT2G,PT2H,L2FG)
          IF (L2FG) CALL UG2DH2(LSUB,PT2G,PT2H,PNTX)
          IF (VHFG.OR.(TRNS(24).LT.0.0)) THEN
            CALL UG2DH3(PT2C,PT2D,TOLR,WKAR,HFCU,PT2I,PT2J,L3FG)
            IF (L3FG) CALL UG2DH2(LSUB,PT2I,PT2J,PNTX)
          END IF
          IF (TRNS(24).LT.0.0) THEN
            CALL UG2DH3(PT2D,PT2A,TOLR,WKAR,HFCU,PT2K,PT2L,L4FG)
            IF (L4FG) CALL UG2DH2(LSUB,PT2K,PT2L,PNTX)
          END IF
C    ADJUST THE HEIGHT FUNCTION.
          IF ((VLFG.OR.(TRNS(24).LT.0.0)).AND.(L1FG)) THEN
            IF ((      VLFG .AND.(TRNS(24).GE.0.0)).OR.
     X          ((.NOT.VLFG).AND.(TRNS(24).LT.0.0))) THEN
              CALL UG2DH4(PT2E,PT2F,TOLR,WKAR,HFCU,HFFL,LGL1)
              IF (LGL1) GO TO 402
            END IF
          END IF
          IF (L2FG) THEN
            IF (TRNS(24).GE.0.0) THEN
              CALL UG2DH4(PT2G,PT2H,TOLR,WKAR,HFCU,HFFL,LGL1)
              IF (LGL1) GO TO 402
            END IF
          END IF
          IF ((VHFG.OR.(TRNS(24).LT.0.0)).AND.(L3FG)) THEN
            IF ((      VHFG .AND.(TRNS(24).GE.0.0)).OR.
     X          ((.NOT.VHFG).AND.(TRNS(24).LT.0.0))) THEN
              CALL UG2DH4(PT2I,PT2J,TOLR,WKAR,HFCU,HFFL,LGL1)
              IF (LGL1) GO TO 402
            END IF
          END IF
          IF (L4FG) THEN
            CALL UG2DH4(PT2K,PT2L,TOLR,WKAR,HFCU,HFFL,LGL1)
            IF (LGL1) GO TO 402
          END IF
  201   CONTINUE
  202 CONTINUE
C
C  RESET ERROR INDICATORS AND RETURN TO CALLER.
      UGELV=0
      UGENM='        '
      UGEIX=0
  301 RETURN
C
C  REPORT ERRORS TO THE UNIFIED GRAPHICS SYSTEM ERROR PROCESSOR.
  401 CALL UGRERR(3,'UG2DHG  ',1)
      GO TO 301
  402 CALL UGRERR(3,'UG2DHG  ',2)
      GO TO 301
  403 CALL UGRERR(3,'UG2DHG  ',3)
      GO TO 301
C
      END
      SUBROUTINE UG2DH1(VSWT,PNT3,TRNS,PPNT)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                  PROJECT A POINT ONTO THE SCREEN                  *
C *                                                                   *
C *  THIS SUBROUTINE IS USED BY UG2DHG TO PROJECT A POINT IN THE      *
C *  (V1-V2-V3) SPACE INTO THE SCREEN REFERENCE SYSTEM.               *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UG2DH1(VSWT,PNT3,TRNS,PPNT)                               *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    VSWT  AN X/Y VARIABLE SWITCH.                                  *
C *    PNT3  THE POINT IN 3-DIMENSIONAL SPACE.                        *
C *    TRNS  THE PROJECTION TRANSFORMATION.                           *
C *    PPNT  THE PROJECTED POINT.                                     *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       VSWT
      REAL          PNT3(3)
      REAL          TRNS(31)
      REAL          PPNT(2)
C
      REAL          PT3D(3)
C
C  OBTAIN THE POINT AND PROJECT IT INTO THE VIEW PLANE.
      IF (VSWT.EQ.1) THEN
        PT3D(1)=PNT3(1)
        PT3D(2)=PNT3(2)
        PT3D(3)=PNT3(3)
      ELSE
        PT3D(1)=PNT3(2)
        PT3D(2)=PNT3(1)
        PT3D(3)=PNT3(3)
      END IF
      CALL UGPROJ(TRNS,PT3D,PPNT)
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE UG2DH2(LSUB,PNT1,PNT2,PNTX)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                        DRAW A LINE SEGMENT                        *
C *                                                                   *
C *  THIS SUBROUTINE IS USED BY UG2DHG TO DRAW A SINGLE LINE          *
C *  SEGMENT.  REDUNDANT BLANK MOVEMENTS ARE SUPPRESSED.              *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UG2DH2(LSUB,PNT1,PNT2,PNTX)                               *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    LSUB  THE LINE SEGMENT END POINT SUBROUTINE.                   *
C *    PNT1  THE FIRST POINT.                                         *
C *    PNT2  THE SECOND POINT.                                        *
C *    PNTX  A POINT USED TO REMEMBER THE LAST DRAWN POINT.           *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      EXTERNAL      LSUB
      REAL          PNT1(2),PNT2(2),PNTX(2)
C
C  DRAW THE VECTOR AND SAVE CURRENT POSITION.
      IF ((PNT1(1).NE.PNTX(1)).OR.(PNT1(2).NE.PNTX(2)))
     X  CALL LSUB(PNT1(1),PNT1(2),0)
      CALL LSUB(PNT2(1),PNT2(2),1)
      PNTX(1)=PNT2(1)
      PNTX(2)=PNT2(2)
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE UG2DH3(PNT1,PNT2,TOLR,WKAR,HFCU,PNT3,PNT4,VFLG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *             CHECK A LINE AGAINST THE HEIGHT FUNCTION              *
C *                                                                   *
C *  THIS SUBROUTINE IS USED BY UG2DHG TO CHECK THE CURRENT LINE      *
C *  SEGMENT AGAINST THE HEIGHT FUNCTION.  IF PART OF THE LINE IS     *
C *  VISIBLE, A FLAG IS SET AND THE VISIBLE PART IS SAVED IN TWO      *
C *  OUTPUT POINTS.                                                   *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UG2DH3(PNT1,PNT2,TOLR,WKAR,HFCU,PNT3,PNT4,VFLG)           *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    PNT1  AN END POINT OF THE CURRENT LINE SEGMENT.                *
C *    PNT2  AN END POINT OF THE CURRENT LINE SEGMENT.                *
C *    TOLR  A TOLERANCE.                                             *
C *    WKAR  A WORK AREA.                                             *
C *    HFCU  THE INDEX OF THE CURRENT HEIGHT FUNCTION ELEMENT.        *
C *    PNT3  AN END POINT OF THE VISIBLE LINE SEGMENT.                *
C *    PNT4  AN END POINT OF THE VISIBLE LINE SEGMENT.                *
C *    VFLG  A FLAG INDICATING IF A VISIBLE SEGMENT IS AVAILABLE.     *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      REAL          PNT1(2),PNT2(2)
      REAL          TOLR
      REAL*4        WKAR(*)
      INTEGER       HFCU
      REAL          PNT3(2),PNT4(2)
      LOGICAL       VFLG
C
      REAL*4        HFB1
      INTEGER*2     HFP1(2)
      EQUIVALENCE   (HFP1(1),HFB1)
C
      LOGICAL       FGSW,FGVI,FGPA
      INTEGER       HFNX
      REAL          TPT3(2),PTP3(2),PTP4(2),PNTI(2)
C
C  SAVE AND ORDER GIVEN LINE SEGMENT, AND SET HEIGHT FUNCTION INDEX.
      IF (PNT1(1).LE.PNT2(1)) THEN
        PNT3(1)=PNT1(1)
        PNT3(2)=PNT1(2)
        PNT4(1)=PNT2(1)
        PNT4(2)=PNT2(2)
        FGSW=.FALSE.
      ELSE
        PNT3(1)=PNT2(1)
        PNT3(2)=PNT2(2)
        PNT4(1)=PNT1(1)
        PNT4(2)=PNT1(2)
        FGSW=.TRUE.
      END IF
  101 IF (WKAR(HFCU+1).GT.PNT3(1)) THEN
        HFB1=WKAR(HFCU)
        HFCU=HFP1(2)
        GO TO 101
      END IF
  102 HFB1=WKAR(HFCU)
      HFNX=HFP1(1)
      IF (PNT3(1).GE.WKAR(HFNX+1)) THEN
        HFCU=HFNX
        GO TO 102
      END IF
      TPT3(1)=PNT3(1)
      TPT3(2)=PNT3(2)
      FGVI=.FALSE.
C
C  DETERMINE WHICH PART (IF ANY) OF THE GIVEN LINE IS ABOVE THE
C  HEIGHT FUNCTION.
  201 CALL UG2DH5(TPT3,WKAR(HFCU+1),WKAR(HFNX+1),PTP3)
      IF (PNT4(1).LE.WKAR(HFNX+1)) THEN
C    GIVEN LINE TERMINATES IN CURRENT HEIGHT FUNCTION SEGMENT.
        CALL UG2DH5(PNT4,WKAR(HFCU+1),WKAR(HFNX+1),PTP4)
        IF (TPT3(2).GE.PTP3(2)) THEN
          IF (PNT4(2).GE.PTP4(2)) THEN
C      GIVEN LINE IS ABOVE CURRENT HEIGHT FUNCTION SEGMENT.
            CONTINUE
          ELSE
C      FIRST POINT IS ABOVE CURRENT HEIGHT FUNCTION, SECOND BELOW.
            CALL UG2DH6(WKAR(HFCU+1),WKAR(HFNX+1),PNT1,PNT2,
     X        PNTI,TOLR,FGPA)
            IF (FGPA) THEN
              PNT4(1)=PNTI(1)
              PNT4(2)=PNTI(2)
            ELSE
              PNT4(1)=TPT3(1)
              PNT4(2)=TPT3(2)
            END IF
          END IF
        ELSE
          IF (PNT4(2).GE.PTP4(2)) THEN
C      FIRST POINT IS BELOW CURRENT HEIGHT FUNCTION, SECOND ABOVE.
            CALL UG2DH6(WKAR(HFCU+1),WKAR(HFNX+1),PNT1,PNT2,
     X        PNTI,TOLR,FGPA)
            IF (FGPA) THEN
              PNT3(1)=PNTI(1)
              PNT3(2)=PNTI(2)
            ELSE
              PNT4(1)=TPT3(1)
              PNT4(2)=TPT3(2)
            END IF
          ELSE
C      GIVEN LINE IS BELOW CURRENT HEIGHT FUNCTION SEGMENT.
            PNT4(1)=TPT3(1)
            PNT4(2)=TPT3(2)
          END IF
        END IF
        GO TO 301
      ELSE
C    GIVEN LINE DOES NOT TERMINATE IN CURRENT HEIGHT FUNCTION SEGMENT.
        CALL UG2DH5(WKAR(HFNX+1),PNT1,PNT2,PTP4)
        IF (TPT3(2).GE.PTP3(2)) THEN
          IF (PTP4(2).GE.WKAR(HFNX+2)) THEN
C      GIVEN LINE IS ABOVE CURRENT HEIGHT FUNCTION SEGMENT.
            TPT3(1)=PTP4(1)
            TPT3(2)=PTP4(2)
            FGVI=.TRUE.
          ELSE
C      FIRST POINT IS ABOVE CURRENT HEIGHT FUNCTION, SECOND BELOW.
            CALL UG2DH6(WKAR(HFCU+1),WKAR(HFNX+1),PNT1,PNT2,
     X        PNTI,TOLR,FGPA)
            IF (FGPA) THEN
              PNT4(1)=PNTI(1)
              PNT4(2)=PNTI(2)
              GO TO 301
            ELSE
              IF (FGVI) THEN
                TPT3(1)=PTP4(1)
                TPT3(2)=PTP4(2)
              ELSE
                TPT3(1)=PTP4(1)
                TPT3(2)=PTP4(2)
                PNT3(1)=PTP4(1)
                PNT3(2)=PTP4(2)
              END IF
            END IF
          END IF
        ELSE
          IF (PTP4(2).GE.WKAR(HFNX+2)) THEN
C      FIRST POINT IS BELOW CURRENT HEIGHT FUNCTION, SECOND ABOVE.
            CALL UG2DH6(WKAR(HFCU+1),WKAR(HFNX+1),PNT1,PNT2,
     X        PNTI,TOLR,FGPA)
            IF (FGPA) THEN
              PNT3(1)=PNTI(1)
              PNT3(2)=PNTI(2)
              TPT3(1)=PNTI(1)
              TPT3(2)=PNTI(2)
              FGVI=.TRUE.
            ELSE
              IF (FGVI) THEN
                TPT3(1)=PTP4(1)
                TPT3(2)=PTP4(2)
              ELSE
                TPT3(1)=PTP4(1)
                TPT3(2)=PTP4(2)
                PNT3(1)=PTP4(1)
                PNT3(2)=PTP4(2)
              END IF
            END IF
          ELSE
C      GIVEN LINE IS BELOW CURRENT HEIGHT FUNCTION SEGMENT.
            IF (FGVI) THEN
              PNT4(1)=TPT3(1)
              PNT4(2)=TPT3(2)
              GO TO 301
            ELSE
              TPT3(1)=PTP4(1)
              TPT3(2)=PTP4(2)
              PNT3(1)=PTP4(1)
              PNT3(2)=PTP4(2)
            END IF
          END IF
        END IF
      END IF
C    ADVANCE HEIGHT FUNCTION POINTER TO NEXT NON-VERTICAL SEGMENT.
  202 HFCU=HFNX
      HFB1=WKAR(HFCU)
      HFNX=HFP1(1)
      IF (WKAR(HFCU+1).EQ.WKAR(HFNX+1)) GO TO 202
      GO TO 201
C
C  PROCESS THE OUTPUT SEGMENT.
  301 IF ((PNT3(1).EQ.PNT4(1)).AND.
     X    (PNT3(2).EQ.PNT4(2))) THEN
        VFLG=.FALSE.
      ELSE
        IF (FGSW) THEN
          TPT3(1)=PNT3(1)
          TPT3(2)=PNT3(2)
          PNT3(1)=PNT4(1)
          PNT3(2)=PNT4(2)
          PNT4(1)=TPT3(1)
          PNT4(2)=TPT3(2)
        END IF
        VFLG=.TRUE.
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE UG2DH4(PNT1,PNT2,TOLR,WKAR,HFCU,HFFL,EFLG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                    UPDATE THE HEIGHT FUNCTION                     *
C *                                                                   *
C *  THIS SUBROUTINE IS USED BY UG2DHG TO UPDATE THE HEIGHT           *
C *  FUNCTION USING THE GIVEN LINE SEGMENT.                           *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UG2DH4(PNT1,PNT2,TOLR,WKAR,HFCU,HFFL,EFLG)                *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    PNT1  AN END POINT OF THE GIVEN SEGMENT.                       *
C *    PNT2  AN END POINT OF THE GIVEN SEGMENT.                       *
C *    TOLR  A TOLERANCE.                                             *
C *    WKAR  A WORK ARRAY.                                            *
C *    HFCU  THE INDEX OF THE CURRENT HEIGHT FUNCTION ELEMENT.        *
C *    HFFL  THE INDEX OF THE HEIGHT FUNCTION FREE ELEMENT LIST.      *
C *    EFLG  AN ERROR FLAG.                                           *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      REAL          PNT1(2),PNT2(2)
      REAL          TOLR
      REAL*4        WKAR(*)
      INTEGER       HFCU,HFFL
      LOGICAL       EFLG
C
      REAL*4        HFB1
      INTEGER*2     HFP1(2)
      EQUIVALENCE   (HFP1(1),HFB1)
C
      REAL          PNTA(2),PNTB(2),PNTC(2),PNTD(2)
      INTEGER       HFNX,HFPA,HFPD
C
      INTEGER       INT1,INT2
C
C  SAVE AND ORDER THE GIVEN LINE SEGMENT.
      IF (ABS(PNT1(1)-PNT2(1)).LE.TOLR) THEN
        GO TO 401
      ELSE IF (PNT1(1).LT.PNT2(1)) THEN
        PNTB(1)=PNT1(1)
        PNTB(2)=PNT1(2)
        PNTC(1)=PNT2(1)
        PNTC(2)=PNT2(2)
      ELSE
        PNTB(1)=PNT2(1)
        PNTB(2)=PNT2(2)
        PNTC(1)=PNT1(1)
        PNTC(2)=PNT1(2)
      END IF
C
C  BRACKET THE SECTION OF HEIGHT FUNCTION TO BE DELETED.
  101 IF (WKAR(HFCU+1).GE.(PNTB(1)-TOLR)) THEN
        HFB1=WKAR(HFCU)
        HFCU=HFP1(2)
        GO TO 101
      END IF
  102 HFB1=WKAR(HFCU)
      HFNX=HFP1(1)
      IF ((PNTB(1)-TOLR).GT.WKAR(HFNX+1)) THEN
        HFCU=HFNX
        GO TO 102
      END IF
      HFPA=HFCU
      CALL UG2DH5(PNTB,WKAR(HFCU+1),WKAR(HFNX+1),PNTA)
  103 IF ((PNTC(1)+TOLR).GE.WKAR(HFNX+1)) THEN
        HFCU=HFNX
        HFB1=WKAR(HFCU)
        HFNX=HFP1(1)
        GO TO 103
      END IF
      HFPD=HFNX
      CALL UG2DH5(PNTC,WKAR(HFCU+1),WKAR(HFNX+1),PNTD)
      HFCU=HFPA
C
C  FREE ALL HEIGHT FUNCTION BLOCKS THAT HAVE BEEN BRACKETED.
      INT1=HFPA
      HFB1=WKAR(INT1)
      INT2=HFP1(1)
  201 IF (INT2.NE.HFPD) THEN
        INT1=INT2
        HFB1=WKAR(INT1)
        INT2=HFP1(1)
        HFP1(1)=HFFL
        WKAR(INT1)=HFB1
        HFFL=INT1
        GO TO 201
      END IF
C
C  INSERT THE NEW POINTS INTO THE HEIGHT FUNCTION.
C    IF NECESSARY, INSERT PNTA AND ADVANCE HFPA.
      IF ((PNTA(1).NE.WKAR(HFPA+1)).OR.
     X    (ABS(PNTA(2)-WKAR(HFPA+2)).GE.TOLR)) THEN
        IF (HFFL.EQ.0) GO TO 403
        HFB1=WKAR(HFPA)
        HFP1(1)=HFFL
        WKAR(HFPA)=HFB1
        HFB1=WKAR(HFFL)
        INT1=HFP1(1)
        HFP1(2)=HFPA
        WKAR(HFFL)=HFB1
        WKAR(HFFL+1)=PNTA(1)
        WKAR(HFFL+2)=PNTA(2)
        HFPA=HFFL
        HFFL=INT1
      END IF
C    IF NECESSARY, INSERT PNTB AND ADVANCE HFPA.
      IF ((PNTB(1).NE.WKAR(HFPA+1)).OR.
     X    (ABS(PNTB(2)-WKAR(HFPA+2)).GE.TOLR)) THEN
        IF (HFFL.EQ.0) GO TO 403
        HFB1=WKAR(HFPA)
        HFP1(1)=HFFL
        WKAR(HFPA)=HFB1
        HFB1=WKAR(HFFL)
        INT1=HFP1(1)
        HFP1(2)=HFPA
        WKAR(HFFL)=HFB1
        WKAR(HFFL+1)=PNTB(1)
        WKAR(HFFL+2)=PNTB(2)
        HFPA=HFFL
        HFFL=INT1
      END IF
C    IF NECESSARY, INSERT PNTD AND DECREMENT HFPD.
      IF ((PNTD(1).NE.WKAR(HFPD+1)).OR.
     X    (ABS(PNTD(2)-WKAR(HFPD+2)).GE.TOLR)) THEN
        IF (HFFL.EQ.0) GO TO 403
        HFB1=WKAR(HFPD)
        HFP1(2)=HFFL
        WKAR(HFPD)=HFB1
        HFB1=WKAR(HFFL)
        INT1=HFP1(1)
        HFP1(1)=HFPD
        WKAR(HFFL)=HFB1
        WKAR(HFFL+1)=PNTD(1)
        WKAR(HFFL+2)=PNTD(2)
        HFPD=HFFL
        HFFL=INT1
      END IF
C    IF NECESSARY, INSERT PNTC AND DECREMENT HFPD.
      IF ((PNTC(1).NE.WKAR(HFPD+1)).OR.
     X    (ABS(PNTC(2)-WKAR(HFPD+2)).GE.TOLR)) THEN
        IF (HFFL.EQ.0) GO TO 403
        HFB1=WKAR(HFPD)
        HFP1(2)=HFFL
        WKAR(HFPD)=HFB1
        HFB1=WKAR(HFFL)
        INT1=HFP1(1)
        HFP1(1)=HFPD
        WKAR(HFFL)=HFB1
        WKAR(HFFL+1)=PNTC(1)
        WKAR(HFFL+2)=PNTC(2)
        HFPD=HFFL
        HFFL=INT1
      END IF
C    LINK HFPA AND HFPD TOGETHER.
      HFB1=WKAR(HFPA)
      HFP1(1)=HFPD
      WKAR(HFPA)=HFB1
      HFB1=WKAR(HFPD)
      HFP1(2)=HFPA
      WKAR(HFPD)=HFB1
C
C  RETURN TO CALLING SUBROUTINE WITH ERROR FLAG SET.
  401 EFLG=.FALSE.
  402 RETURN
  403 EFLG=.TRUE.
      GO TO 402
C
      END
      SUBROUTINE UG2DH5(PNTI,PNT1,PNT2,PNTO)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *               PROJECT A POINT ONTO A STRAIGHT LINE                *
C *                                                                   *
C *  THIS SUBROUTINE IS USED BY UG2DH3 TO PROJECT A GIVEN POINT       *
C *  VERTICALLY ONTO A STRAIGHT LINE.  THE X COORDINATES OF THE ENDS  *
C *  OF THE STRAIGHT LINE MUST BE DISTINCT.                           *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UG2DH5(PNTI,PNT1,PNT2,PNTO)                               *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    PNTI  THE GIVEN POINT THAT IS TO BE PROJECTED ONTO THE LINE.   *
C *    PNT1  THE FIRST END POINT OF THE LINE.                         *
C *    PNT2  THE SECOND END POINT OF THE LINE.                        *
C *    PNTO  THE COMPUTED POINT ON THE LINE.                          *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      REAL          PNTI(2),PNT1(2),PNT2(2),PNTO(2)
C
C  PROJECT THE POINT ONTO THE LINE.
      PNTO(1)=PNTI(1)
      PNTO(2)=(PNT1(2)*(PNT2(1)-PNTI(1))+
     X         PNT2(2)*(PNTI(1)-PNT1(1)))/(PNT2(1)-PNT1(1))
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END
      SUBROUTINE UG2DH6(PTA1,PTA2,PTB1,PTB2,PNTO,TOLR,PFLG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                   INTERSECT TWO STRAIGHT LINES                    *
C *                                                                   *
C *  THIS SUBROUTINE IS USED BY UG2DH3 TO INTERSECT TWO STRAIGHT      *
C *  LINES GIVEN BY THEIR END POINTS.  THE EXTENSIVE CHECKING DONE    *
C *  HERE IS NECESSARY IN SOME CASES AND DEPENDS ON THE WAY THIS      *
C *  SUBROUTINE IS CALLED.                                            *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UG2DH6(PTA1,PTA2,PTB1,PTB2,PNTO,TOLR,PFLG)                *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    PTA1  THE FIRST END POINT OF THE FIRST LINE.                   *
C *    PTA2  THE SECOND END POINT OF THE FIRST LINE.                  *
C *    PTB1  THE FIRST END POINT OF THE SECOND LINE.                  *
C *    PTB2  THE SECOND END POINT OF THE SECOND LINE.                 *
C *    PNTO  THE COMPUTED INTERSECTION POINT.                         *
C *    TOLR  A TOLERANCE.                                             *
C *    PFLG  A FLAG INDICATING IF AN INTERSECTION POINT IS            *
C *          AVAILABLE.                                               *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      REAL          PTA1(2),PTA2(2),PTB1(2),PTB2(2),PNTO(2),TOLR
      LOGICAL       PFLG
C
      REAL          EQUA(3),EQUB(3),DETR
      REAL          TLSQ,POLA,POLB
C
C  SET UP THE EQUATIONS OF THE TWO LINES AND SOLVE THEM SIMULTANEOUSLY.
      TLSQ=TOLR*TOLR
      EQUA(1)=PTA1(2)-PTA2(2)
      EQUA(2)=PTA2(1)-PTA1(1)
      EQUA(3)=PTA1(1)*PTA2(2)-PTA2(1)*PTA1(2)
      EQUB(1)=PTB1(2)-PTB2(2)
      EQUB(2)=PTB2(1)-PTB1(1)
      EQUB(3)=PTB1(1)*PTB2(2)-PTB2(1)*PTB1(2)
      DETR=EQUA(1)*EQUB(2)-EQUB(1)*EQUA(2)
      IF (ABS(DETR).LE.TLSQ) THEN
        PFLG=.FALSE.
      ELSE
        PFLG=.TRUE.
        PNTO(1)=(EQUA(2)*EQUB(3)-EQUB(2)*EQUA(3))/DETR
        PNTO(2)=(EQUB(1)*EQUA(3)-EQUA(1)*EQUB(3))/DETR
        IF (PTB1(1).EQ.PTB2(1)) PNTO(1)=PTB1(1)
        IF ((PNTO(1)+TOLR).LT.PTA1(1)) THEN
          PFLG=.FALSE.
        ELSE IF ((PNTO(1)-TOLR).GT.PTA2(1)) THEN
          PFLG=.FALSE.
        ELSE IF ((PNTO(1)+TOLR).LT.MIN(PTB1(1),PTB2(1))) THEN
          PFLG=.FALSE.
        ELSE IF ((PNTO(1)-TOLR).GT.MAX(PTB1(1),PTB2(1))) THEN
          PFLG=.FALSE.
        ELSE IF ((PNTO(2)+TOLR).LT.MIN(PTB1(2),PTB2(2))) THEN
          PFLG=.FALSE.
        ELSE IF ((PNTO(2)-TOLR).GT.MAX(PTB1(2),PTB2(2))) THEN
          PFLG=.FALSE.
        ELSE
          POLA=EQUA(1)*PNTO(1)+EQUA(2)*PNTO(2)+EQUA(3)
          POLB=EQUB(1)*PNTO(1)+EQUB(2)*PNTO(2)+EQUB(3)
          IF ((POLA*POLA).GT.
     X        (TLSQ*(EQUA(1)*EQUA(1)+EQUA(2)*EQUA(2)))) THEN
            PFLG=.FALSE.
          ELSE IF ((POLB*POLB).GT.
     X        (TLSQ*(EQUB(1)*EQUB(1)+EQUB(2)*EQUB(2)))) THEN
            PFLG=.FALSE.
          END IF
        END IF
      END IF
C
C  RETURN TO CALLING SUBROUTINE.
      RETURN
C
      END


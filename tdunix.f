C*---------------------------------------------------------------------**
C*                                                                     **
C*                          DISCLAIMER NOTICE                          **
C*                                                                     **
C* This document  and/or portions of  the material and  data furnished **
C* herewith, was developed under sponsorship of the U.S.   Government. **
C* Neither the U.S. nor the U.S.D.O.E., nor the Leland Stanford Junior **
C* University, nor their employees,  nor their respective contractors, **
C* subcontractors, or their employees, makes any warranty,  express or **
C* implied,  or assumes any liability  or responsibility for accuracy, **
C* completeness or usefulness of any information,  apparatus,  product **
C* or process disclosed,  or represents that its use will not infringe **
C* privately-owned rights.  Mention of any product,  its manufacturer, **
C* or suppliers  shall not,  nor is  it intended to,   imply approval, **
C* disapproval, or fitness for any particular use.   The U.S.  and the **
C* University at  all times  retain the right  to use  and disseminate **
C* same for any purpose whatsoever.                                    **
C*                                                                     **
C*---------------------------------------------------------------------**
C*                                                                     **
C* Copyright (C) 1986 The Board of Trustees of The Leland Stanford     **
C* Junior University.  All Rights Reserved.                            **
C*                                                                     **
C*---------------------------------------------------------------------**
      subroutine tdmain(mmfile,lfile,mopts,lopts,maxopts)
      character *(*)  mmfile,mopts
      character * 136 file
      character * 132 chtmp,opts
      COMMON /T2COM/            REDUCE(2), TITX, TITY, BUFFER(400), XFRM
     112(12), XFRM13(12), XFRM14(12), XFRM23(12), XFRM24(12), XFRM34(6),
     2 XVARSX(20), TSIZDF,         GRDSIZ, CIRSIZ(2,3), ASIZE, AFLARE, S
     3MSZDF, ORAXES(3), EYEDIR(3), VUEDIR(3), VUECEN(3), VERTCL(3), EYES
     4EP, SCRD, SCRZ, EYEDIS, EYEPNT(3), XYPART(2, 2), BARSIZ(3), TIKSIZ
     5(3), XYZBAS(3), TTHETA, TPHI, FRELBL(4), DOTWID, DOTWDF, PATRN(10)
     6, SCLPRM(10,3), WINDOW(4), SCREEN(2),                 FACTXY(15),
     7LBLSIZ, SYMSIZ, XYZLIM(3,2), WORLD(3), RADRMX,RADRMN,RADAMN,RADANG
     8,RADX0,RADY0,XUNUSD(6), USRKBD,USRSCR, PLTFIL, ITITDT(3),
     9   LINTEX, ERRFIL, OUTFIL, DBGFIL, INPFIL, LENCRD,SYMBOL,      GRD
     XTYP, LINWID, LINCOL, TITCON(5), ITCNTR, IUNUSD,GRDSYM,    NXYZ1(3)
     X, NXYZ2(3), LBLCHR(3), LINWDF, NPATRN, NDIMNS(3), IBLKTP, IDIMNS,
     XLDEVCE, NMESH1, NMESH2, MESH1, MESH2, MESH3, NFIELD, IFIELD(15), N
     XINCR, LISTPT, NNAME, NHEADR, IVARBL(15), IVRPTR(15), LINEAR(4), NO
     XNLIN(4), INFO(10),NOSYMB,     NPOINT, BUFSIZ, LETSET, MXECHO, LINE
     XS, MAXLNS
      REAL WINDOW,SCREEN,SYMSIZ,LBLSIZ,GRDSYM, GRDSIZ,BUFFER,REDUCE,SYMB
     1OL,NOSYMB, FRELBL,SCLPRM,TIKSIZ,BARSIZ,XYZBAS, CIRSIZ,TITX,TITY,VU
     2EDIR,EYEPNT,XYPART, ORAXES,EYEDIR,VUECEN,VERTCL,SCRD,SCRZ,
     3EYESEP,EYEDIS
      INTEGER       NXYZ1,NXYZ2,LBLCHR,NONLIN,LINEAR, ERRFIL,OUTFIL,DBGF
     1IL, LENCRD,BUFSIZ,IFIELD, ITITDT,ITCNTR, USRKBD,USRSCR,PLTFIL, DOT
     2WID,DOTWDF,LINWID,LINWDF, GRDTYP,LETSET, NDIMNS,IBLKTP,IDIMNS,NINC
     3R,LISTPT, IVARBL,NNAME,NHEADR,IVRPTR
      COMMON /T2COMC/ TDDATE(8),CARD(160),INPFMT(16),PXNAME(8),INAME(3)
      CHARACTER*1 TDDATE,INAME
      CHARACTER*1 CARD,INPFMT,PXNAME
      logical topext
c      CHARACTER*1 ESC
      CHARACTER * 132 SETLIN, DEFLT(132) * 1
      EQUIVALENCE (DEFLT(1),SETLIN)
c      esc=char(27)
      if(lfile.gt.128) then
         write(*,*) ' filename too long '
         goto 999
      endif
      file=mmfile(1:lfile)
      opts=mopts(1:lopts)
      call debug
C SET DEFAULT DEVICE AND FILE
      DO J=1,10
         INFO(J) = 0
      ENDDO
c take away .top ending
      if(lfile.gt.3.and.file(lfile-3:lfile).eq.'.top') then
         llfile=lfile-4
         topext=.true.
      else
         llfile=lfile
         topext=.false.
      endif
c set default device
      if(index(opts(1:lopts),' DEVICE=').eq.0) then
          if(lopts+10.le.maxopts) then
             opts(lopts+1:lopts+10) = 'DEVICE=PS '
             lopts = lopts+10
          else
             goto 333
          endif
      endif
c  set default output file name
      if(index(opts(1:lopts),' OUTPUT=').eq.0) then
          if(lopts+llfile+19.le.maxopts) then
             opts(lopts+1:lopts+llfile+11)=
     #            ' OUTPUT="""'//file(1:llfile)
             lopts = lopts+llfile+11
             kdevice=8+index(opts,' DEVICE=')
             jdevice=kdevice+index(opts(kdevice+1:lopts),' ')
             if(opts(kdevice:jdevice).eq.'PS') then
                opts(lopts+1:lopts+3)='.ps'
                lopts=lopts+3
             elseif(opts(kdevice:jdevice).eq.'LN3') then
                opts(lopts+1:lopts+4)='.ln3'
                lopts=lopts+4
             endif
             opts(lopts+1:lopts+4)='""" '
             lopts=lopts+4
          else
             goto 333
          endif
      endif
c change OUTPUT into DDNAME
      k=index(opts(1:lopts),' OUTPUT=')
      opts(k+1:k+6)='DDNAME'
      k=index(opts(1:lopts),' DEVICE=')+8
      l=index(opts(k:lopts),' ')+k-2
c put device first
      chtmp=opts(k:l)//opts(1:k-9)//opts(l+1:lopts)
      lopts=lopts-8
      opts(1:lopts)=chtmp
      if(opts(1:4).eq.'LN3 ') then
         if(lopts.lt.maxopts-12) then
            chtmp='4010 SEQUENTIAL'//opts(4:lopts)
            lopts=lopts+12
            opts(1:lopts)=chtmp
         else
            goto 333
         endif
      endif
      if(opts(1:5).eq.'4010 ') then
         if(lopts.lt.maxopts-13) then
            chtmp='4010 INTERACTIVE'//opts(5:lopts)
            lopts=lopts+13
            opts(1:lopts)=chtmp
         else
            goto 333
         endif
      endif
      if(.not.topext) then
c add .top first (if not already there)
         file(lfile+1:)='.top'
         open (unit=9,file=file(1:lfile+4),
     #        status='old',err=70)
         goto 100
 70      file(lfile+1:)=' '
      endif
c try name as given
      open (unit=9,file=file(1:lfile),
     # status='old',err=995)
 100  continue
c chsk if td.lis and tderrors.lis files are desired
      k=index(opts(1:lopts),' LIS ')
      if(k.gt.0) then
         opts(k+1:k+3)='   '
         OPEN(UNIT=8,file='td.lis',status='UNKNOWN')
         OPEN(UNIT=86,file='tderrors.lis',status='UNKNOWN')
      else
         OPEN(UNIT=8,file='/dev/null',status='OLD')
         dbgfil=6
         errfil=6
      endif
      INFO(1) = lopts
      INFO(2) = 0
      CALL TXDDEV (INFO,opts)
      CALL TDVM
      CALL NICEST
      goto 999
 995  if(lfile.eq.llfile) then
         write(*,'(3a)') ' file ',file(1:lfile),'[.top] not found'
      else
         write(*,'(3a)') ' file ',file(1:lfile),' not found'
      endif
      goto 999
 333  write(*,*)' td: option string too long'
      goto 999
C MAKE EXTERNALS FOR THE LOADER
 999  continue
      close(8)
      close(86)
      close(9)
      END

      SUBROUTINE ZZWAIT
      CHARACTER*4 LINE,WORDS(12)
C CALL PROMPT(' ',1,$TERMINAL IN);  NULL PROMPT STRING
      DATA WORDS/    'HALT',    'STOP',    'QUIT',    'HX',    'HT',
     1               'halt',    'stop',    'quit',    'hx',    'ht',
     2               'EXIT',    'exit'/
      WRITE(6,'('' C.R. to go on, STOP to quit:'')')
      READ (5,10,END=30) LINE
 10   FORMAT (A4)
      DO 20 I=1,12
      IF (LINE.EQ.WORDS(I)) THEN
          CALL NICEST
          STOP
      ENDIF
 20   CONTINUE
      RETURN
 30   REWIND 5
      RETURN
      END
 
      SUBROUTINE BEFST(STRING)
C STORE LINES TO OUTPUT BEFORE STOP
      CHARACTER * 80 LINE(10), STRING * (*)
      DATA ILINES/0/
      IF(ILINES.GE.10) THEN
         WRITE(6,*) 'Too many closing lines'
         STOP
      ENDIF
      ILINES = ILINES + 1
      LINE(ILINES) = STRING
      RETURN
C TO BE CALLED BEFORE STOP TO OUTPUT STORED LINES
      ENTRY NICEST
      DO J=1,ILINES
         WRITE(6,*) LINE(ILINES)
      ENDDO
      END
      SUBROUTINE TDVM
      COMMON /T2FLGC/ FLAGS(100)
      LOGICAL   FLAGS
      COMMON /T2COM/            REDUCE(2), TITX, TITY, BUFFER(400), XFRM
     112(12), XFRM13(12), XFRM14(12), XFRM23(12), XFRM24(12), XFRM34(6),
     2 XVARSX(20), TSIZDF,         GRDSIZ, CIRSIZ(2,3), ASIZE, AFLARE, S
     3MSZDF, ORAXES(3), EYEDIR(3), VUEDIR(3), VUECEN(3), VERTCL(3), EYES
     4EP, SCRD, SCRZ, EYEDIS, EYEPNT(3), XYPART(2, 2), BARSIZ(3), TIKSIZ
     5(3), XYZBAS(3), TTHETA, TPHI, FRELBL(4), DOTWID, DOTWDF, PATRN(10)
     6, SCLPRM(10,3), WINDOW(4), SCREEN(2),                 FACTXY(15),
     7LBLSIZ, SYMSIZ, XYZLIM(3,2), WORLD(3), RADRMX,RADRMN,RADAMN,RADANG
     8,RADX0,RADY0,XUNUSD(6), USRKBD,USRSCR, PLTFIL, ITITDT(3),
     9   LINTEX, ERRFIL, OUTFIL, DBGFIL, INPFIL, LENCRD,SYMBOL,      GRD
     XTYP, LINWID, LINCOL, TITCON(5), ITCNTR, IUNUSD,GRDSYM,    NXYZ1(3)
     X, NXYZ2(3), LBLCHR(3), LINWDF, NPATRN, NDIMNS(3), IBLKTP, IDIMNS,
     XLDEVCE, NMESH1, NMESH2, MESH1, MESH2, MESH3, NFIELD, IFIELD(15), N
     XINCR, LISTPT, NNAME, NHEADR, IVARBL(15), IVRPTR(15), LINEAR(4), NO
     XNLIN(4), INFO(10),NOSYMB,     NPOINT, BUFSIZ, LETSET, MXECHO, LINE
     XS, MAXLNS
      REAL WINDOW,SCREEN,SYMSIZ,LBLSIZ,GRDSYM, GRDSIZ,BUFFER,REDUCE,SYMB
     1OL,NOSYMB, FRELBL,SCLPRM,TIKSIZ,BARSIZ,XYZBAS, CIRSIZ,TITX,TITY,VU
     2EDIR,EYEPNT,XYPART, ORAXES,EYEDIR,VUECEN,VERTCL,SCRD,SCRZ,
     3EYESEP,EYEDIS
      INTEGER       NXYZ1,NXYZ2,LBLCHR,NONLIN,LINEAR, ERRFIL,OUTFIL,DBGF
     1IL, LENCRD,BUFSIZ,IFIELD, ITITDT,ITCNTR, USRKBD,USRSCR,PLTFIL, DOT
     2WID,DOTWDF,LINWID,LINWDF, GRDTYP,LETSET, NDIMNS,IBLKTP,IDIMNS,NINC
     3R,LISTPT, IVARBL,NNAME,NHEADR,IVRPTR
      COMMON /T2COMC/ TDDATE(8),CARD(160),INPFMT(16),PXNAME(8),INAME(3)
      CHARACTER*1 TDDATE,INAME
      CHARACTER*1 CARD,INPFMT,PXNAME
      COMMON /TOKENC/ INTERP,INTEG,FLOTNG,KEYORD,NSTRNG,MAXSTR
      REAL FLOTNG
      INTEGER MAXSTR,INTERP,KEYORD,NSTRNG
      INTEGER   INTEG
      COMMON /TOK2/ STRING(160)
      CHARACTER*1 STRING
      COMMON /EXPAU/ LPAUSE
      LOGICAL LPAUSE
      CHARACTER*1 LINE(120)
C
C Format for reading input line.
C
c      INTEGER LINFO(9)
C
C Write introductory note.
C
 10   FORMAT(120 A1)
      WRITE (6,20) TDDATE
C
C Write date or version.
C
 20   FORMAT (   ' Top Drawer  ',8A1)
      CALL T2PNTR
      NPLOTS=0
C
C Reset flags.
C
 30   NPLOTS=NPLOTS+1
      CALL TRACER (4)
C
C Interactive.
C
      CALL T2REST
      CALL TRACER (5)
C
C Error file empty.
C
      FLAGS(57)=.FALSE.
      FLAGS(23)=.FALSE.
      CALL TRACER (9)
C
C Flush the buffers.
C
      CALL T2MAIN (ICODE)
C
C Close device to flush buffer.
C
      IF (FLAGS(54).AND.FLAGS(24)) CALL TXXMT
      IF (FLAGS(59)) CALL TXINTR       ! Interactive devices
C
      IF (.NOT.(FLAGS(16)).AND.FLAGS(53)) THEN
        WRITE (6,90) NPLOTS
 90     FORMAT (    5H PLOT,I3,    6H DONE.)
      ENDIF
C
C Error file has been used.
C
      CALL TRACER (13)
      IF (FLAGS(23)) THEN
        CALL TRACER (20)
        REWIND 86
C        IF (.NOT.FLAGS(53)) CALL ZZWAIT
        WRITE (6,40)
 40     FORMAT (   34H ERROR MESSAGES TO COME.          )
C
C Maximum lines to list.
C
 50     CALL ZZWAIT
        DO 70 I=1,10
        CALL TRACER (26)
        READ (86,10,END=80) LINE
        CALL TRACER (30)
        WRITE (6,60) LINE
 60     FORMAT (1X,120 A1)
 70     CONTINUE
        CALL TRACER (35)
        GO TO 50
 80     CALL TRACER (40)
        REWIND 86
      ENDIF
C
      IF (FLAGS(83)) CALL ZZWAIT         ! Slave devices
C
C Disasterous error.
C
c
c---------------- Modificato da P. Nason il 8-1-92
c Sembra che le due prossime righe causino problemi quando ci sono
c molti plot in modo interattivo, e che inoltre causino il prompt
c "Enter C.R. to go on" anche per output sequenziali (in files).
c Commentate via (con il corrispondente endif) l'8-1-92 da P. Nason
c      IF (FLAGS(53)) THEN                            ! commentate
c        IF (.NOT.FLAGS(16) .AND. LPAUSE) CALL ZZWAIT  ! via
C
C If end of file on input.
C
        IF (INFO(1).GE.0) THEN
          CALL TXNEXT
          GO TO 30
        ENDIF
c      ENDIF                                          ! commentato via
      CALL TXEND
      CALL NICEST
c      STOP
      END
C
C  ROUTINE TO PROVIDE INTERPRETATION OF A CHARACTER STRING.
C
C  VARIABLES PASSED AS ARGUEMENTS OF CALL --
C    INFO ARRAY
C       (1)  LENGTH OF 'CHARS' STRING (NOT CHANGED)
C               TOKRED EXPECTS ZERO TO START, SETS IT TO -1 ON
C               END OF INPUT FILE.
C       (2)  LENGTH OF 'CHARS' TO INTERPRET (NOT CHANGED)
C       (3)  ON INPUT: POINTER TO LAST CHARACTER OF PREVIOUS
C                 FIELD (0 FOR NEW CARD)
C            ON OUTPUT: -> LAST CHARACTER OF FIELD JUST INTERPRETED
C       (4)  ON OUTPUT: -> CHARACTER BEFORE FIRST OF CURRENT FIELD
C       (5)  ON OUTPUT: -> FIRST CHARACTER IF ERRONIOUS FIELD, OR 0
C    (OTHER ELEMENTS IN THE INFO ARRAY, USED BY TD, ARE:
C       (6)  ERROR COUNTER (NOT USED) (INCREMENTED BY T2ERR)
C       (7)  CARD ID (NUMBER, WYLBUR LINE, WHATHAVEYOU.)
C                 (NOT USED) (INCREMENTED BY TOKRED)
C       (8)  (NOT USED)
C            -> COLUMN CONTAINING PREVIOUS END-OF-CARD CHARACTER
C             OR ZERO IF CURRENT 'CARD' STARTED IN COLUMN 1.
C       (9)  (A FLAG FOR TOKRED) REREAD THIS SAME CARD
C       (10) (A FLAG FOR TOKERR) THIS CARD HAS BEEN PRINTED
C
C    CHARS--THE INPUT STRING ITSELF.
C
C    KEYS--KEYWORDS TO COMPARE TO CURRENT INFO FIELD.
C        LOWER CASE KEYWORDS WILL NOT BE MATCHED.  IN GENERAL,
C        THE REQUIREMENTS ON WRITING A KEYWORD STRING ARE
C        MEANER THAN THOSE FOR CHARS INPUT.
C       KEYWORD FIELDS ARE SEPARATED BY ANY SEPARATOR, AND LIST
C       IS TERMINATED BY END-OF-CARD CHARACTER.
C       A KEYWORD FIELD CONTAINS A KEYWORD (CONTAINING ANY
C       CHARACTER WITH TYPE < 6), WHICH MAY BE DIRECTLY FOLLOWED BY
C       A COLON AND AN UNSIGNED INTEGER, SPECIFYING THE
C       KEYWORD FLAG. (RETURNED IN -INTEG-)
C       MATCHED TOKENS WHICH WOULD BE AMBIGUOUS, ARE NOT
C       AMBIGUOUS IF ALL KEYWORDS THEY MATCH HAVE THE SAME
C       NON-ZERO KEYWORD FLAG.  THUS, 'RI' IS AMBIGUOUS IN
C       THE KEYWORD STRING 'RIGHT,RITE;', BUT NOT IN 'RIGHT:1,RITE:1;'
C
C   VARIABLES (ALL OUTPUT, EXCEPT MAXSTR) PASSED IN THE
C      COMMON BLOCK /TOKENC/   --------------------
C
C    INTERP  OUTPUT INTERPRETATION
C      1  END OF CARD
C           INTEG  NOT RELEVANT
C           FLOTNG NOT RELEVANT
C           KEYORD NOT RELEVANT
C           STRING NOT RELEVANT
C      2  KEYWORD MATCHED.
C           INTEG  KEYWORD FLAG. (OR ZERO, IF NONE)
C           FLOTNG NOT RELEVANT
C           KEYORD ORDINAL NUMBER OF MATCHED KEYWORD
C           STRING MATCHED KEYWORD
C      3  INTEGER.
C           INTEG  VALUE
C           FLOTNG FLOAT(VALUE)
C           KEYORD NOT RELEVANT
C           STRING FIELD FROM INFO
C      4  FLOATING POINT NUMBER.
C           INTEG  INT(VALUE)
C           FLOTNG VALUE
C           KEYORD NOT RELEVANT
C           STRING FIELD FROM INFO
C      5  QUOTED STRING (STARTED BY EITHER APOSTROPHE
C             OR QUOTE, TERMINATED BY SAME CHARACTER.
C             IMBEDDED CHARACTER MUST BE DOUBLED.)
C           INTEG  NOT RELEVANT
C           FLOTNG NOT RELEVANT
C           KEYORD NOT RELEVANT
C           STRING STRING, WITH NO LEADING QUOTE, AND
C                  DOUBLED QUOTES SINGLED
C      6  NOT RECOGNIZED. (LEADING ALPHA)
C            TREATED AS QUOTED STRING, TO NEXT SEPARATOR.
C      7  NOT RECOGNIZED (ILLEGAL NUMBER OR NON-ALPHAMERIC
C            STARTING CHARACTER)
C            TREATED AS QUOTED STRING, TO NEXT SEPARATOR.
C      8  AMBIGUOUS KEYWORD MATCH
C           INTEG HAS ORDINAL NUMBER OF FIRST MATCH
C           FLOTNG NOT RELEVANT
C           KEYORD ORDINAL NUMBER OF FIRST MATCHED KEYWORD
C           STRING HAS LIST OF MATCHED KEYWORDS
C      9  SPECIAL CHARACTER (E.G. '?')
C             ONLY ONE CHARACTER IS RETURNED.  SPECIAL
C             CHARACTERS TERMINATE A FIELD AND ARE NOT
C             INCLUDED EXCEPT AS THE FIRST CHARACTER.
C           INTEG  NOT RELEVANT
C           FLOTNG NOT RELEVANT
C           KEYORD NOT RELEVANT
C           STRING THE CHARACTER
C    INTEG   INTEGER OUTPUT
C    FLOTNG  FLOATING POINT OUTPUT
C    KEYORD  ORDINAL NUMBER OF MATCHED KEYWORD
C    MAXSTR  LENGTH (IN CHARACTERS) OF STRING ARRAY (TO BE
C               SET IN CALLING PROGRAM)
C    NSTRNG  CHARACTER COUNT FOR STRING
C    STRING  STRING OUTPUT.
C "
C
C  "TYPTAB IS TABLE OF TYPES FOR EACH POSSIBLE CHARACTER
C   CHARACTER TYPES---
C     1 -- DIGIT
C     2 -- DECIMAL POINT
C     3 -- SIGN (+ OR -)
C     4 -- E OR D (FOR EXPONENT)
C     5 -- LETTERS OTHER THAN D,E AND SPECIAL CHARACTERS
C     6 -- BLANK
C     7 -- SEPARATORS  !,=$|/
C     8 -- END OF CARD ; Z00  ZFF
C     9 -- DELIMITER FOR QUOTED STRING (APOSTROPHE AND QUOTE)
C    10 -- START OF COMMENT (
C    11 -- END OF COMMENT )
C    12 -- COLON (SEPARATOR FOR KEYWORD AND KEYFLAG)
C    13 -- ERROR (ILLEGAL CHARACTER)
C    14 -- SPECIAL CHARACTER
      SUBROUTINE TOKEN (INFO,CHARS,KEYS)
      INTEGER INFO(5)
      CHARACTER*1 CHARS(*)
      CHARACTER*1 KEYS(*)
      COMMON /TOKENC/ INTERP,INTEG,FLOTNG,KEYORD,NSTRNG,MAXSTR
      REAL FLOTNG
      INTEGER MAXSTR,INTERP,KEYORD,NSTRNG
      INTEGER   INTEG
      COMMON /TOK2/ STRING(160)
      CHARACTER*1 STRING
      COMMON /TOKENT/ TYPTAB
      INTEGER   TYPTAB(256)
      INTEGER DEC256(256),DECIDE(16,16)
      INTEGER CHARAC,KEYLET,QUOTE,MINUS,ZERO
      INTEGER FLTING,STAGE,OLDSTG,CHTYPE,KEYTYP
      INTEGER KEYPNT,ICOL,EXPON,ESIGN,NSIGN,DIGITS
      CHARACTER*1 CBLANK
      REAL XPA(4),XPB(10),XPC(10)
      INTEGER   IVALUE,ILONG,ALLBIT,TOOBIG,LONG10
      EQUIVALENCE (DECIDE(1,1),DEC256(1))
      DATA ALLBIT/2 147 483 647/, TOOBIG/214 748 364/, LONG10/10/
      DATA CBLANK /     ' '/
      DATA XPA(1)/1./
      DATA XPB(1)/1.E00/,XPB(2)/1.E10/,XPB(3)/1.E20/,XPB(4)/1.E30/
 
C*VAX
C*VAX FOR A VAX SET  DATA XPB(5)/0./,XPB(6)/0./,XPB(7)/0./,XPB(8)/0./
C*VAX
c      DATA XPB(5)/0./,XPB(6)/0./,XPB(7)/0./,XPB(8)/0./
      DATA XPB(5)/0./,XPB(6)/0./,XPB(7)/0./,XPB(8)/0./
c Actually the line below does not work for SunOS and AIX either
c      DATA XPB(5)/1.e40/,XPB(6)/1.e50/,XPB(7)/1.e60/,XPB(8)/1.e70/
      DATA XPB(9)/0. 0/,XPB(10)/0. 0/
      DATA XPC(1)/1.E0/,XPC(2)/1.E1/,XPC(3)/1.E2/,XPC(4)/1.E3/
      DATA XPC(5)/1.E4/,XPC(6)/1.E5/,XPC(7)/1.E6/,XPC(8)/1.E7/
      DATA XPC(9)/1.E8/,XPC(10)/1.E9/
      DATA MINUS/45/,ZERO/48/
      DATA DEC256 /   02,05,04,02,02,01,01,17, 13,15,25,25,25,18,26,26,
     1 02,02,02,02,02,29,29,30, 30,15,30,30,30,30,26,26,  06,05,04,27,27
     2,26,26,17, 13,15,25,25,25,26,26,26,  06,05,25,25,25,25,25,25, 25,1
     35,25,25,25,25,26,26,  08,25,25,25,25,25,25,25, 25,15,25,25,25,25,2
     46,26,  06,07,10,09,25,19,19,20, 20,15,20,20,20,20,26,26,  08,25,10
     5,09,20,19,19,20, 20,15,20,20,20,20,26,26,  08,25,10,09,20,19,19,20
     6, 20,15,20,20,20,20,26,26,  11,25,10,25,25,09,25,25, 25,15,25,25,2
     75,25,26,26,  11,25,25,25,25,10,25,25, 25,15,25,25,25,25,26,26,  11
     8,20,20,20,20,19,20,20, 20,15,25,25,25,20,26,26,  12,12,12,12,12,21
     9,21,22, 22,15,12,12,12,22,26,26,  14,14,14,14,14,14,14,28, 23,14,1
     X4,14,14,14,26,26,  14,14,14,14,14,14,14,28, 23,14,14,14,14,14,26,2
     X6,  16,16,16,16,16,16,16,16, 16,16,24,16,16,16,26,26,  16,16,16,16
     X,16,16,16,16, 16,16,24,16,16,16,26,26/
      DATA MINMCH/2/
      NSTR=0
      INTERP=0
      INTEG=0
      NSMAX=0
      KEYPNT=0
      KEYORD=0
      KEYFLG=0
      NKEY=1
      NMATCH=0
      FLTING=0
      IVALUE=0
      DIGITS=0
      EXPON=0
      STAGE=1
      KEY1=1
      NSIGN=1
      KEYSEP=0
      ESIGN=1
      ICOL=MAX0(0,INFO(3))
      INFO(4)=ICOL
      MAXS=MAX0(20,MAXSTR)
      MAXCOL=133
      IF (INFO(1).GT.0) MAXCOL=MIN0(MAXCOL,INFO(1))
      IF (INFO(2).GT.0) MAXCOL=MIN0(MAXCOL,INFO(2))
 10   ICOL=ICOL+1
      IF (ICOL.LE.MAXCOL) THEN
        CHARAC=ICHAR(CHARS(ICOL))
      ELSE
        CHARAC=0
      ENDIF
      CHTYPE=TYPTAB(CHARAC+1)
      IF (NSTR.LT.MAXS) THEN
        NSTR=NSTR+1
        STRING(NSTR)=CHARS(ICOL)
      ENDIF
      OLDSTG=STAGE
      STAGE=DECIDE(CHTYPE,STAGE)
      IF (CHARAC.GE.97.AND.CHARAC.LE.122) CHARAC=CHARAC-32
      GO TO (50,130,20,170,200,190,200,180,200,210,220,10,240,270,280,29
     10,310,320,340,330,380,370,260,300,230,20,160,250,70,60),STAGE
 20   WRITE (6,30) INFO
 30   FORMAT(   20H TOKEN ERROR...INPUT,9I4)
C$    WRITE (6,40) ICOL,CHARAC,CHTYPE,STAGE,OLDSTG
C$ 40 FORMAT (   32H ICOL,CHARAC,CHTYPE,STAGE,OLDSTG,5Z9)
      STOP 741
 50   INFO(4)=ICOL
      NSTR=0
      GO TO 10
 60   ICOL=ICOL-1
 70   KEYPNT=KEYPNT+1
      KEYLET=ICHAR(KEYS(KEYPNT))
      KEYTYP=TYPTAB(KEYLET+1)
      KEYSEP=KEYSEP+1
      IF (KEYTYP.GE.6) NMATCH=1000
 80   IF (KEYTYP.LT.6) THEN
        KEYPNT=KEYPNT+1
        KEYLET=ICHAR(KEYS(KEYPNT))
        KEYTYP=TYPTAB(KEYLET+1)
        GO TO 80
      ENDIF
      KEY2=KEYPNT
      KYFOLD=KEYFLG
      KEYFLG=0
      IF (KEYTYP.EQ.12) THEN
 90     KEYPNT=KEYPNT+1
        KEYLET=ICHAR(KEYS(KEYPNT))
        KEYTYP=TYPTAB(KEYLET+1)
        IF (KEYTYP.EQ.1) THEN
          KEYFLG=10*KEYFLG+(KEYLET-ZERO)
          GO TO 90
        ENDIF
 100    IF (KEYTYP.LT.6) THEN
          KEYPNT=KEYPNT+1
          KEYLET=ICHAR(KEYS(KEYPNT))
          KEYTYP=TYPTAB(KEYLET+1)
          GO TO 100
        ENDIF
      ENDIF
      IF (NMATCH.GE.MINMCH) THEN
        IF ((KEYORD.NE.0).AND.((NMATCH.NE.1000).AND.(KEYFLG.NE.KYFOLD.OR
     1.KEYFLG.EQ.0))) THEN
          INTERP=8
          NSTR=NSMAX
          INFO(5)=INFO(4)+1
        ELSE
          KEYORD=NKEY
          INTERP=2
          NSTR=0
          INFO(5)=0
          INTEG=KEYFLG
        ENDIF
        DO 110 I=KEY1,KEY2
        IF (NSTR.GE.MAXS) GO TO 120
        NSTR=NSTR+1
        STRING(NSTR)=KEYS(I)
 110    CONTINUE
 120    NSMAX=NSTR
        IF (NMATCH.EQ.1000) GO TO 390
        ICOL1=ICOL
      ELSE
        NSTR=NSMAX
      ENDIF
      ICOL=INFO(4)
      KEYPNT=KEYPNT-1
      STAGE=OLDSTG
      GO TO 10
 130  KEYPNT=KEYPNT+1
      KEYLET=ICHAR(KEYS(KEYPNT))
      KEYTYP=TYPTAB(KEYLET+1)
      KEYSEP=KEYSEP+1
      IF ((KEYTYP.GE.6).AND.(KEYTYP.EQ.12)) THEN
 140    KEYPNT=KEYPNT+1
        KEYLET=ICHAR(KEYS(KEYPNT))
        KEYTYP=TYPTAB(KEYLET+1)
        IF (KEYTYP.LT.6) GO TO 140
      END IF
      IF (KEYTYP.GE.6) THEN
        IF (KEYTYP.GE.8) THEN
          NSTR=NSMAX
          IF (INTERP.NE.0) THEN
            ICOL=ICOL1
            GO TO 390
          END IF
          STAGE=3
        ENDIF
        ICOL=INFO(4)
        IF (KEYSEP.GT.1) NKEY=NKEY+1
        KEYSEP=0
        KEY1=KEYPNT+1
        NMATCH=0
        GO TO 10
      ENDIF
      IF (KEYLET.EQ.CHARAC) THEN
        NMATCH=NMATCH+1
        GO TO 10
      ENDIF
      ICOL=INFO(4)
 150  KEYPNT=KEYPNT+1
      KEYLET=ICHAR(KEYS(KEYPNT))
      KEYTYP=TYPTAB(KEYLET+1)
      IF ((KEYTYP.GE.6).AND.(KEYTYP.NE.12)) THEN
        KEYPNT=KEYPNT-1
        GO TO 10
      ELSE
        GO TO 150
      ENDIF
 160  INTERP=6
      INFO(5)=ICOL
      STAGE=12
      GO TO 10
 170  IF (CHARAC.EQ.MINUS) NSIGN=-1
      GO TO 10
 180  DIGITS=DIGITS-1
 190  IF (IVALUE.GE.TOOBIG) THEN
        DIGITS=DIGITS+1
        GO TO 10
      ENDIF
      ILONG=IVALUE+IVALUE
      ILONG=ILONG+ILONG+IVALUE
      IVALUE=ILONG+ILONG+(CHARAC-ZERO)
      GO TO 10
 200  FLTING=1
      GO TO 10
 210  IF (CHARAC.EQ.MINUS) ESIGN=-1
      GO TO 10
 220  EXPON=EXPON*10+(CHARAC-ZERO)
      GO TO 10
 230  INTERP=7
      INFO(5)=ICOL
      STAGE=12
      GO TO 10
 240  NSTR=0
      NQUOTE=0
      ISAVE=STAGE
      QUOTE=CHARAC
      GO TO 10
 250  STAGE=ISAVE
      IF (ICOL.LE.MAXCOL) GO TO 270
      CHARAC=QUOTE
      NSTR=NQUOTE+1
 260  IF (CHARAC.EQ.QUOTE) THEN
        ICOL=ICOL+1
        IF (ICOL.LE.MAXCOL) THEN
          CHARAC=ICHAR(CHARS(ICOL))
        ELSE
          CHARAC=0
        ENDIF
        CHTYPE=TYPTAB(CHARAC+1)
        IF (CHARAC.NE.QUOTE) THEN
          ICOL=ICOL-1
          INTEG=NSTR-1
          INTERP=5
          GO TO 390
        ENDIF
      ENDIF
      STAGE=ISAVE
 270  IF (CHTYPE.NE.6) NQUOTE=NSTR
      GO TO 10
 280  ISAVE=OLDSTG
      GO TO 10
 290  IF (ICOL.LT.MAXCOL) GO TO 10
 300  STAGE=ISAVE
      GO TO 10
 310  NSTR=1
      ICOL=ICOL-1
      INTERP=1
      GO TO 390
 320  INTERP=9
      INTEG=1
      NSTR=2
      STRING(1)=CHARS(ICOL)
      GO TO 390
 330  ICOL=ICOL-1
 340  DIGITS=DIGITS+ISIGN(EXPON,ESIGN)
      IF (NSIGN.LT.0) IVALUE=-IVALUE
      IABDIG=IABS(DIGITS)
      FLOTNG=FLOAT(IVALUE)
      J1=IABDIG/100
      J2=MOD(IABDIG/10,10)
      J3=MOD(IABDIG,10)
      Y=XPA(J1+1)*XPB(J2+1)*XPC(J3+1)
      IF (DIGITS.LT.0) THEN
        FLOTNG=FLOTNG/Y
      ELSE
        FLOTNG=FLOTNG*Y
      ENDIF
      IF (DIGITS.GT.0) THEN
        DO 350 J1=1,DIGITS
        IF (IVALUE.GE.TOOBIG) THEN
          IVALUE=ALLBIT
        ELSE
          ILONG=IVALUE+IVALUE
          ILONG=ILONG+ILONG+IVALUE
          IVALUE=ILONG+ILONG
        ENDIF
 350    CONTINUE
      ELSE
      IF (DIGITS.LT.0) THEN
        DO 360 J1=1,IABDIG
        IVALUE=IVALUE/LONG10
 360    CONTINUE
        ENDIF
      ENDIF
      INTEG=IVALUE
      INTERP=3+FLTING
      GO TO 390
 370  ICOL=ICOL-1
 380  IF (STAGE.EQ.0) GO TO 10
 390  STRING(NSTR)=CBLANK
      NSTRNG=NSTR-1
      INFO(3)=ICOL
      RETURN
      END
      SUBROUTINE TOKRED (INFO,CHARS,LENGTH,INFILE,FORMT)
      INTEGER INFO(10),LENGTH,INFILE,FORMT(4)
      CHARACTER*1 CHARS(LENGTH)
      COMMON /TOKENT/ TYPTAB
      INTEGER   TYPTAB(256)
      INTEGER CHARAC,CHTYPE,MAXCOL,ICOL
      CHARACTER*1 SLASH
      DATA SLASH/     '/'/
      MAXCOL=MAX0(0,INFO(1))
      ICOL=MAX0(0,INFO(3))
      IF (INFO(1).GE.0) THEN
        IF (INFO(9).NE.0) THEN
          ICOL=INFO(8)
          INFO(9)=0
        ELSE
 10       IF (ICOL.GE.MAXCOL) THEN
  20          READ (INFILE,FMT=15,END=90) CHARS
            IF (CHARS(1).NE.SLASH) GO TO 30
  15        FORMAT (160A1)
            IF (CHARS(2).NE.SLASH) GO TO 30
            GO TO 20
 30         INFO(1)=LENGTH
            IF (INFO(2).LE.0) INFO(2)=LENGTH
            INFO(7)=INFO(7)+1
            INFO(9)=0
            INFO(10)=0
            ICOL=0
            CHTYPE=8
          ELSE
            ICOL=ICOL+1
            IF (ICOL.LE.MAXCOL) THEN
              CHARAC=ICHAR(CHARS(ICOL))
            ELSE
 50           CHARAC=0
            END IF
 60         CHTYPE=TYPTAB(CHARAC+1)
 70         IF (CHTYPE.NE.8) GO TO 10
          END IF
        END IF
        INFO(3)=ICOL
        INFO(8)=ICOL
        INFO(4)=0
        INFO(5)=0
        INFO(6)=0
        RETURN
      END IF
 90   IF (INFO(1).EQ.0) THEN
        WRITE (6,100)
 100    FORMAT(   30H *** WARNING *** NO INPUT FILE)
        STOP 111
      END IF
      INFO(1)=-1
      INFO(3)=0
      INFO(4)=0
      INFO(5)=0
      INFO(9)=0
      INFO(10)=0
      RETURN
      END
      BLOCK DATA
      COMMON /TOKENT/ TYPTAB
      INTEGER   TYPTAB(256)
      DATA TYPTAB  / 08,13,13,13,13,13,13,13, 13,13,13,13,13,13,13,13,
     113,13,13,13,13,13,13,13, 13,13,13,13,13,13,13,13,  06,13,09,13,07,
     213,13,09, 10,11,13,03,07,03,02,07,  01,01,01,01,01,01,01,01, 01,01
     3,12,08,13,07,13,13,  13,05,05,05,04,04,05,05, 05,05,05,05,05,05,05
     4,05,  05,05,05,05,05,05,05,05, 05,05,05,13,13,13,13,13,  13,05,05,
     505,04,04,05,05, 05,05,05,05,05,05,05,05,  05,05,05,05,05,05,05,05,
     6 05,05,05,13,13,13,13,08,  128*13/
      END
C  "THIS ROUTINE FINDS EXTREM=(MIN,MAX) AS THE LIMITS FOR THE
C   AXIS.  INPUT VALUES ARE
C      VALS      ARRAY OF INPUT VALUES
C                  TO ACCOMMODATE 3D (MESH) INPUT, VALS IS
C                  DIMENSIONED INCRMT,NP, AND I USE ONLY THE VALUES
C                  VALS(J,I), FOR J=MINPNT,MAXPNT AND I=1,NP
C      ERRS      ARRAY OF INPUT ERRORS, OR 'NONE'
C                  DIMENSION AND RANGE IS SAME AS VALS
C      NP        NUMBER OF INPUT VALUES
C      ISCAL     USUAL SCALING INDICATOR
C          *** ISCAL MAY BE CHANGED FROM -2 TO -3 IF NECESSARY
C      IFLAG     INDICATOR FOR VALUES
C                  1--VALUE IS FIXED
C                  0--VALUE EXISTS BUT CAN BE CHANGED
C                 -1--VALUE DOES NOT (YET) EXIST
C      EXTREM    (MIN,MAX) VALUES.  STATUS IS PROVIDED IN IFLAG
C           "VMAX AND VPMN ARE MAX AND MIN OVER ALL POSITIVE VALUES.
C            VMMX AND VMIN ARE MAX AND MIN FOR NEGATIVE VALUES.
C            (VMAX ENDS UP AT LARGEST VALUE.  VMIN ENDS AT
C            SMALLEST VALUE.  IF ANY VALUE WAS POSITIVE, VPMN
C            ENDS SMALL AND POSITIVE.  IF ANY VALUE WAS NEGATIVE,
C            VMMX ENDS NEAR ZERO AND NEGATIVE.
C        "SET UP LIMITS, USING ARRAY LIMITS AND PROVIDED LIMITS"
C        "LIMS ARE POSITIVE IF
C            1--EITHER LIM IS POSITIVE AND REQUIRED
C            2--NO LIM IS REQUIRED AND THERE ARE ANY POSITIVE VALUES
C         LIMS ARE NEGATIVE IF
C            1--EITHER LIM IS NEGATIVE AND REQUIRED
C            2--NO LIM IS REQUIRED AND THERE ARE NO POSITIVE VALUES
C         LIMS ARE ARBITRARY IF
C            1--NO LIMS REQUIRED AND NO NON-ZERO VALUES
C         ERROR IF
C            1--BOTH LIMS REQUIRED AND RANGE CONTAINS ZERO
      SUBROUTINE T2STL1 (VALS,ERRS,NP,ISCAL,IFLAG,EXTREM,INCRMT,MINPNT,M
     1AXPNT)
      INTEGER ISCAL,NP,IFLAG(2),MINPNT,MAXPNT
      REAL VALS(INCRMT,1),ERRS(INCRMT,1),EXTREM(2)
      COMMON /T2FLGC/ FLAGS(100)
      LOGICAL   FLAGS
      COMMON /T2COM/            REDUCE(2), TITX, TITY, BUFFER(400), XFRM
     112(12), XFRM13(12), XFRM14(12), XFRM23(12), XFRM24(12), XFRM34(6),
     2 XVARSX(20), TSIZDF,         GRDSIZ, CIRSIZ(2,3), ASIZE, AFLARE, S
     3MSZDF, ORAXES(3), EYEDIR(3), VUEDIR(3), VUECEN(3), VERTCL(3), EYES
     4EP, SCRD, SCRZ, EYEDIS, EYEPNT(3), XYPART(2, 2), BARSIZ(3), TIKSIZ
     5(3), XYZBAS(3), TTHETA, TPHI, FRELBL(4), DOTWID, DOTWDF, PATRN(10)
     6, SCLPRM(10,3), WINDOW(4), SCREEN(2),                 FACTXY(15),
     7LBLSIZ, SYMSIZ, XYZLIM(3,2), WORLD(3), RADRMX,RADRMN,RADAMN,RADANG
     8,RADX0,RADY0,XUNUSD(6), USRKBD,USRSCR, PLTFIL, ITITDT(3),
     9   LINTEX, ERRFIL, OUTFIL, DBGFIL, INPFIL, LENCRD,SYMBOL,      GRD
     XTYP, LINWID, LINCOL, TITCON(5), ITCNTR, IUNUSD,GRDSYM,    NXYZ1(3)
     X, NXYZ2(3), LBLCHR(3), LINWDF, NPATRN, NDIMNS(3), IBLKTP, IDIMNS,
     XLDEVCE, NMESH1, NMESH2, MESH1, MESH2, MESH3, NFIELD, IFIELD(15), N
     XINCR, LISTPT, NNAME, NHEADR, IVARBL(15), IVRPTR(15), LINEAR(4), NO
     XNLIN(4), INFO(10),NOSYMB,     NPOINT, BUFSIZ, LETSET, MXECHO, LINE
     XS, MAXLNS
      REAL WINDOW,SCREEN,SYMSIZ,LBLSIZ,GRDSYM, GRDSIZ,BUFFER,REDUCE,SYMB
     1OL,NOSYMB, FRELBL,SCLPRM,TIKSIZ,BARSIZ,XYZBAS, CIRSIZ,TITX,TITY,VU
     2EDIR,EYEPNT,XYPART, ORAXES,EYEDIR,VUECEN,VERTCL,SCRD,SCRZ,
     3EYESEP,EYEDIS
      INTEGER       NXYZ1,NXYZ2,LBLCHR,NONLIN,LINEAR, ERRFIL,OUTFIL,DBGF
     1IL, LENCRD,BUFSIZ,IFIELD, ITITDT,ITCNTR, USRKBD,USRSCR,PLTFIL, DOT
     2WID,DOTWDF,LINWID,LINWDF, GRDTYP,LETSET, NDIMNS,IBLKTP,IDIMNS,NINC
     3R,LISTPT, IVARBL,NNAME,NHEADR,IVRPTR
      COMMON /T2COMC/ TDDATE(8),CARD(160),INPFMT(16),PXNAME(8),INAME(3)
      CHARACTER*1 TDDATE,INAME
      CHARACTER*1 CARD,INPFMT,PXNAME
      COMMON /TOKENC/ INTERP,INTEG,FLOTNG,KEYORD,NSTRNG,MAXSTR
      REAL FLOTNG
      INTEGER MAXSTR,INTERP,KEYORD,NSTRNG
      INTEGER   INTEG
      COMMON /TOK2/ STRING(160)
      CHARACTER*1 STRING
      DATA HNONE/-32767/
      IF ((IFLAG(1).NE.1).OR.(IFLAG(2).NE.1).OR.(NP.GT.0).OR.(VALS(1,1).
     1NE.HNONE)) THEN
        IF (ERRS(1,1).EQ.HNONE) THEN
          IE=0
          ERR=0.
        ELSE
          IE=1
        ENDIF
        IF ((ISCAL.EQ.(-2)).OR.(ISCAL.EQ.(-3))) THEN
C*VAX
C*VAX  FOR A VAX SET    VPMN=1.E30      VMMX=-1.E30
C*VAX
          VPMN=1.E30
          VMMX=-1.E30
          VMIN=0.
          VMAX=0.
          DO 40 I=1,NP
            DO 30 J=MINPNT,MAXPNT
              IF (IE.NE.0) ERR=ABS(ERRS(J,I))
              V=VALS(J,I)
              VP=V+ERR
              VM=V-ERR
              IF (VMAX.LT.VP) VMAX=VP
              IF (VMIN.GT.VM) VMIN=VM
              IF (V) 10,30,20
 10           IF (VP.GE.0.) VP=V*0.25
              IF (VMMX.LT.VP) VMMX=VP
              GO TO 30
 20           IF (VM.LE.0.) VM=V*0.25
              IF (VPMN.GT.VM) VPMN=VM
 30         CONTINUE
 40       CONTINUE
          IF (FLAGS(29)) THEN
            WRITE (DBGFIL,50) VPMN,VMAX,VMIN,VMMX
 50         FORMAT (63X,   14H(LOG)EXTREMA  ,4G10.3)
          ENDIF
          SN=0.
          IF (IFLAG(1).GT.0) THEN
            VMIN=EXTREM(1)
            SN=SIGN(1.,VMIN)
          ENDIF
          IF (IFLAG(2).GT.0) THEN
            IF (SN*EXTREM(2).LT.0.) STOP 770
            VMAX=EXTREM(2)
            SN=SIGN(1.,VMAX)
          ENDIF
          IF (SN.EQ.0.) THEN
            IF (IFLAG(1).EQ.0) THEN
              IF (EXTREM(1).GT.0.) SN=1.
            ENDIF
            IF (IFLAG(2).EQ.0) THEN
              IF (EXTREM(2).GT.0.) SN=1.
            ENDIF
            IF (VMAX.GE.VPMN) THEN
              SN=1.
            ENDIF
          ENDIF
          IF (SN.EQ.0.) THEN
            IF (IFLAG(1).EQ.0) THEN
              IF (EXTREM(1).LT.0.) SN=-1.
            ENDIF
            IF (IFLAG(2).EQ.0) THEN
              IF (EXTREM(2).LT.0.) SN=-1.
            ENDIF
            IF (VMIN.LE.VMMX) THEN
              SN=-1.
            ENDIF
          END IF
          IF (SN.EQ.0.) THEN
            ISCAL=-2
            VMIN=0.5
            VMAX=2.
            GO TO 90
          ENDIF
          IF (SN.GT.0.) THEN
            VMIN=VPMN
            ISCAL=-2
          ELSE
            VMAX=VMMX
            ISCAL=-3
          ENDIF
          FAC=(VMAX/VMIN)**0.1
          IF (FAC.EQ.1.) FAC=2.
          VMAX=VMAX*FAC
          VMIN=VMIN/FAC
          IF (IFLAG(1).GT.0) VMIN=EXTREM(1)
          IF (IFLAG(1).EQ.0) THEN
            IF (SN*EXTREM(1).GT.0.) VMIN=AMIN1(VMIN,EXTREM(1))
          ENDIF
          IF (IFLAG(2).GT.0) VMAX=EXTREM(2)
          IF (IFLAG(2).NE.0) GO TO 90
          IF (SN*EXTREM(2).GT.0.) VMAX=AMAX1(VMAX,EXTREM(2))
        ELSE
          VMAX=VALS(MINPNT,1)
          VMIN=VMAX
          DO 70 I=1,NP
            DO 60 J=MINPNT,MAXPNT
              IF (IE.NE.0) ERR=ERRS(J,I)
              VP=VALS(J,I)+ERR
              VM=VALS(J,I)-ERR
              IF (VP.GT.VMAX) VMAX=VP
              IF (VM.LT.VMIN) VMIN=VM
 60         CONTINUE
 70       CONTINUE
          IF (FLAGS(29)) THEN
            WRITE (DBGFIL,80) VMIN,VMAX
 80         FORMAT (63X,   13H(LIN) EXTREMA,2G10.3)
          ENDIF
          IF (ISCAL.EQ.(-1)) THEN
            FAC=(VMAX-VMIN)*0.1
            IF (FAC.EQ.0.) FAC=ABS(VMAX)
            VMAX=VMAX+FAC
            VMIN=VMIN-FAC
            FAC=ABS(FAC)
            IF (ABS(VMAX).LE.FAC) VMAX=0.
            IF (ABS(VMIN).LE.FAC) VMIN=0.
            IF (VMAX.EQ.VMIN) THEN
              VMAX=1.
              VMIN=-1.
            ENDIF
          ENDIF
          IF (IFLAG(1).GT.0) VMIN=EXTREM(1)
          IF (IFLAG(1).EQ.0) VMIN=AMIN1(VMIN,EXTREM(1))
          IF (IFLAG(2).GT.0) VMAX=EXTREM(2)
          IF (IFLAG(2).EQ.0) VMAX=AMAX1(VMAX,EXTREM(2))
        END IF
 90     EXTREM(1)=VMIN
        IFLAG(1)=1
        EXTREM(2)=VMAX
        IFLAG(2)=1
      ENDIF
      IF (ISCAL.LE.-4.AND.ISCAL.GE.-6) THEN
        IF (IFLAG(1).GE.0) EXTREM(1)=AINT(EXTREM(1))
        IF (IFLAG(2).GE.0) EXTREM(2)=AINT(EXTREM(2))+0.99
        IF ((EXTREM(1).GE.0.0).AND.(EXTREM(2).GE.0)) GO TO 115
          WRITE (ERRFIL,100)
 100  FORMAT (   33H *** WARNING *** NEGATIVE VALUES ,     36HHAVE NO ME
     1ANING FOR A CALENDAR SCALE)
          FLAGS(23)=.TRUE.
      ELSE
        IF ((ISCAL.EQ.(-2)).OR.(ISCAL.EQ.(-3))) THEN
      IF ((EXTREM(1).EQ.0.0).OR.((EXTREM(2).EQ.0.0).OR.(SIGN(1.,EXTREM(1
     1)).NE.SIGN(1.,EXTREM(2))))) THEN
            WRITE (ERRFIL,110)
 110  FORMAT (   48H *** WARNING *** LOG SCALE MAY NOT INCLUDE ZERO.  ,
     1  13H LINEAR USED.)
            FLAGS(23)=.TRUE.
            ISCAL=-1
          ENDIF
        ENDIF
      ENDIF
 115  IF (FLAGS(29)) THEN
        WRITE (DBGFIL,120) ISCAL,EXTREM,IFLAG
 120    FORMAT (62X,   19HISCAL,EXTREMA,IFLAG,I4,2G10.3,2I2)
      ENDIF
      RETURN
      END
      SUBROUTINE ERRTRA
      RETURN
      END
      SUBROUTINE T2HIST (XVALS,YVALS,ZVALS,XERRS,YERRS,ZERRS,JTYPE,INLEV
     1L,NP,INCRMT)
      INTEGER JTYPE,INLEVL,NP
      REAL XVALS(INCRMT,NP),YVALS(INCRMT,NP),ZVALS(INCRMT,NP),  XERRS(IN
     1CRMT,NP),YERRS(INCRMT,NP),ZERRS(INCRMT,NP)
      COMMON /T2FLGC/ FLAGS(100)
      LOGICAL   FLAGS
      COMMON /T2COM/            REDUCE(2), TITX, TITY, BUFFER(400), XFRM
     112(12), XFRM13(12), XFRM14(12), XFRM23(12), XFRM24(12), XFRM34(6),
     2 XVARSX(20), TSIZDF,         GRDSIZ, CIRSIZ(2,3), ASIZE, AFLARE, S
     3MSZDF, ORAXES(3), EYEDIR(3), VUEDIR(3), VUECEN(3), VERTCL(3), EYES
     4EP, SCRD, SCRZ, EYEDIS, EYEPNT(3), XYPART(2, 2), BARSIZ(3), TIKSIZ
     5(3), XYZBAS(3), TTHETA, TPHI, FRELBL(4), DOTWID, DOTWDF, PATRN(10)
     6, SCLPRM(10,3), WINDOW(4), SCREEN(2),                 FACTXY(15),
     7LBLSIZ, SYMSIZ, XYZLIM(3,2), WORLD(3), RADRMX,RADRMN,RADAMN,RADANG
     8,RADX0,RADY0,XUNUSD(6), USRKBD,USRSCR, PLTFIL, ITITDT(3),
     9   LINTEX, ERRFIL, OUTFIL, DBGFIL, INPFIL, LENCRD,SYMBOL,      GRD
     XTYP, LINWID, LINCOL, TITCON(5), ITCNTR, IUNUSD,GRDSYM,    NXYZ1(3)
     X, NXYZ2(3), LBLCHR(3), LINWDF, NPATRN, NDIMNS(3), IBLKTP, IDIMNS,
     XLDEVCE, NMESH1, NMESH2, MESH1, MESH2, MESH3, NFIELD, IFIELD(15), N
     XINCR, LISTPT, NNAME, NHEADR, IVARBL(15), IVRPTR(15), LINEAR(4), NO
     XNLIN(4), INFO(10),NOSYMB,     NPOINT, BUFSIZ, LETSET, MXECHO, LINE
     XS, MAXLNS
      REAL WINDOW,SCREEN,SYMSIZ,LBLSIZ,GRDSYM, GRDSIZ,BUFFER,REDUCE,SYMB
     1OL,NOSYMB, FRELBL,SCLPRM,TIKSIZ,BARSIZ,XYZBAS, CIRSIZ,TITX,TITY,VU
     2EDIR,EYEPNT,XYPART, ORAXES,EYEDIR,VUECEN,VERTCL,SCRD,SCRZ,
     3EYESEP,EYEDIS
      INTEGER       NXYZ1,NXYZ2,LBLCHR,NONLIN,LINEAR, ERRFIL,OUTFIL,DBGF
     1IL, LENCRD,BUFSIZ,IFIELD, ITITDT,ITCNTR, USRKBD,USRSCR,PLTFIL, DOT
     2WID,DOTWDF,LINWID,LINWDF, GRDTYP,LETSET, NDIMNS,IBLKTP,IDIMNS,NINC
     3R,LISTPT, IVARBL,NNAME,NHEADR,IVRPTR
      COMMON /T2COMC/ TDDATE(8),CARD(160),INPFMT(16),PXNAME(8),INAME(3)
      CHARACTER*1 TDDATE,INAME
      CHARACTER*1 CARD,INPFMT,PXNAME
      COMMON /TOKENC/ INTERP,INTEG,FLOTNG,KEYORD,NSTRNG,MAXSTR
      REAL FLOTNG
      INTEGER MAXSTR,INTERP,KEYORD,NSTRNG
      INTEGER   INTEG
      COMMON /TOK2/ STRING(160)
      CHARACTER*1 STRING
C
C   DEFINE MTOD AS REAL (MONTH-TO-DAY CONVERSION FUNCTION)
C   ADDED BY WBJ 4/24/85 TO CORRECT MONTH SCALE PROBLEM
C
      REAL MTOD
      REAL WID(3),XYZ(3)
      INTEGER IE(3),IV(3)
      LOGICAL LIMSET(3)
      DATA HNONE/-32767/
      IF (NP.LE.1) RETURN
      IF (FLAGS(29)) THEN
        WRITE (DBGFIL,10) NP
 10     FORMAT(60X,   18HT2HIST CALLED WITH,I6,    7H POINTS)
        N=MIN0(NP,6)
        WRITE (DBGFIL,20) XVALS(1,1),(XVALS(1,I),I=1,N)
        WRITE (DBGFIL,20) YVALS(1,1),(YVALS(1,I),I=1,N)
        WRITE (DBGFIL,20) ZVALS(1,1),(ZVALS(1,I),I=1,N)
        WRITE (DBGFIL,20) XERRS(1,1),(XERRS(1,I),I=1,N)
        WRITE (DBGFIL,20) YERRS(1,1),(YERRS(1,I),I=1,N)
        WRITE (DBGFIL,20) ZERRS(1,1),(ZERRS(1,I),I=1,N)
 20     FORMAT(60X,A4,6G10.3)
      ENDIF
      FLAGS(4)=.TRUE.
      FLAGS(28)=.TRUE.
      DO 30 I=61,72
        FLAGS(I)=.FALSE.
 30   CONTINUE
      DO 40 I=1,3
        LIMSET(I)=FLAGS(2*I+3)
 40   CONTINUE
      CALL T2AXES
      CALL TXSCIS (2)
      IDEP=2
      IF (YVALS(1,1).EQ.HNONE.OR.FLAGS(56)) IDEP=3
      IF (.NOT.(LIMSET(IDEP)).AND.NONLIN(IDEP).EQ.-1) THEN
        IF (XYZLIM(IDEP,1).GT.0.) THEN
          XYZLIM(IDEP,1)=0.
          CALL TXDEF2
        ENDIF
      ENDIF
      ISAVE=LINTEX
      ITXTUR=JTYPE/64
      IF (ITXTUR.NE.0) LINTEX=ITXTUR
      DO 50 I=1,3
        XYZ(I)=XYZLIM(I,1)
        IV(I)=0
        IE(I)=0
 50   CONTINUE
      IF (XVALS(1,1).NE.HNONE) IV(1)=1
      IF (XERRS(1,1).NE.HNONE) IE(1)=1
      IF (YVALS(1,1).NE.HNONE) IV(2)=1
      IF (YERRS(1,1).NE.HNONE) IE(2)=1
      IF (ZVALS(1,1).NE.HNONE) IV(3)=1
      IF (ZERRS(1,1).NE.HNONE) IE(3)=1
      IV(IDEP)=0
      IF (INLEVL.EQ.0) THEN
C
C   THIS IS THE HISTOGRAM SECTION OF T2HIST
C
        IF (NONLIN(IDEP).EQ.(-1)) THEN
          XYZ(IDEP)=0.
        ELSE
          XYZ(IDEP)=XYZLIM(IDEP,1)
        ENDIF
        YSTART=XYZ(IDEP)
        MAXJ=BUFSIZ-15
        JXB=3
        IF (.NOT.FLAGS(56)) THEN
          BUFFER(JXB+1)=XYZ(1)
          BUFFER(JXB+2)=XYZ(2)
          BUFFER(JXB+3)=XYZ(3)
C
C  THE EXCLUSION OF THIS LINE SEEMS TO CORRECT THE HISTOGRAM PLOT
C         JXB=JXB+3
C  WITH THE SIDEWAYS OPTION. (JOEYW 7/12/85)
C
        ENDIF
C
C   MODIFIED BY WBJ 4/24/85 TO CORRECT MONTH SCALES ERROR
C
C   THIS SECTION CALCULATES THE BEGINNING EDGE OF THE HISTOGRAM
C
        IF (IV(1).NE.0) THEN
          IF(NONLIN(1).EQ.-4) THEN
            DELTA=(MTOD(XVALS(1,2))-MTOD(XVALS(1,1)))*0.01
            XYZ(1)=2*XVALS(1,1)-DELTA
          ELSE
            XYZ(1)=3.*XVALS(1,1)-XVALS(1,2)
          ENDIF
          IF (IE(1).NE.0) XYZ(1)=XYZ(1)+XERRS(1,2)-XERRS(1,1)
          XYZ(1)=0.5*XYZ(1)
        ENDIF
        IF (IV(2).NE.0) THEN
          IF(NONLIN(2).EQ.-4) THEN
            DELTA=(MTOD(YVALS(1,2))-MTOD(YVALS(1,1)))*0.01
            XYZ(2)=2*YVALS(1,1)-DELTA
          ELSE
            XYZ(2)=3.*YVALS(1,1)-YVALS(1,2)
          ENDIF
          IF (IE(2).NE.0) XYZ(2)=XYZ(2)+YERRS(1,2)-YERRS(1,1)
          XYZ(2)=0.5*XYZ(2)
        ENDIF
        IF (IV(3).NE.0) THEN
          IF(NONLIN(3).EQ.-4) THEN
            DELTA=(MTOD(ZVALS(1,2))-MTOD(ZVALS(1,1)))*0.01
            XYZ(3)=2*ZVALS(1,1)-DELTA
          ELSE
            XYZ(3)=3.*ZVALS(1,1)-ZVALS(1,2)
          ENDIF
          IF (IE(3).NE.0) XYZ(3)=XYZ(3)+ZERRS(1,2)-ZERRS(1,1)
          XYZ(3)=0.5*XYZ(3)
        ENDIF
        BUFFER(JXB+1)=XYZ(1)
        BUFFER(JXB+2)=XYZ(2)
        BUFFER(JXB+3)=XYZ(3)
        JXB=JXB+3
        DO 60 I=2,NP
          IF ((IDEP-2).LT.0) THEN
            XYZ(1)=XVALS(1,I-1)
          ELSE IF ((IDEP-2).EQ.0) THEN
            XYZ(2)=YVALS(1,I-1)
          ELSE IF ((IDEP-2).GT.0) THEN
            XYZ(3)=ZVALS(1,I-1)
          ENDIF
          BUFFER(JXB+1)=XYZ(1)
          BUFFER(JXB+2)=XYZ(2)
          BUFFER(JXB+3)=XYZ(3)
          JXB=JXB+3
C
C   MODIFIED BY WBJ 4/24/85 TO CORRECT MONTH SCALES ERROR
C
C   CALCULATE HISTOGRAM EDGE BETWEEN ADJOINING BINS
C
          IF (IV(1).NE.0) THEN
            IF(NONLIN(1).EQ.-4) THEN
              DELTA=(MTOD(XVALS(1,I))-MTOD(XVALS(1,I-1)))*0.01
              XYZ(1)=2.*XVALS(1,I-1)+DELTA
            ELSE
              XYZ(1)=XVALS(1,I-1)+XVALS(1,I)
            ENDIF
            IF (IE(1).NE.0) THEN
              DIFF=XERRS(1,I)-XERRS(1,I-1)
              IF (XVALS(1,I).LE.XVALS(1,I-1)) THEN
                XYZ(1)=XYZ(1)+DIFF
              ELSE
                XYZ(1)=XYZ(1)-DIFF
              ENDIF
            ENDIF
            XYZ(1)=XYZ(1)*0.5
          ENDIF
        IF (IV(2).NE.0) THEN
            IF(NONLIN(2).EQ.-4) THEN
              DELTA=(MTOD(YVALS(1,I))-MTOD(YVALS(1,I-1)))*0.01
              XYZ(2)=2.*YVALS(1,I-1)+DELTA
            ELSE
              XYZ(2)=YVALS(1,I-1)+YVALS(1,I)
            ENDIF
            IF (IE(2).NE.0) THEN
              DIFF=YERRS(1,I)-YERRS(1,I-1)
              IF (YVALS(1,I).LE.YVALS(1,I-1)) THEN
                XYZ(2)=XYZ(2)+DIFF
              ELSE
                XYZ(2)=XYZ(2)-DIFF
              ENDIF
            ENDIF
            XYZ(2)=XYZ(2)*0.5
          ENDIF
          IF (IV(3).NE.0) THEN
            IF(NONLIN(3).EQ.-4) THEN
              DELTA=(MTOD(ZVALS(1,I))-MTOD(ZVALS(1,I-1)))*0.01
              XYZ(3)=2.*ZVALS(1,I-1)+DELTA
            ELSE
              XYZ(3)=ZVALS(1,I-1)+ZVALS(1,I)
            ENDIF
            IF (IE(3).NE.0) THEN
              DIFF=ZERRS(1,I)-ZERRS(1,I-1)
              IF (ZVALS(1,I).LE.ZVALS(1,I-1)) THEN
                XYZ(3)=XYZ(3)+DIFF
              ELSE
                XYZ(3)=XYZ(3)-DIFF
              ENDIF
            END IF
            XYZ(3)=XYZ(3)*0.5
          ENDIF
          BUFFER(JXB+1)=XYZ(1)
          BUFFER(JXB+2)=XYZ(2)
          BUFFER(JXB+3)=XYZ(3)
          JXB=JXB+3
          IF (JXB.GE.MAXJ) THEN
            JXB=(JXB-3)/3
            CALL T2XFRM (3,JXB,BUFFER(4),NONLIN,XFRM14,2,BUFFER)
            CALL TXLINX (BUFFER,JXB,LINTEX)
            JXB=3
            BUFFER(JXB+1)=XYZ(1)
            BUFFER(JXB+2)=XYZ(2)
            BUFFER(JXB+3)=XYZ(3)
            JXB=JXB+3
          ENDIF
 60     CONTINUE
        IF (IDEP-2.LT.0) THEN
          XYZ(1)=XVALS(1,I-1)
        ELSE IF (IDEP-2.EQ.0) THEN
          XYZ(2)=YVALS(1,I-1)
        ELSE IF (IDEP-2.GT.0) THEN
          XYZ(3)=ZVALS(1,I-1)
        ENDIF
        BUFFER(JXB+1)=XYZ(1)
        BUFFER(JXB+2)=XYZ(2)
        BUFFER(JXB+3)=XYZ(3)
        JXB=JXB+3
C
C   MODIFIED BY WBJ 4/24/85 TO CORRECT MONTH SCALES ERROR
C
C   THIS SECTION CALCULATES FINAL EDGE OF THE HISTOGRAM
C
        IF (IV(1).NE.0) THEN
          IF(NONLIN(1).EQ.-4) THEN
            DELTA=(MTOD(XVALS(1,NP))-MTOD(XVALS(1,NP-1)))*0.01
            XYZ(1)=2.*XVALS(1,NP)+DELTA
          ELSE
            XYZ(1)=3.*XVALS(1,NP)-XVALS(1,NP-1)
          ENDIF
          IF (IE(1).NE.0) XYZ(1)=XYZ(1)+XERRS(1,NP)-XERRS(1,NP-1)
          XYZ(1)=XYZ(1)*0.5
        ENDIF
        IF (IV(2).NE.0) THEN
          IF(NONLIN(2).EQ.-4) THEN
            DELTA=(MTOD(YVALS(1,NP))-MTOD(YVALS(1,NP-1)))*0.01
            XYZ(2)=2.*YVALS(1,NP)+DELTA
          ELSE
            XYZ(2)=3.*YVALS(1,NP)-YVALS(1,NP-1)
          ENDIF
          IF (IE(2).NE.0) XYZ(2)=XYZ(2)+YERRS(1,NP)-YERRS(1,NP-1)
          XYZ(2)=XYZ(2)*0.5
        ENDIF
        IF (IV(3).NE.0) THEN
          IF(NONLIN(3).EQ.-4) THEN
            DELTA=(MTOD(ZVALS(1,NP))-MTOD(ZVALS(1,NP-1)))*0.01
            XYZ(3)=2.*ZVALS(1,NP)+DELTA
          ELSE
            XYZ(3)=3.*ZVALS(1,NP)-ZVALS(1,NP-1)
          ENDIF
          IF (IE(3).NE.0) XYZ(3)=XYZ(3)+ZERRS(1,NP)-ZERRS(1,NP-1)
          XYZ(3)=XYZ(3)*0.5
        ENDIF
        BUFFER(JXB+1)=XYZ(1)
        BUFFER(JXB+2)=XYZ(2)
        BUFFER(JXB+3)=XYZ(3)
        JXB=JXB+3
        XYZ(IDEP)=YSTART
        BUFFER(JXB+1)=XYZ(1)
        BUFFER(JXB+2)=XYZ(2)
        BUFFER(JXB+3)=XYZ(3)
        JXB=JXB+3
        IF (.NOT.FLAGS(56)) THEN
          XYZ(1)=XYZLIM(1,2)
          BUFFER(JXB+1)=XYZ(1)
          BUFFER(JXB+2)=XYZ(2)
          BUFFER(JXB+3)=XYZ(3)
          JXB=JXB+3
        ENDIF
        JXB=(JXB-3)/3
        CALL T2XFRM (3,JXB,BUFFER(4),NONLIN,XFRM14,2,BUFFER)
        CALL TXLINX (BUFFER,JXB,LINTEX)
      ELSE
C
C  THIS IS THE BARGRAPH SECTION OF T2HIST
C
        J=0
        MAJOR=0
        DO 70 I=1,3
          IF (IV(I).NE.0) THEN
            J=I
            IF (IE(I).NE.0) MAJOR=I
          ENDIF
 70     CONTINUE
        IF (MAJOR.EQ.0) MAJOR=J
        IF (MAJOR.EQ.0) RETURN
        MINOR=6-IDEP-MAJOR
        YSTART=AMAX1(0.,XYZLIM(IDEP,1))
        IF (.NOT.(FLAGS(56))) THEN
      IF ((SIGN(1.,XYZLIM(IDEP,1)-YSTART).EQ.SIGN(1.,YSTART-XYZLIM(IDEP,
     12)))) THEN
            SAV1=XYZLIM(IDEP,1)
            SAV2=XYZLIM(IDEP,2)
            XYZLIM(IDEP,1)=YSTART
            XYZLIM(IDEP,2)=YSTART
            CALL T2XFRM (3,2,XYZLIM,NONLIN,XFRM14,2,BUFFER)
            XYZLIM(IDEP,1)=SAV1
            XYZLIM(IDEP,2)=SAV2
            CALL TXLINX (BUFFER,2,LINTEX)
          ENDIF
        ENDIF
C*VAX
C*VAX  FOR VAX CHANGE WIDDF TO 1.E30
C*VAX
        WIDDF=1.E30
        IF (MAJOR.EQ.1) THEN
          DO 80 I=2,NP
            W=XVALS(1,I)-XVALS(1,I-1)
            IF (W.NE.0.) WIDDF=AMIN1(WIDDF,ABS(W))
 80       CONTINUE
        ENDIF
        IF (MAJOR.EQ.2) THEN
          DO 90 I=2,NP
            W=YVALS(1,I)-YVALS(1,I-1)
            IF (W.NE.0.) WIDDF=AMIN1(WIDDF,ABS(W))
 90       CONTINUE
        ENDIF
        IF (MAJOR.EQ.3) THEN
          DO 100 I=2,NP
            W=ZVALS(1,I)-ZVALS(1,I-1)
            IF (W.NE.0.) WIDDF=AMIN1(WIDDF,ABS(W))
 100      CONTINUE
        ENDIF
      IF (NONLIN(MAJOR).EQ.-4.OR.NONLIN(MAJOR).EQ.-5) WIDDF=AMIN1(WIDDF,
     10.05)
        WIDDF=0.33*WIDDF
        WIDTH=WIDDF
        DO 110 I=1,3
          BUFFER(I+3)=XYZLIM(I,1)
          IF (SIGN(1.,XYZLIM(I,1)).NE.SIGN(1.,XYZLIM(I,2))) BUFFER(I+3)=
     10.
 110    CONTINUE
        BUFFER(IDEP+9)=BUFFER(IDEP+3)
        BUFFER(IDEP)=BUFFER(IDEP+3)
        IF (FLAGS(29)) THEN
          WRITE (DBGFIL,120) IDEP,MAJOR,MINOR
 120      FORMAT(60X,   16HIDEP,MAJOR,MINOR,3I3)
        ENDIF
        DO 140 I=1,NP
          IF (IE(MAJOR).NE.0) THEN
            WIDTH=0.
            IF (MAJOR.LT.2) THEN
              WIDTH=XERRS(1,I)
            ELSE IF (MAJOR.EQ.2) THEN
              WIDTH=YERRS(1,I)
            ELSE IF (MAJOR.GT.2) THEN
              WIDTH=ZERRS(1,I)
            ENDIF
            IF (WIDTH.EQ.0.) WIDTH=WIDDF
          ENDIF
          IF (XVALS(1,1).NE.HNONE) BUFFER(4)=XVALS(1,I)
          IF (YVALS(1,1).NE.HNONE) BUFFER(5)=YVALS(1,I)
          IF (ZVALS(1,1).NE.HNONE) BUFFER(6)=ZVALS(1,I)
          DO 130 J=1,3
            IF (NONLIN(J).EQ.-4.OR.NONLIN(J).EQ.-5) THEN
      IF (ABS(BUFFER(J+3)-AINT(BUFFER(J+3))).LT.0.002) BUFFER(J+3)=BUFFE
     1R(J+3)+0.16
            ENDIF
 130      CONTINUE
          BUFFER(MAJOR+6)=BUFFER(MAJOR+3)+WIDTH
          BUFFER(MAJOR+9)=BUFFER(MAJOR+6)
          BUFFER(MAJOR+3)=BUFFER(MAJOR+3)-WIDTH
          BUFFER(MAJOR)=BUFFER(MAJOR+3)
          BUFFER(IDEP+6)=BUFFER(IDEP+3)
          BUFFER(MINOR)=BUFFER(MINOR+3)
          BUFFER(MINOR+6)=BUFFER(MINOR+3)
          BUFFER(MINOR+9)=BUFFER(MINOR+3)
          CALL T2XFRM (3,4,BUFFER,NONLIN,XFRM14,2,BUFFER(13))
          CALL TXLINX (BUFFER(13),4,LINTEX)
 140    CONTINUE
      ENDIF
      LINTEX=ISAVE
      CALL TXSCIS (1)
      CALL T2TCKS
      RETURN
      END
      REAL FUNCTION MTOD(X)
      REAL DAYS(12)
      REAL CYCLE
      DATA DAYS/31.,28.,31.,30.,31.,30.,2*31.,30.,31.,30.,31./
      DATA CYCLE/1461./
      Y=X+.005
      M=INT(Y)
      XM=FLOAT(M)
      XD=100*(Y-XM)
      IC=(M-1)/48
      C=FLOAT(IC)
      IY=(M-1-48*IC)/12
      Y=FLOAT(IY)
      MTOD=C*CYCLE+Y*365+XD
      NEXT=IC*48+IY*12
      IF(M-1-NEXT.GT.0) THEN
        DO 10 I=NEXT+1,M-1
        J=MOD(I,12)
        IF(J.EQ.0) J=12
        K=MOD(I,48)
        IF(K.EQ.0) K=48
        IF(K.NE.38) THEN
          D=DAYS(J)
        ELSE
          D=29.
        ENDIF
 10     MTOD=MTOD+D
      ELSE
      ENDIF
      RETURN
      END
      SUBROUTINE TXDEVC (INFOIN,CARDIN)
      INTEGER INFOIN(*)
      CHARACTER*1 CARDIN(*)
      CHARACTER*256 TMPCHR
C
C  Input is the string from a SET DEVICE command.
C   Output, if a valid device is found, is QOPN, which will be used
C   as the argument in the call to UGOPEN  in TXXOPN.
C   NEWOPN is set TRUE if the device to be opened is different
C   from the current situation.
C
      COMMON /TXCOMD/ LXXNEW,LXXWID,LXXTXT,LXXCOL, TXTSIZ,TXTANG,TXTAN0,
     1TXTWID,TXTTXT,TXTCOL, TXXNEW,TXXWID,TXXCOL,URSIZE, VIRGIN,INFOXX(1
     20),            DFTWID,DFTTXT,DFTANG,DFTCOL,DFTSIZ,DFTVEC, XARRAY(6
     30),YARRAY(60), PXEL
      LOGICAL VIRGIN
      REAL TXTSIZ,TXTANG,TXTAN0,DFTANG,DFTSIZ,URSIZE
      INTEGER LXXNEW,LXXWID,LXXTXT,LXXCOL,TXXNEW,TXXWID,TXXCOL, TXTWID,T
     1XTTXT,TXTCOL, DFTWID,DFTTXT,DFTCOL,DFTVEC, INFOXX, PXEL(500)
      COMMON/TXCOMV/NQOPN,NQLIN,NQTXT
      INTEGER NQOPN,NQLIN,NQTXT
      COMMON/TXCOM2/QOPN,QLIN,QTXT
      CHARACTER*80 QOPN
      CHARACTER*20 QLIN
      CHARACTER*80 QTXT
      COMMON/TXCOM/PLIM(4,4),ZSCRN(2), XLLINE,YLLINE,XTLINE,YTLINE,XFRMF
     1C, IPAT,NPAT, NOWTEX,NOWWID,NOWCOL, NOWSET, PAT(20),PATCUM(20),PAT
     2TOT, NPTS,IDDEVC,SIDEWZ,CCSCIS(4)
      REAL PLIM,ZSCRN,XLLINE,YLLINE,XTLINE,YTLINE,XFRMFC, PAT,PATCUM,PAT
     1TOT,CCSCIS
      INTEGER NPTS,IDDEVC,SIDEWZ,IPAT,NPAT
C
C  This is needed to transmit SIDEWZ back to TXDEF1.
C
      COMMON /T2FLGC/ FLAGS(100)
      LOGICAL   FLAGS
      COMMON /T2COM/            REDUCE(2), TITX, TITY, BUFFER(400), XFRM
     112(12), XFRM13(12), XFRM14(12), XFRM23(12), XFRM24(12), XFRM34(6),
     2 XVARSX(20), TSIZDF,         GRDSIZ, CIRSIZ(2,3), ASIZE, AFLARE, S
     3MSZDF, ORAXES(3), EYEDIR(3), VUEDIR(3), VUECEN(3), VERTCL(3), EYES
     4EP, SCRD, SCRZ, EYEDIS, EYEPNT(3), XYPART(2, 2), BARSIZ(3), TIKSIZ
     5(3), XYZBAS(3), TTHETA, TPHI, FRELBL(4), DOTWID, DOTWDF, PATRN(10)
     6, SCLPRM(10,3), WINDOW(4), SCREEN(2),                 FACTXY(15),
     7LBLSIZ, SYMSIZ, XYZLIM(3,2), WORLD(3), RADRMX,RADRMN,RADAMN,RADANG
     8,RADX0,RADY0,XUNUSD(6), USRKBD,USRSCR, PLTFIL, ITITDT(3),
     9   LINTEX, ERRFIL, OUTFIL, DBGFIL, INPFIL, LENCRD,SYMBOL,      GRD
     XTYP, LINWID, LINCOL, TITCON(5), ITCNTR, IUNUSD,GRDSYM,    NXYZ1(3)
     X, NXYZ2(3), LBLCHR(3), LINWDF, NPATRN, NDIMNS(3), IBLKTP, IDIMNS,
     XLDEVCE, NMESH1, NMESH2, MESH1, MESH2, MESH3, NFIELD, IFIELD(15), N
     XINCR, LISTPT, NNAME, NHEADR, IVARBL(15), IVRPTR(15), LINEAR(4), NO
     XNLIN(4), INFO(10),NOSYMB,     NPOINT, BUFSIZ, LETSET, MXECHO, LINE
     XS, MAXLNS
      REAL WINDOW,SCREEN,SYMSIZ,LBLSIZ,GRDSYM, GRDSIZ,BUFFER,REDUCE,SYMB
     1OL,NOSYMB, FRELBL,SCLPRM,TIKSIZ,BARSIZ,XYZBAS, CIRSIZ,TITX,TITY,VU
     2EDIR,EYEPNT,XYPART, ORAXES,EYEDIR,VUECEN,VERTCL,SCRD,SCRZ,
     3EYESEP,EYEDIS
      INTEGER       NXYZ1,NXYZ2,LBLCHR,NONLIN,LINEAR, ERRFIL,OUTFIL,DBGF
     1IL, LENCRD,BUFSIZ,IFIELD, ITITDT,ITCNTR, USRKBD,USRSCR,PLTFIL, DOT
     2WID,DOTWDF,LINWID,LINWDF, GRDTYP,LETSET, NDIMNS,IBLKTP,IDIMNS,NINC
     3R,LISTPT, IVARBL,NNAME,NHEADR,IVRPTR
      COMMON /T2COMC/ TDDATE(8),CARD(160),INPFMT(16),PXNAME(8),INAME(3)
      CHARACTER*1 TDDATE,INAME
      CHARACTER*1 CARD,INPFMT,PXNAME
      COMMON /TOKENC/ INTERP,INTEG,FLOTNG,KEYORD,NSTRNG,MAXSTR
      REAL FLOTNG
      INTEGER MAXSTR,INTERP,KEYORD,NSTRNG
      INTEGER   INTEG
      COMMON /TOK2/ STRING(160)
      CHARACTER*1 STRING
      COMMON /EXPAU/ LPAUSE
      LOGICAL LPAUSE
C
C  To add new devices, increment NUMDEV in the PARAMETER statement
C     below which controls the size of DDLIST & FLUSH
C
      PARAMETER (NUMDEV=22)
      CHARACTER*8 DDLIST(NUMDEV)
      CHARACTER*80 BUFFR
      INTEGER EXTFLG
      LOGICAL NEWOPN,FLUSH(NUMDEV)
C
C  To add new devices, place UGS77 Device Name in DDLIST DATA statement
C     (and mind the damn ',' at the end of the string),
C     and at a parallel position in FLUSH enter .TRUE. for devices
C     whose buffers need flushing after each plot, otherwise .TRUE.
C     Some devices can be treated in three different ways:
C       1) sequential, in which case output is written to a file,
C       2) slave, in which case output is written directly to the
C          device by UG with a pause between pictures
C       3) interactive, in which case pictures are written to the
C          and TOPDRAW remains in control and waits for a keyboard
C          interupt to occur before continuing.
C     Tektronix 4010 and 4105 are examples of this.  Other devices
C     (e.g., IBM3179, IBM5080) are purely interactive while others
C     (e.g., Versatec or Imagen printers) are purely sequential.
C     When a new device is added, an appropriate short section of
C     code may be needed to define its characteristics.  This is
C     best done by paraphrasing code for a similar device.  A comment
C     near the end of the code shows where this should be done.
C
      DATA DDLIST/'SEQ4010,', 'SDD4010,', 'TEK4010,', 'VEP12FF,',
     1'VEP12CR,', 'IMPRT10,', 'IMGN300,', 'PDEVLIN,', 'SEQ4105,',
     2'SDD4105,', 'TEK4105,', 'SEQGIGI,', 'SDDGIGI,', 'SDDVST2,',
     3'DECVST2,', 'SDDGRIN,', 'IMGNIBM,', 'TALARIS,', 'PRTTRNX,',
     4'POSTSCR,', 'SDDXWDO,', 'XWINDOW,'/
c debug
      DATA FLUSH/ .FALSE.,    .TRUE. ,    .TRUE. ,    .FALSE.,
     1.FALSE.,    .FALSE.,    .FALSE.,    .FALSE.,    .FALSE.,
     2.TRUE. ,    .TRUE. ,    .FALSE.,    .TRUE. ,    .TRUE. ,
     3.TRUE. ,    .TRUE. ,    .FALSE.,    .FALSE.,    .FALSE.,
     4.FALSE.,    .FALSE.,    .FALSE. /
      IF (FLAGS(29)) THEN
        WRITE (DBGFIL,10) (INFOIN(IRITE),IRITE=1,10),(CARDIN(I),I=1,80)
 10     FORMAT (60X,    6HTXDEVC,10I5,99(/60X,6A2))
      END IF
      REDUCE(1)=0.      ! No reduction factor
      REDUCE(2)=1.      ! Units are inches
      FLAGS(54)=.FALSE. ! Device should be closed after each plot
      FLAGS(59)=.FALSE. ! Interactive device flag
      FLAGS(83)=.FALSE. ! Slave device flag
      LPAUSE=.TRUE.     ! Control for pause between pictures
      IDDDDD=0          ! DDNAME not set
      IDDVVV=0          ! Device not set
      IFORMT=0          ! Flag for INTERACTIVE, SLAVE, SEQUENTIAL
      IFANF=0           ! Flag for FANFOLD/CONTINUOUS
      SIDEWZ=0
      TXTAN0=0.         ! TD text angle agrees with graphic pkg
      EXTFLG=0          ! INTERNAL/EXTERNAL not specified
      NBUFFR=0
      BUFFR=' '
      TMPCHR='GENIL,'   ! Start the argument buffer
      CALL TXCRAM (TMPCHR,6,BUFFR,NBUFFR)
C
C  LARGE or SMALL
C
      TMPCHR='LARGE,30INCH,33INCH,BIG,SMALL,10INCH,11INCH,;'
      CALL TOKEN (INFOIN,CARDIN,TMPCHR)
      IF (INTERP.EQ.2.OR.INTERP.EQ.8) THEN   ! Successful match
        IF (INTERP.EQ.8) CALL T2ERR (INFOIN(1),INFOIN,CARDIN,ERRFIL,5)
        IF (KEYORD.LE.4) THEN                ! LARGE,30INCH,33INCH,BIG
          LARGE=2
        ELSE IF (KEYORD.LE.7) THEN           ! SMALL,10INCH,11INCH
          LARGE=1
        ELSE IF (KEYORD.GE.8) THEN           ! None of these
          LARGE=0
        END IF
      ELSE                             ! Token not matched
        LARGE=0                        ! Default to LARGE=0
        INFOIN(3)=INFOIN(4)            ! Reprocess token
      END IF
C
C  Device Names
C
C  To add new devices, place the TopDraw device name and number in
C     the argument list of the call to token below (e.g.,'IMAGEN:7')
C
C  (Notice all Tektronix 4010 synonyms are defaulted to device 3
C   which is a SLAVE 4013.  Other flavors are set when INTERACTIVE/
C   SEQUENTIAL is decided.)
C
      TMPCHR='TEKTRONIX:2,40:2,4013:2,4010:2,'//        ! 31 Char
     1       'IMAGEN:7,IMGN300:7,IMGNIBM:17,'//         ! 30
     2       'IMPRT10:6,IMGN240:6,'//                   ! 20
     3       'VERSATEC:4,'//                            ! 11
     4       'USERDEVC:8,'//                            ! 11
     5       '4105:9,'//                                !  7
     6       'SEQGIGI:12,'//                            ! 11
     7       'GIGI:13,REGIS:13,'//                      ! 17
     8       'VST2:14,UIS:14,'//                        ! 15
     9       'GRINNEL:16,'//                            ! 11
     A       'TALARIS:18,'//                            ! 11
     B       'PRTTRNX:19,'//                            ! 11
     C       'POSTSCR:20,PS:20,'//                      ! 16
     D       'DECW:21,;'                                ! 11
C  Sum to be sure TMPCHR does not exceed 256 char:       213 Total char
      CALL TOKEN (INFOIN,CARDIN,TMPCHR)
      IF (INTERP.EQ.2) THEN         ! Successful match
        IF (KEYORD.NE.9) THEN       ! Everything but VERSATEC
          IDDEVC=INTEG
          DFTWID=2
          IDDVVV=1
        ELSE                        ! VERSATEC
          DFTWID=1
          IDDVVV=1
        END IF
      ELSE IF (INTERP.EQ.8) THEN    ! Error condition
        CALL T2ERR (INFOIN(1),INFOIN,CARDIN,ERRFIL,5)
      ELSE                          ! Unsuccessful match
        INFOIN(3)=INFOIN(4)
      END IF
C
C   Modifying parameters
C
 100  TMPCHR='TEST,DEBUG,INTERACTIVE:3,SEQUENTIAL:1,SLAVE:2,PDS:1,'//
     1       'CONTINUOUS:2,FANFOLD:1,INTERNAL:1,EXTERNAL:2,'//
     2       'SIDEWAYS,DDNAME,;'
      CALL TOKEN (INFOIN,CARDIN,TMPCHR)
      IF (INTERP.EQ.2.OR.INTERP.EQ.8) THEN   ! Token Match
        IF (INTERP.EQ.8) CALL T2ERR (INFOIN(1),INFOIN,CARDIN,ERRFIL,5)
C
C   'TEST' appears to be ignored as an option.
C
        IF (KEYORD.EQ.2) THEN     ! DEBUG
          FLAGS(29)=.TRUE.
          GO TO 100
        ELSE IF (KEYORD.GE.3.AND.KEYORD.LE.6) THEN
          IFORMT=INTEG  !=1, 2, or 3 for SEQUENTIAL, SLAVE, or
C                         INTERACTIVE
          GO TO 100
        ELSE IF (KEYORD.GE.7.AND.KEYORD.LE.8) THEN    ! CONTINUOUS/
C                                                         FANFOLD
          IFANF=INTEG
          GO TO 100
        ELSE IF (KEYORD.GE.9.AND.KEYORD.LE.10) THEN   ! INTERNAL/
C                                                         EXTERNAL
          EXTFLG=INTEG
          GO TO 100
        ELSE IF (KEYORD.EQ.11) THEN    ! SIDEWAYS
          SIDEWZ=1
          TXTAN0= 90.0
          GO TO 100
        ELSE IF(KEYORD.EQ.12) THEN     ! DDNAME
          TMPCHR=';'
          CALL TOKEN (INFOIN,CARDIN,TMPCHR)
          IF (INTERP.GE.5.AND.INTERP.LE.6) THEN
c
C CORRECTED BY P. NASON TO ALLOW FOR NSTRNG>8 FOR VAXES
c            IF ((NSTRNG.LE.0).OR.(NSTRNG.GT.8)) THEN !REPLACED BY FOLLOWING
            IF (NSTRNG.LE.0) THEN
              CALL T2ERR (INFOIN(1),INFOIN,CARDIN,ERRFIL,5)
              GO TO 100
            ELSE
              IDDDDD=1       ! Flag that DDNAME is set.
              TMPCHR='DDNAME='
              CALL TXCRAM (TMPCHR,7,BUFFR,NBUFFR)
              DO I=1,NSTRNG       ! Put STRING into BUFFR
              CALL TXCRAM (STRING(I),1,BUFFR,NBUFFR)
              END DO
              TMPCHR=','          ! End with a comma.
              CALL TXCRAM (TMPCHR,1,BUFFR,NBUFFR)
              GO TO 100
            END IF
          ELSE
            CALL T2ERR (INFOIN(1),INFOIN,CARDIN,ERRFIL,5)
            GO TO 100
          END IF
        END IF
      ELSE IF (INTERP.EQ.5) THEN
        DO I=1,NSTRNG
          CALL TXCRAM (STRING(I),1,BUFFR,NBUFFR)
        END DO
        TMPCHR=','
        CALL TXCRAM (TMPCHR,1,BUFFR,NBUFFR)
        IF (IDDVVV.EQ.0) IDDEVC=0      ! Default device is nothing.
        GO TO 100
      ELSE IF (INTERP.EQ.3.OR.INTERP.EQ.4.OR.INTERP.EQ.6.OR.
     X         INTERP.EQ.7.OR.INTERP.EQ.9) THEN
        CALL T2ERR (INFOIN(1),INFOIN,CARDIN,ERRFIL,5)
        GO TO 100
      END IF
C
C   At this point, the SET DEVICE command has been scanned and the
C   relevant info dumped into BUFFR or various flags.  Now
C   go thru the flags and complete the BUFFR info.
C
C     Cycle through families of devices and set 'OPEN' characteristics
C     When a new device is added, this may mean adding code to this
C     section to accommodate its particular characteristics.
C
      IF (IDDEVC.GT.0) THEN
        IF (DDLIST(IDDEVC).EQ.'SDD4010,') THEN   ! TEKTRONIX 4010 family
C
C        The following code assumes the device list contains sequential,
C        slave, and interactive drivers specified in that order and that
C        the default device is 'slave'.
C
C   Check IFORMT for INTERACTIVE, SEQUENTIAL, PDS output.
C
          IF (IFORMT.EQ.0) IFORMT=2    ! VM default is 'SLAVE'
          IF (IFORMT.EQ.1) THEN        ! Output to file
            IDDEVC=IDDEVC-1            ! Demote a notch from default
          ELSE IF (IFORMT.EQ.2) THEN   ! SLAVE 4010.
            IDDDDD=1                   ! Don't set DDNAME
            FLAGS(83)=.TRUE.           ! Set the SLAVE flag
          ELSE IF (IFORMT.EQ.3) THEN   ! Interactive 4010.
            IDDDDD=1                   ! Don't set DDNAME
            IDDEVC=IDDEVC+1            ! Promote a notch from default
            FLAGS(59)=.TRUE.           ! Set the INTERACTIVE flag
          END IF
          CALL TXCRAM (DDLIST(IDDEVC),8,BUFFR,NBUFFR)
        ELSE IF (DDLIST(IDDEVC).EQ.'SEQ4105,') THEN ! 4105 TEKTRONIX
C                                                   family
C
C         Treated with the the assumption that the drivers in the
C         device list are in the order sequential, slave, and
C         interactive and that the default device is sequential (note
C         the difference from 4010 in default).
C
          IF (IFORMT.EQ.0) THEN IFORMT=1
          IF (IFORMT.EQ.2) THEN
            IDDEVC=IDDEVC+1            ! Bump one notch if slave
            IDDDDD=1                   ! Don't set DDNAME
            FLAGS(83)=.TRUE.           ! Set the SLAVE flag
          ELSE IF (IFORMT.EQ.3) THEN
            IDDDDD=1                   ! Don't set DDNAME
            IDDEVC=IDDEVC+2            ! Bump two notches if interactive
            FLAGS(59)=.TRUE.           ! Set the INTERACTIVE flag
          END IF
          CALL TXCRAM (DDLIST(IDDEVC),8,BUFFR,NBUFFR)
        ELSE IF (DDLIST(IDDEVC).EQ.'VEP12FF,') THEN     ! VERSATEC famil
          IF (IFANF.EQ.0) IFANF=1      ! Default is fanfold.
          IF (IFANF.EQ.2) IDDEVC=5     ! Continuous.
          CALL TXCRAM (DDLIST(IDDEVC),8,BUFFR,NBUFFR)
        ELSE IF (DDLIST(IDDEVC).EQ.'SDDGIGI,') THEN   ! Gigi
          IF (IFORMT.EQ.0) IFORMT=2    ! SLAVE device as default.
          IF (IFORMT.EQ.1) THEN        ! SEQUENTIAL has been specified.
            IDDEVC=IDDEVC-1            ! Bump down one notch
          ELSE IF (IFORMT.EQ.2) THEN   ! SLAVE case.
            IDDDDD=1                   ! Don't set DDNAME.
            FLAGS(83)=.TRUE.           ! Set the SLAVE flag.
          ELSE IF (IFORMT.EQ.3) THEN   ! INTERACTIVE was specified.
            IDDEVC=IDDEVC+1            ! Bump pointer up one notch.
            IDDDDD=1                   ! Don't set DDNAME.
            FLAGS(59)=.TRUE.           ! Set the INTERACTIVE flag.
          ENDIF
          CALL TXCRAM (DDLIST(IDDEVC),8,BUFFR,NBUFFR)
        ELSE IF (DDLIST(IDDEVC).EQ.'SDDVST2,') THEN   ! VAX Station 2000
C                                                       (VWS software)
          IF (IFORMT.EQ.0) IFORMT=2    ! SLAVE device is default.
          IF (IFORMT.EQ.1) THEN        ! SEQUENTIAL was specified;
C                                        since device can only be SLAVE
C                                        or INTERACTIVE, assume he
            IDDDDD=1                   ! meant SLAVE.
            FLAGS(83)=.TRUE.
          ELSE IF (IFORMT.EQ.2) THEN   ! SLAVE device.
            IDDDDD=1                   ! Don't set DDNAME.
            FLAGS(83)=.TRUE.           ! Set the SLAVE flag.
          ELSE IF (IFORMT.EQ.3) THEN   ! INTERACTIVE was specified.
            IDDEVC=IDDEVC+1            ! Bump pointer up one notch.
            IDDDDD=1                   ! Don't set DDNAME.
            FLAGS(59)=.TRUE.           ! Set the INTERACTIVE flag.
          ENDIF
          LPAUSE=.FALSE.               ! No extra pause between frames
          CALL TXCRAM (DDLIST(IDDEVC),8,BUFFR,NBUFFR)
        ELSE IF (DDLIST(IDDEVC).EQ.'SDDXWDO,') THEN   ! DECwindows
          IF (IFORMT.EQ.0) IFORMT=2    ! SLAVE device is default.
          IF (IFORMT.EQ.1) THEN        ! SEQUENTIAL was specified;
C                                        since device can only be SLAVE
C                                        or INTERACTIVE, assume he
            IDDDDD=1                   ! meant SLAVE.
            FLAGS(83)=.TRUE.
          ELSE IF (IFORMT.EQ.2) THEN   ! SLAVE device.
            IDDDDD=1                   ! Don't set DDNAME.
            FLAGS(83)=.TRUE.           ! Set the SLAVE flag.
          ELSE IF (IFORMT.EQ.3) THEN   ! INTERACTIVE was specified.
            IDDEVC=IDDEVC+1            ! Bump pointer up one notch.
            IDDDDD=1                   ! Don't set DDNAME.
            FLAGS(59)=.TRUE.           ! Set the INTERACTIVE flag.
          ENDIF
          LPAUSE=.FALSE.               ! No extra pause between frames
          CALL TXCRAM (DDLIST(IDDEVC),8,BUFFR,NBUFFR)
        ELSE IF (DDLIST(IDDEVC).EQ.'SDDGRIN,') THEN   ! Grinnell GMR-27
          IDDDDD=1                     ! Don't set DDNAME.
          FLAGS(83)=.TRUE.             ! Device always treated as SLAVE.
          CALL TXCRAM (DDLIST(IDDEVC),8,BUFFR,NBUFFR)
C
C   Addition devices should be added as an 'ELSE IF' structure at this
C   point in this if the IF construct.
C
        ELSE                      ! For 'ordinary devices', just put
C                                   the device name in BUFFR.
          CALL TXCRAM (DDLIST(IDDEVC),8,BUFFR,NBUFFR)
        END IF
        IF (IDDDDD.EQ.0) THEN     ! DDNAME not explicitly set
          TMPCHR='DDNAME='
          CALL TXCRAM (TMPCHR,7,BUFFR,NBUFFR)
          CALL TXCRAM (DDLIST(IDDEVC),8,BUFFR,NBUFFR)
        END IF
      END IF
C
C   Finish the options string.
C
      TMPCHR='*   '     ! Three extra blanks for IBM fullwords.
      CALL TXCRAM (TMPCHR,4,BUFFR,NBUFFR)
      CALL T2REST       ! Restore things that a SET DEVICE should
      FLAGS(54)=FLUSH(IDDEVC)     ! Flush buffer at end of picture?
      FLAGS(53)=.NOT.FLAGS(54)    ! Hard copy?
C
C   Check for identical call to an already open device.
C
      NEWOPN=BUFFR.EQ.QOPN
      QOPN=BUFFR
      NQOPN=NBUFFR
      IF (FLAGS(29)) THEN
      WRITE (DBGFIL,340) FLAGS(53),IDDEVC,BUFFR(1:20),BUFFR(21:40),BUFFR
     1(41:60)
 340  FORMAT (60X,   16HHARDCOPY,IDDEVC ,L1,I3,5(/60X,    7HUGOPEN(,A))
      END IF
      IF (NEWOPN) CALL TXEND
      RETURN
      END
 
 


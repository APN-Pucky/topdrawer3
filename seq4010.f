      SUBROUTINE UGTS01(DDIN,DDST,DDEX)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                  DEVICE-DEPENDENT MODULE FOR THE                  *
C *              TEKTRONIX 4010 SERIES GRAPHIC TERMINALS              *
C *                 FOR PRODUCING DISPLAY FILES IN A                  *
C *                       SEQUENTIAL DATA SET                         *
C *                                                                   *
C *  THIS SUBROUTINE IS A DEVICE-DEPENDENT MODULE FOR A NON-          *
C *  INTERACTIVE GRAPHIC DISPLAY DEVICE.                              *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGTS01(DDIN,DDST,DDEX)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    DDIN  AN INTEGER INPUT ARRAY.                                  *
C *    DDST  A CHARACTER STRING FOR INPUT AND OUTPUT.                 *
C *    DDEX  AN INTEGER OUTPUT ARRAY.                                 *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       DDIN(*)
      CHARACTER*(*) DDST
      INTEGER       DDEX(*)
C
      INCLUDE        'include/ugddacbk.for'
C
      INCLUDE        'include/ugddxtsq.for'
C
      INTEGER*4     INST(25)
      INTEGER*4     EXST(19)
      CHARACTER*4   EXPI
      INTEGER*4     EXPN,EXBR
      CHARACTER*64  EXNM
      EQUIVALENCE   (EXPI,EXST(1)),       (EXPN,EXST(2)),
     X              (EXBR,EXST(3)),       (EXNM,EXST(4))
C
      INTEGER       INT1
C
      DATA          INST/4,4,6,1, 4,4HPICT,4HID  ,
     X                     2,6,2, 0,4HPICT,4HSQ  ,
     X                     2,8,3, 0,4HBAUD,4HRATE,
     X                     4,6,4,64,4HDDNA,4HME  /
C
C  CHECK OPERATION FLAG AND BRANCH TO THE CORRECT SECTION.
      INT1=DDIN(1)
      IF ((INT1.LT.1).OR.(INT1.GT.10)) GO TO 902
      GO TO (101,151,201,251,301,351,401,451,501,551),INT1
C
C  OPERATION 1: OPEN THE GRAPHIC DEVICE.
  101 EXPI='PICT'
      EXPN=1
      EXBR=2400
      EXNM='UGDEVICE.DAT'
      CALL UGOPTN(DDST,INST,EXST)
      DDALX=DDXZZ
      CALL UGZ005(DDXRY,DDACX)
      DDAAT='SEQ4010 '
      DDABD(1,1)=0
      DDABD(1,2)=1023
      DDABD(2,1)=0
      DDABD(2,2)=779
      DDABX=0.0195
      DDABY=0.0195
      DDXID='DDA/TS00'
      DDXBN=0
      DDXNC=(MAX(MIN(EXBR,9600),300)-300)/1000
      DDXPI=EXPI
      DO 102 INT1=1,4
        IF (DDXPI(INT1:INT1).EQ.' ') DDXPI(INT1:INT1)='0'
  102 CONTINUE
      DDXPN=EXPN
      DDXPL='        '
      DDXDL='        '
      DDXPV=0
      DDXDC=0
      CALL UGTS07(EXNM,DDXIO,INT1)
      IF (INT1.NE.0) GO TO 902
      GO TO 901
C
C  OPERATION 2: CLOSE THE GRAPHIC DEVICE.
  151 CALL UGTS02(0)
      CALL UGTS08(DDXIO)
      GO TO 901
C
C  OPERATION 3: CLEAR THE SCREEN ON THE GRAPHIC DEVICE.
  201 IF (DDIN(2).NE.0) GO TO 902
      DDXDC=1
      DDXPL=DDXDL
      DDXDL=DDST
      GO TO 901
C
C  OPERATION 4: MANIPULATE THE SCREEN ON THE GRAPHIC DEVICE.
  251 GO TO 901
C
C  OPERATION 5: BEGIN A NEW GRAPHIC SEGMENT.
  301 IF (DDXDC.NE.0) THEN
        CALL UGTS02(1)
        DDXDC=0
      END IF
      DDEX(2)=0
      GO TO 901
C
C  OPERATION 6: TERMINATE A GRAPHIC SEGMENT.
  351 GO TO 901
C
C  OPERATION 7: MANIPULATE A GRAPHIC SEGMENT.
  401 GO TO 902
C
C  OPERATION 8: INQUIRE ABOUT A GRAPHIC PRIMITIVE.
  451 IF (DDIN(3).LT.3) THEN
        DDXIL=0
      ELSE
        DDXIL=1
      END IF
      INT1=DDIN(2)
      IF ((INT1.LT.1).OR.(INT1.GT.3)) GO TO 902
      GO TO (461,471,481),INT1
  461 GO TO 901
  471 IF (DDIN(7).NE.1) GO TO 902
      GO TO 901
  481 IF ((DDIN(8).GE.5).AND.(DDIN(8).LE.355)) GO TO 902
      IF (DDIN(9).NE.2) THEN
        IF (DDIN(7).LT.11) GO TO 902
        IF (DDIN(7).GT.17) GO TO 902
      END IF
      DDEX(2)=-5
      DDEX(3)=-7
      DDEX(4)=14
      DDEX(5)=0
      GO TO 901
C
C  OPERATION 9: DISPLAY A GRAPHIC PRIMITIVE.
  501 INT1=DDIN(2)
      IF ((INT1.LT.1).OR.(INT1.GT.3)) GO TO 902
      GO TO (511,521,531),INT1
  511 CALL UGTS03(DDIN(3),DDIN(4),0)
      IF (DDXIL.EQ.0) THEN
        CALL UGTS03(DDIN(3),DDIN(4),1)
      ELSE
        CALL UGTS03(DDIN(3),DDIN(4)+1,1)
        IF (DDIN(3).EQ.1023) GO TO 901
        CALL UGTS03(DDIN(3)+1,DDIN(4)+1,1)
        CALL UGTS03(DDIN(3)+1,DDIN(4),1)
      END IF
      GO TO 901
  521 CALL UGTS03(DDIN(3),DDIN(4),DDIN(5))
      GO TO 901
  531 CALL UGTS04(DDIN(3),DDIN(4),DDST)
      GO TO 901
C
C  OPERATION 10: PROCESS MISCELLANEOUS CONTROL FUNCTIONS.
  551 GO TO 902
C
C  SET ERROR INDICATOR AND RETURN TO CALLER.
  901 DDEX(1)=0
      GO TO 903
  902 DDEX(1)=1
  903 RETURN
C
      END
      SUBROUTINE UGTS02(FLAG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO TERMINATE ANY PREVIOUS PICTURE AND    *
C *  (OPTIONALLY) START A NEW PICTURE.                                *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGTS02(FLAG)                                              *
C *                                                                   *
C *  THE PARAMETER IN THE CALLING SEQUENCE IS:                        *
C *    FLAG  NEW PICTURE FLAG (0 MEANS TERMINATE CURRENT PICTURE      *
C *          ONLY, 1 MEANS ALSO START A NEW PICTURE).                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       FLAG
C
      INCLUDE        'include/ugddxtsq.for'
C
      CHARACTER*6   STR1
C
      CHARACTER*11  TAIL
      CHARACTER*1   TALX(11)
      EQUIVALENCE   (TAIL,TALX(1))
      CHARACTER*3   CLER
      CHARACTER*1   CLRX(3)
      EQUIVALENCE   (CLER,CLRX(1))
      CHARACTER*1   NULL
C
      INTEGER       INT1,INT2
C
      CHARACTER*1 ESC      ! Added by P. NASON, 1-11-92
c      DATA ESC/27/         ! Added by P. NASON, 1-11-92
c      DATA          TALX/Z1F,10*Z07/     ! Previous statement
c      DATA          TALX/Z1F,10*Z1D/      ! Replaced by P. NASON, 1-11-92
c      DATA          CLRX/Z1D,Z1B,Z0C/
c      DATA          NULL/Z1D/
c      DATA          TALX/31,10*29/      ! Replaced by P. NASON, 1-11-92
c      DATA          CLRX/29,27,12/
c      DATA          NULL/29/
      esc=char(27)
      TALX(1)=char(31)
      talx(2)=char(29)      ! Replaced by P. NASON, 1-11-92
      do j=3,11
         talx(j)=talx(2)
      enddo
      CLRX(1)=char(29)
      CLRX(2)=CHAR(27)
      CLRX(3)=CHAR(12)
      NULL=CHAR(29)
      
C
C  TERMINATE THE PREVIOUS PICTURE IF ONE IS AVAILABLE.
      IF (DDXPV.NE.0) THEN
        IF (16.GT.(DDXZ1-DDXBN)) CALL UGTS05(1,1)
        CALL UGTS03(0,767,0)
        DDXBF(DDXBN+1:DDXBN+11)=TAIL
        DDXBN=DDXBN+11
        CALL UGTS05(0,1)
        IF (DDXPL.NE.'        ') THEN
          DDXBF(1:20)='./       ALIAS NAME='
          DDXBF(21:28)=DDXPL
          DDXBN=28
          CALL UGTS05(0,0)
        END IF
      END IF
C
C  TERMINATE THE DATA SET OR START A NEW PICTURE.
      DDXBN = DDXZ1
      IF (FLAG.EQ.0) THEN
         DO INT1=8,DDXZ1
          DDXBF(INT1:INT1)=' '
         ENDDO
         DDXBF(1:7)= ' '//ESC//'[?38l'
         CALL UGTS05(0,1)
      ELSE
         IF(DDXPN.EQ.1) THEN
            DO INT1=8,DDXZ1
              DDXBF(INT1:INT1)=NULL
            ENDDO
            DDXBF(1:7)= ' '//ESC//'[?38h'
            CALL UGTS05(0,1)
         ELSE
            DDXBF(1:3)=CLER
            DO INT1=4,DDXZ1
               DDXBF(INT1:INT1)=NULL
            ENDDO
            CALL UGTS05(0,1)
            IF (DDXNC.GT.0) THEN
               DO INT1=1,DDXNC
                  DO INT2=1,DDXZ1
                     DDXBF(INT2:INT2)=NULL
                  ENDDO
                  CALL UGTS05(0,1)
               ENDDO
            END IF
         ENDIF
         DDXPN=DDXPN+1
         DDXPL='        '
         DDXPV=1
      END IF
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGTS03(XCRD,YCRD,BBIT)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO GENERATE ORDERS TO MOVE THE BEAM TO   *
C *  A NEW POSITION.                                                  *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGTS03(XCRD,YCRD,BBIT)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    XCRD  THE X COORDINATE OF THE NEW POSITION.                    *
C *    YCRD  THE Y COORDINATE OF THE NEW POSITION.                    *
C *    BBIT  THE BLANKING BIT.                                        *
C *                                                                   *
C *********************************************************************
C
      INTEGER       XCRD,YCRD,BBIT
C
      INCLUDE        'include/ugddxtsq.for'
C
      INTEGER       MYHI,MYLO,MXHI,MXLO
      CHARACTER*1   BLKO
      INTEGER       CREQ
C
c      DATA          MYHI/Z00000020/
c      DATA          MYLO/Z00000060/
c      DATA          MXHI/Z00000020/
c      DATA          MXLO/Z00000040/
c      DATA          BLKO/Z1D/
      DATA          MYHI/32/
      DATA          MYLO/96/
      DATA          MXHI/32/
      DATA          MXLO/64/
      BLKO=char(29)
C
C  COMPUTE REQUIREMENTS AND WRITE OUT CURRENT RECORD IF NECESSARY.
      IF (BBIT.EQ.0) THEN
        CREQ=10
      ELSE
        CREQ=5
      END IF
      IF (CREQ.GT.(DDXZ1-DDXBN)) THEN
        CALL UGTS05(1,1)
        IF (BBIT.NE.0) THEN
          DDXBF(DDXBN+1:DDXBN+1)=BLKO
          DDXBF(DDXBN+2:DDXBN+5)=DDXOP
          DDXBN=DDXBN+5
        END IF
      END IF
C
C  CREATE THE NEW POINT AND ADD IT TO THE CURRENT RECORD.
      DDXOP(1:1)=CHAR(MOD(YCRD/32,32)+MYHI)
      DDXOP(2:2)=CHAR(MOD(YCRD   ,32)+MYLO)
      DDXOP(3:3)=CHAR(MOD(XCRD/32,32)+MXHI)
      DDXOP(4:4)=CHAR(MOD(XCRD   ,32)+MXLO)
      IF (BBIT.EQ.0) THEN
        DDXBF(DDXBN+1:DDXBN+1)=BLKO
        DDXBN=DDXBN+1
      END IF
      DDXBF(DDXBN+1:DDXBN+4)=DDXOP
      DDXBN=DDXBN+4
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGTS04(XCRD,YCRD,TEXT)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO GENERATE ORDERS TO DISPLAY            *
C *  CHARACTERS AT A SPECIFIC POSITION.                               *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGTS04(XCRD,YCRD,TEXT)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    XCRD  THE X COORDINATE OF THE FIRST CHARACTER.                 *
C *    YCRD  THE Y COORDINATE OF THE FIRST CHARACTER.                 *
C *    TEXT  THE CHARACTER STRING.                                    *
C *                                                                   *
C *********************************************************************
C
      INTEGER       XCRD,YCRD
      CHARACTER*(*) TEXT
C
      INCLUDE        'include/ugddxtsq.for'
C
      INTEGER       NTXT
      CHARACTER*80  ITXT
      CHARACTER*1   ECMO
      INTEGER       CRDX
      INTEGER       INSI,INSJ,INSN
C
c      DATA          ECMO/Z1F/
      ECMO=char(31)
C
C  TRANSLATE THE CHARACTERS AND INITIALIZE THE PROGRAM.
      NTXT=LEN(TEXT)
      ITXT(1:NTXT)=TEXT
      CALL UGTS06(1,ITXT(1:NTXT))
      CRDX=XCRD
      INSI=1
      INSN=NTXT
C
C  WRITE OUT THE CURRENT RECORD IF NECESSARY.
      IF (11.GT.(DDXZ1-DDXBN)) CALL UGTS05(1,1)
C
C  INSERT THE TEXT INTO THE RECORD.
  101 CALL UGTS03(CRDX,YCRD,0)
      DDXBF(DDXBN+1:DDXBN+1)=ECMO
      INSJ=MIN(DDXZ1-DDXBN-2,INSN)
      DDXBF(DDXBN+2:DDXBN+INSJ+1)=ITXT(INSI:INSI+INSJ-1)
      DDXBN=DDXBN+INSJ+1
      IF (INSJ.NE.INSN) THEN
        CRDX=CRDX+14*INSJ
        INSI=INSI+INSJ
        INSN=INSN-INSJ
        CALL UGTS05(1,1)
        GO TO 101
      END IF
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGTS05(FLG1,FLG2)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO COMPLETE THE CURRENT RECORD AND       *
C *  WRITE IT OUT.  A TERMINAL BYTE MAY OPTIONALLY BE ADDED TO THE    *
C *  RECORD.                                                          *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGTS05(FLG1,FLG2)                                         *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    FLG1  TERMINAL BYTE FLAG (0 MEANS DO NOT ADD TERMINAL BYTE, 1  *
C *          MEANS ADD TERMINAL BYTE).                                *
C *    FLG2  TRANSLATE FLAG (0 MEANS DO NOT TRANSLATE, 1 MEANS        *
C *          TRANSLATE).                                              *
C *                                                                   *
C *********************************************************************
C
      INTEGER       FLG1,FLG2
C
      INCLUDE        'include/ugddxtsq.for'
C
      CHARACTER*1   TERM
C
      INTEGER       INT1
C
c      DATA          TERM/Z1D/
      TERM=char(29)
C
C  PROCESS THE CARD AND WRITE IT OUT.
      IF (FLG1.NE.0) THEN
        DDXBF(DDXBN+1:DDXBN+1)=TERM
        DDXBN=DDXBN+1
      END IF
      IF (FLG2.NE.0) CALL UGTS06(0,DDXBF(1:DDXBN))
      DO 101 INT1=DDXBN+1,DDXZ2
        DDXBF(INT1:INT1)=' '
  101 CONTINUE
      CALL UGTS09(DDXIO,DDXBF)
      DDXBN=0
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGTS06(FLAG,STRG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO TRANSLATE CHARACTER STRINGS FROM THE  *
C *  TERMINAL CHARACTER SET TO THE COMPUTER CHARACTER SET OR FROM     *
C *  THE COMPUTER CHARACTER SET TO THE TERMINAL CHARACTER SET.        *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGTS06(FLAG,STRG)                                         *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    FLAG  TRANSLATE FLAG (0 MEANS TERMINAL TO COMPUTER, 1 MEANS    *
C *          COMPUTER TO TERMINAL).                                   *
C *    STRG  THE CHARACTER STRING TO BE TRANSLATED.                   *
C *                                                                   *
C *********************************************************************
C
      INTEGER       FLAG
      CHARACTER*(*) STRG
C
      INTEGER       INT1
      CHARACTER*1   CHR1
C
C  TRANSLATE THE CHARACTER STRING AS NEEDED.
      IF (FLAG.EQ.1) THEN
        DO 101 INT1=1,LEN(STRG)
          CHR1=STRG(INT1:INT1)
          IF ((CHR1.LT.' ').OR.(CHR1.GT.'~')) STRG(INT1:INT1)='@'
  101   CONTINUE
      END IF
C
C  RETURN TO CALLER.
      RETURN
C
      END

c
c *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
c *                  DEVICE-DEPENDENT MODULE FOR THE                  *
c *              TEKTRONIX 4010 SERIES GRAPHIC TERMINALS              *
c *                 FOR PRODUCING DISPLAY FILES IN A                  *
c *                       SEQUENTIAL DATA SET                         *
c *                                                                   *
c *  THESE SUBROUTINES ARE THE ACTUAL OUTPUT ROUTINES.  UGTS07 IS     *
c *  THE OPEN MODULE, UGTS08 IS THE CLOSE MODULE, AND UGTS09 IS THE   *
c *  WRITE MODULE.                                                    *
c *                                                                   *
c *  THE CALLING SEQUENCES ARE:                                       *
c *    CALL UGTS07(NAME,PIOD,FLAG)                                    *
c *    CALL UGTS08(PIOD)                                              *
c *    CALL UGTS09(PIOD,STRG)                                         *
c *                                                                   *
c *  THE PARAMETERS IN THE CALLING SEQUENCES ARE:                     *
c *    NAME  THE NAME OF THE OUTPUT DATA SET.  THE CHARACTER STRING   *
c *          SHOULD BE 64 CHARACTERS LONG.                            *
c *    PIOD  A POINTER TO THE I/O CONTROL BLOCKS.  THIS ITEM IS       *
c *          GENERATED BY THE OPEN FUNCTION AND MUST BE SUPPLIED IN   *
c *          THE OTHER FUNCTIONS.                                     *
c *    FLAG  A SUCCESS FLAG (0 MEANS SUCCESS, 1 MEANS ERROR).         *
c *    STRG  THE CHARACTER STRING THAT IS TO BE WRITTEN TO THE        *
c *          OUTPUT DATA SET.  THIS CHARACTER STRING SHOULD BE 80     *
c *          CHARACTERS LONG.                                         *
c *                                                                   *
c *                          ROBERT C. BEACH                          *
c *                    COMPUTATION RESEARCH GROUP                     *
c *                STANFORD LINEAR ACCELERATOR CENTER                 *
c *                                                                   *
c *********************************************************************
c
      subroutine ugts07(NAME,PIOD,FLAG)
      character *64 NAME
      integer piod,flag
      piod=33
      open(unit=33,file=name,status='UNKNOWN',iostat=flag)
      end
      subroutine ugts08(piod)
      close(33)
      end
      subroutine ugts09(piod,strg)
      character * 80 strg
      write(33,'(30a)')strg
      end

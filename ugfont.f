      SUBROUTINE UGFONT(OPTN)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *              SELECT THE EXTENDED CHARACTER SET FONT               *
C *                                                                   *
C *  THIS SUBROUTINE MAY BE USED TO SELECT THE FONT FOR THE EXTENDED  *
C *  CHARACTER SET.                                                   *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGFONT(OPTN)                                              *
C *                                                                   *
C *  THE PARAMETER IN THE CALLING SEQUENCE IS:                        *
C *    OPTN  THE OPTIONS LIST.                                        *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      CHARACTER*(*) OPTN
C
      INCLUDE        'include/ugmcacbk.for'
C
      INCLUDE        'include/ugerrcbk.for'
C
      INTEGER*4     INST(13)
      INTEGER*4     EXST(1),EXCG
      EQUIVALENCE   (EXCG,EXST(1))
C
      CHARACTER*8   NAMS(2)
C
      DATA          INST/2,1,7,1,1,4HSIMP,4HLEX ,
     X                     1,6,1,2,4HDUPL,4HEX  /
      DATA          NAMS/'UGA013  ',
     X                   'UGA014  '/
C
C  SCAN THE OPTIONS LIST.
      EXCG=0
      CALL UGOPTN(OPTN,INST,EXST)
C
C  HAS A NEW FONT BEEN REQUESTED?
      IF (EXCG.EQ.0) GO TO 101
      IF (MCACN.EQ.NAMS(EXCG)) GO TO 101
C
C  DELETE THE OLD FONT IF ONE HAS BEEN PREVIOUSLY LOADED.
      IF (MCACP.NE.0) THEN
        CALL UGZ002(1,MCACN,MCACP)
        MCACP=0
      END IF
C
C  SAVE THE NAME OF THE NEW FONT.
      MCACN=NAMS(EXCG)
C
C  RESET ERROR INDICATORS AND RETURN TO CALLER.
  101 UGELV=0
      UGENM='        '
      UGEIX=0
      RETURN
C
      END


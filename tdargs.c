#include <stdio.h>	
#include <stdlib.h>
# define NARG 1
# define LENSTR (NARG*132)

void usage()
{
 printf(
"usage: td filename -device=dev -output=outfile -sideways\n\
arguments starting with - are assumed to be options, and are passed\n\
to topdrawer. Arguments not starting with - are filenames.\n\
If a filename does not end in .top, filename.top is tryed first.\n\
If this fails, filename alone is used.\n\
Some useful options:\n\
-device=ps   : (default) creates a postscript file fname.ps\n\
-device=4010 : displays the plot on tektronic terminals\n\
-device=ln3  : creates a ln03 file fname.ln3\n\
-sideways    : creates a rotated plot.\n\
-output=outf : use outf instead of default for output filename\n\
-lis         : create the td.lis and tderrors.lis files.\n");
 exit(1);
}

void tdmain_(int file, int* lfile,char* str,int* lenstr,int* maxstr);

getoptions(argv,argc,str,plen)
     int argc, *plen;
     char *argv[];
     char str[LENSTR];
{
  char *p,*p_start,*p_last;
  int strcmp();
  char *pargv;
  int i, lenstr, maxstr;
  p = &str[0];
  p_start = p ;
/* all nulls for safety */
  for(i=0;i<LENSTR;i++) str[i]='\0';
/* leave some slack! */
  p_last = p+LENSTR-6 ;
  *(p++)=' ';
  for(i=1;i<argc;i++) {
    pargv=argv[i];
    if( (*pargv) == '-') {
      /*  skip '-' */
      pargv++;
      while((*pargv)!='\0') {
	if(p > p_last) {
	  printf("td: argument line too long");
	  exit(1);
	}
	/* after an OUTPUT= stop translation and quote argument*/
	if(*pargv == '=' && p-p_start>=6 && 0==strcmp(p-6,"OUTPUT")){
	  *(p++)=*(pargv++);*(p++)='"';*(p++)='"';*(p++)='"';
	  while(p<p_last && (*(p++)=*pargv)!='\0')pargv++;
	  *(p++)='"';*(p++)='"';*(p++)='"';}
	else {
	  /*  and translate option to uppercase */
	  if((*pargv)<='z' && (*pargv)>= 'a')
	    (*p)='A'-'a'+(*pargv);
	  else (*p)=(*pargv);
	  p++; pargv++; }
      }
      (*p++)=' ';
    }}
  *plen=p-&str[0];
}

main(argc,argv)
    int argc;
    char *argv[];
{
    char str[LENSTR];
    int i, lfile, lenstr, maxstr;
    char * file, *pargv;
    maxstr=LENSTR-6;
    lfile=0;
/* scan for files */
    for(i=1;i<argc;i++) {
	pargv=argv[i];
/* if it is not an option; must be a filename */
	if( (*pargv) != '-') 
	    { file=argv[i]; lfile=0; while((*(pargv+lfile))!='\0') lfile++;
	      /* scan for options first */
	      getoptions(argv,argc,str,&lenstr);
	      tdmain_(file,&lfile,str,&lenstr,&maxstr);	} }
    if(lfile == 0) usage();
    exit(0);
}

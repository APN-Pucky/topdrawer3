c system dependent values for masks.
c msk1 must have only one bit on, msk2 = inot(msk1),
c and the relevant bit must be irrelevant in real *4 numbers
       DATA          MSK1/ 1/
       DATA          MSK2/-2/

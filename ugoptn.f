      SUBROUTINE opterr(STR)
      CHARACTER * (*) STR
      WRITE(*,*) '************ ERROR ***********'
      WRITE(*,*) STR
      END

      subroutine tokens(string,istart,iend,ifl,maxtok,itok)
c
c- find the tokens of which string is made up
c  string: input string
c  istart(*): initial character of each token
c  iend(*)  : final character of each token
c  ifl(*)   : flag: 1 for name,
c                   2 for integer,
c                   3 for float,
c                   4 for quoted string
c                   5 for special characters
c  maxtok   : don't do more than maxtok tokens (arrays istart,iend,ifl limits
c  itok     : return value, number of found tokens
c
c- c is the current character, sd is the string delimiter
      character * 1 c, sd, string *(*)
      dimension istart(*),iend(*),ifl(*)
      lll = len(string)
      itok = 1
      ifl(itok) = 0
      do 10 j=1,lll
         c = string(j:j)
         if(ifl(itok).eq.4) then
c- is a string. If c is not the terminator continue
            if(c.ne.sd) goto 10
c is a string end?
c- look ahead for one more, to check for double quotes
            if(j.lt.lll.and.string(j+1:j+1).eq.sd) then
                string(j:)=string(j+1:)
            else
c- close the string
               iend(itok)=j-1
               itok=itok+1
               if(itok.gt.maxtok) goto 20
               ifl(itok)=0
            endif
            goto 10
         elseif(c.eq.''''.or.c.eq.'"') then
            if(ifl(itok).ne.0) then
c- close old token
               iend(itok)=j-1
               itok=itok+1
               if(itok.gt.maxtok) goto 20
            endif
c- open string token
            ifl(itok)=4
            istart(itok)=j+1
            sd=c
         elseif(c.eq.' ') then
            if(ifl(itok).eq.0.or.ifl(itok).eq.4) goto 10
            iend(itok)=j-1
            itok=itok+1
            if(itok.gt.maxtok) goto 20
            ifl(itok) = 0
            goto 10
         elseif
     #   ((c.le.'z'.and.c.ge.'a').or.(c.le.'Z'.and.c.ge.'A')
     #     .or.(c.eq.'_')) then
            if(ifl(itok).eq.0) then
c- new token (identifier)
               istart(itok) = j
               ifl(itok) = 1
            endif
            goto 10
         elseif(c.le.'9'.and.c.ge.'0')then
            if(ifl(itok).eq.0) then
c- new token (integer)
               if(itok.gt.maxtok) goto 20
               istart(itok) = j
               ifl(itok) = 2
            endif
            goto 10
         else
            if(c.eq.'-'.and.ifl(itok).eq.0) then
c- new token (integer)
               if(itok.gt.maxtok) goto 20
               istart(itok) = j
               ifl(itok) = 2
               goto 10
            endif
c- special character unless . in number
            if(c.eq.'.'.and.ifl(itok).eq.2) then
c- floating point number
               ifl(itok)=3
               goto 10
            endif
c- or . in name
            if(c.eq.'.'.and.ifl(itok).eq.1) then
c- name
               goto 10
            endif
c- special character
c- close old token
            if(ifl(itok).ne.0) then
               if(itok.gt.maxtok) goto 20
               iend(itok)=j-1
               itok=itok+1
               if(itok.gt.maxtok) goto 20
            endif
c- new token
            if(itok.gt.maxtok) goto 20
            istart(itok) = j
            iend(itok) = j
            ifl(itok) = 5
            itok=itok+1
            if(itok.gt.maxtok) goto 20
            ifl(itok) = 0
         endif
10    continue
      if(ifl(itok).ne.0) then
         iend(itok)=lll
      else
         itok = itok-1
      endif
      return
20    write(*,*) 'token: too many tokens in string'
      stop
      end


      subroutine ugopTN(optns,indat,iexdat)
      dimension indat(*),iexdat(*)
      character * 4 str4, chtmp * 10
      character *(*) optns
      dimension istart(128),iend(128),ifl(128)
      equivalence (str4(1:1),istr4)
      equivalence (real,intg)
      data maxtok/128/
      call tokens(optns,istart,iend,ifl,maxtok,itok)
c- number of items
      nitems = indat(1)
      do 100 l=1,itok
      if(ifl(l).eq.1) then
      iltok = iend(l)-istart(l)+1
      iptr = 2
      do 101 j=1,nitems
c
         ilen = indat(iptr+1)
         if(ilen.ne.iltok) goto 90
         ileno4 = ilen/4
         ilenr = ilen-ileno4*4
c
         itmp = iptr+4
         istr4 = indat(itmp)
         itmp1 = itmp+ileno4
         itmp = itmp+1
         kk = istart(l)
         do jj=itmp,itmp1
            if(str4.ne.optns(kk:kk+3) ) goto 90
            istr4 = indat(jj)
            kk=kk+4
         enddo
         if(ilenr.gt.0) then
            if(str4(1:ilenr).ne.optns(kk:kk+ilenr-1) ) goto 90
         endif
c
c Found a match
c
         ityp = indat(iptr)
         iout = indat(iptr+2)
         if(ityp.eq.1) then
c flag (alone
            iexdat(iout) = indat(iptr+3)
         elseif(ityp.eq.2) then
c flag=integer
            if(optns(istart(l+1):iend(l+1)).eq.'='
     #        .and. ifl(l+2).eq.2) then
               chtmp=' '
               chtmp=optns(istart(l+2):iend(l+2))
               read(chtmp,'(bn,i10)')
     #          iexdat(iout)
            else
               call opterr('UGOPTN: expected an integer')
               stop
            endif
         elseif(ityp.eq.3) then
c flag=float
            if(optns(istart(l+1):iend(l+1)).eq.'='
     #        .and. ifl(l+2).eq.3.OR.IFL(L+2).EQ.2) then
               chtmp=' '
               chtmp=optns(istart(l+2):iend(l+2))
               read(chtmp,'(bn,f10.0)') real
               iexdat(iout)= intg
            else
               call opterr('UGOPTN: expected a float')
               stop
            endif
         elseif(ityp.eq.4) then
c flag=string
            if(optns(istart(l+1):iend(l+1)).eq.'='
     #        .and. ifl(l+2).eq.4.or.ifl(l+2).eq.1) then
               ibytes = indat(iptr+3)
               itmp = istart(l+2)
               if(iend(l+2)-istart(l+2)+1.gt.ibytes) then
                  call opterr
     #         ('UGOPTN: not enough room to hold the string')
                  stop
               endif
               itmpe = min(itmp+ibytes-1,iend(l+2))
               do jj=0,(ibytes-1)/4
                  itmp1 = min(itmp+3,itmpe)
                  if(itmp.le.itmp1) then
                     str4 = optns(itmp:itmp1)
                  else
                     str4 = ' '
                  endif
                  iexdat(iout+jj)= istr4
                  itmp = itmp+4
               enddo
            endif
         elseif(ityp.eq.5) then
C FLAG=010001110101  (BIT PATTERN
            if(ityp.eq.5) then
              WRITE(*,*) 'THIS DOESN''T WORK YET'
              STOP
            endif
            if(optns(istart(l+1):iend(l+1)).eq.'='
     #        .and. ifl(l+2).eq.5) then
               ibytes = indat(iptr+3)
               itmp = istart(l+2)
               itmpe = iend(l+2)
               do jj=0,ibytes-1
                  itmp1 = itmp+4
                  if(itmp1.le.itmpe) then
                     str4 = optns(itmp:itmp1)
                  elseif(itmp1.gt.itmpe) then
                     str4 = optns(itmp:itmpe)
                  else
                     str4 = ' '
                  endif
                  iexdat(jj)= istr4
               enddo
            endif
         endif
 90      continue
         iptr = iptr+4+(ilen+3)/4
101   continue
      endif
100   continue
      end

      SUBROUTINE UGPR01(DDIN,DDST,DDEX)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                    DEVICE-DEPENDENT MODULE FOR                    *
C *                      THE POSTSCRIPT LANGUAGE                      *
C *                                                                   *
C *  THIS SUBROUTINE IS A DEVICE-DEPENDENT MODULE FOR A NON-          *
C *  INTERACTIVE GRAPHIC DISPLAY DEVICE.                              *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGPR01(DDIN,DDST,DDEX)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    DDIN  AN INTEGER INPUT ARRAY.                                  *
C *    DDST  A CHARACTER STRING FOR INPUT AND OUTPUT.                 *
C *    DDEX  AN INTEGER OUTPUT ARRAY.                                 *
C *                                                                   *
C *                          ROBERT C. BEACH                          *
C *                    COMPUTATION RESEARCH GROUP                     *
C *                STANFORD LINEAR ACCELERATOR CENTER                 *
C *                                                                   *
C *********************************************************************
C
      INTEGER       DDIN(*)
      CHARACTER*(*) DDST
      INTEGER       DDEX(*)
C
c     INCLUDE        'include/ugddacbk.for'
      INCLUDE        'include/ugddacbk.for'
C
c     INCLUDE        'include/ugddxpsc.for'
      INCLUDE        'include/ugddxpsc.for'
C
      INTEGER*4     INST(99)
      INTEGER*4     EXST(122)
      INTEGER*4     EXXN,EXXX,EXYN,EXYX
      REAL*4        EXZX,EXZY
      INTEGER*4     EXRA,EXFT,EXGL,EXAS
      CHARACTER*64  EXNM
      CHARACTER*64  EXBP,EXEP
      CHARACTER*64  EXBX,EXEX
      CHARACTER*64  EXBR,EXER
      EQUIVALENCE   (EXXN,EXST(  1)),     (EXXX,EXST(  2)),
     X              (EXYN,EXST(  3)),     (EXYX,EXST(  4)),
     X              (EXZX,EXST(  5)),     (EXZY,EXST(  6)),
     X              (EXRA,EXST(  7)),     (EXFT,EXST(  8)),
     X              (EXGL,EXST(  9)),     (EXAS,EXST( 10)),
     X              (EXNM,EXST( 11)),
     X              (EXBP,EXST( 27)),     (EXEP,EXST( 43)),
     X              (EXBX,EXST( 59)),     (EXEX,EXST( 75)),
     X              (EXBR,EXST( 91)),     (EXER,EXST(107))
C
      REAL          FLT1,FLT2,FLT3
      INTEGER       INT1,INT2
C
      integer itmpl,jtmpl,ixl,iyl,ixh,iyh
      save itmpl,jtmpl,ixl,iyl,ixh,iyh
      DATA          INST/17,2,4,  1, 0,4HXMIN,
     X                      2,4,  2, 0,4HXMAX,
     X                      2,4,  3, 0,4HYMIN,
     X                      2,4,  4, 0,4HYMAX,
     X                      3,5,  5, 0,4HRUCM,4HX   ,
     X                      3,5,  6, 0,4HRUCM,4HY   ,
     X                      1,7,  7, 1,4HROTA,4HXIS ,
     X                      1,7,  8, 1,4HSLDF,4HILL ,
     X                      1,5,  9, 1,4HGENI,4HL   ,
     X                      1,5, 10, 1,4HASCI,4HI   ,
     X                      4,6, 11,64,4HDDNA,4HME  ,
     X                      4,6, 27,64,4HBEGF,4HIL  ,
     X                      4,6, 43,64,4HENDF,4HIL  ,
     X                      4,6, 59,64,4HBEGP,4HIC  ,
     X                      4,6, 75,64,4HENDP,4HIC  ,
     X                      4,6, 91,64,4HBEGR,4HEC  ,
     X                      4,6,107,64,4HENDR,4HEC  /
C
C  CHECK OPERATION FLAG AND BRANCH TO THE CORRECT SECTION.
      INT1=DDIN(1)
      IF ((INT1.LT.1).OR.(INT1.GT.10)) GO TO 902
      GO TO (101,151,201,251,301,351,401,451,501,551),INT1
C
C  OPERATION 1: OPEN THE GRAPHIC DEVICE.
  101 EXXN=150
      EXXX=3150
      EXYN=150
      EXYX=2400
      EXZX=118.11024
      EXZY=118.11024
      EXRA=0
c P. Nason, Nov 12 97
c       EXFT=0
      EXFT=1
c end P. Nason, Nov 12 97
      EXGL=0
      EXAS=0
      EXNM='UGDEVICE.DAT'
      EXBP='  '
      EXEP='  '
      EXBX='  '
      EXEX='  '
      EXBR='  '
      EXER='  '
c Initialize limits for bounding box
      ixl=1000000
      ixh=-1000000
      iyl=1000000
      iyh=-1000000
c
      CALL UGOPTN(DDST,INST,EXST)
      DDALX=DDXZZ
      CALL UGZ005(DDXRY,DDACX)
      DDAAT='POSTSCR '
      IF (EXRA.EQ.0) THEN
        DDABD(1,1)=EXXN
        DDABD(1,2)=EXXX
        DDABD(2,1)=EXYN
        DDABD(2,2)=EXYX
        DDABX=1.0/EXZX
        DDABY=1.0/EXZY
      ELSE
        DDABD(1,1)=EXYN
        DDABD(1,2)=EXYX
        DDABD(2,1)=EXXN
        DDABD(2,2)=EXXX
        DDABX=1.0/EXZY
        DDABY=1.0/EXZX
      END IF
      IF (DDABD(1,1).GE.DDABD(1,2)) GO TO 902
      IF (DDABD(2,1).GE.DDABD(2,2)) GO TO 902
      DDXID='DDA/PR00'
      DDXRA=EXRA
      DDXFT=EXFT
      DDXGL=EXGL
      DDXAS=EXAS
      CALL UGPR02(EXBP,DDXBP,DDXN1)
      CALL UGPR02(EXEP,DDXEP,DDXN2)
      CALL UGPR02(EXBX,DDXBX,DDXN3)
      CALL UGPR02(EXEX,DDXEX,DDXN4)
      CALL UGPR02(EXBR,DDXBR,DDXN5)
      CALL UGPR02(EXER,DDXER,DDXN6)
       DDXPC=0
      DDXPV=0
      DDXDC=0
C    OPEN A PATH TO THE DEVICE.
      CALL UGPR10(EXNM,DDXIO,INT1,INT2)
      IF (INT2.NE.0) GO TO 902
      DDXBM=INT1-DDXN6
      IF ((DDXBM-DDXN5).LT.64) GO TO 902
      IF ((DDXBM-DDXN5).LT.DDXN1) GO TO 902
      IF ((DDXBM-DDXN5).LT.DDXN2) GO TO 902
      IF ((DDXBM-DDXN5).LT.DDXN3) GO TO 902
      IF ((DDXBM-DDXN5).LT.DDXN4) GO TO 902
C    INITIALIZE THE OUTPUT PROCESS.
      DDXBN=0
      CALL UGPR08
C    GENERATE INITIAL RECORD IF NECESSARY.
      IF (DDXN1.GT.0) THEN
        CALL UGPR07(DDXBP(1:DDXN1))
        CALL UGPR08
      END IF
C    GENERATE INITIALIZATION RECORDS.
      CALL UGPR07('%!PS-Adobe-2.0 EPSF-1.2')
      CALL UGPR08
      CALL UGPR07(
     # '%%Creator: Unified Graphics System (modified 20-2-97)')
      CALL UGPR08
      CALL UGPR07('%%Pages: (atend)')
      CALL UGPR08
      CALL UGPR07('%%DocumentFonts: Courier')
      CALL UGPR08
      CALL UGPR07('%%BoundingBox: (atend)')
c      CALL UGPR06((72.0*EXYN)/(2.54*EXZY),0)
c      CALL UGPR06((72.0*EXXN)/(2.54*EXZX),0)
c      CALL UGPR06((72.0*EXYX)/(2.54*EXZY),0)
c      CALL UGPR06((72.0*EXXX)/(2.54*EXZX),0)
c      DDXBN=DDXBN-1
      CALL UGPR08
      CALL UGPR07('%%EndComments')
      CALL UGPR08
      CALL UGPR07('/Sw{setlinewidth}def')
      CALL UGPR08
      CALL UGPR07('/Sg{setgray}def')
      CALL UGPR08
      CALL UGPR07('/Sd{setdash}def')
      CALL UGPR08
      CALL UGPR07('/Sc{setrgbcolor}def')
      CALL UGPR08
      CALL UGPR07('/P {newpath')
      CALL UGPR08
      CALL UGPR07('    moveto')
      CALL UGPR08
      CALL UGPR07('0 0 rlineto')
      CALL UGPR08
      CALL UGPR07('    stroke}def')
      CALL UGPR08
      CALL UGPR07('/M {moveto}def')
      CALL UGPR08
      CALL UGPR07('/D {lineto}def')
      CALL UGPR08
      CALL UGPR07('/N {newpath}def')
      CALL UGPR08
      CALL UGPR07('/S {stroke}def')
      CALL UGPR08
c Add instructions to draw poligon economically
      call ugpr07('/i {newpath mark}def')
      call ugpr08
      call ugpr07(
     #'/f {moveto counttomark 2 idiv { rlineto }repeat pop stroke}def')
      call ugpr08
c end additions
      CALL UGPR07('/T {/Courier findfont')
      CALL UGPR08
      CALL UGPR07('    exch')
      CALL UGPR08
      CALL UGPR07('    scalefont')
      CALL UGPR08
      CALL UGPR07('    setfont}def')
      CALL UGPR08
      CALL UGPR07('/R {rotate}def')
      CALL UGPR08
      CALL UGPR07('/W {show}def')
      CALL UGPR08
      CALL UGPR07('/F {fill}def')
      CALL UGPR08
      CALL UGPR07('/X {gsave}def')
      CALL UGPR08
      CALL UGPR07('/Y {grestore}def')
      CALL UGPR08
      CALL UGPR07('%%EndProlog')
      CALL UGPR08
      GO TO 901
C
C  OPERATION 2: CLOSE THE GRAPHIC DEVICE.
  151 IF (DDXPV.NE.0) THEN
        CALL UGPR07('showpage')
        CALL UGPR08
C    GENERATE FINAL PICTURE RECORD IF NECESSARY.
        IF (DDXN4.GT.0) THEN
          CALL UGPR07(DDXEX(1:DDXN4))
          CALL UGPR08
        END IF
      END IF
C    GENERATE FILE TRAILER.
      CALL UGPR07('%%Trailer')
      CALL UGPR08
      call ugpr14(ixl,iyl,ixh,iyh)
      CALL UGPR07('%%Pages: ')
      CALL UGPR05(DDXPC)
      DDXBN=DDXBN-1
      CALL UGPR08
C    GENERATE FINAL RECORD IF NECESSARY.
      IF (DDXN2.GT.0) THEN
        CALL UGPR07(DDXEP(1:DDXN2))
        CALL UGPR08
      END IF
C    TERMINATE THE DATA SET.
      CALL UGPR11(DDXIO)
      GO TO 901
C
C  OPERATION 3: CLEAR THE SCREEN ON THE GRAPHIC DEVICE.
  201 IF (DDIN(2).NE.0) GO TO 902
      DDXDC=1
      DDXCI=-9999
      DDXCC=-9999
      DDXCL=-9999
      DDXCT=-9999
      DDXLA=0
      GO TO 901
C
C  OPERATION 4: MANIPULATE THE SCREEN ON THE GRAPHIC DEVICE.
  251 GO TO 901
C
C  OPERATION 5: BEGIN A NEW GRAPHIC SEGMENT.
  301 IF (DDXDC.NE.0) THEN
C    PROCESS DEFERRED CLEAR AND START A NEW PICTURE.
        IF (DDXPV.NE.0) THEN
          CALL UGPR07('showpage')
          CALL UGPR08
C    GENERATE FINAL PICTURE RECORD IF NECESSARY.
          IF (DDXN4.GT.0) THEN
            CALL UGPR07(DDXEX(1:DDXN4))
            CALL UGPR08
          END IF
          CALL UGPR07('%%PageTrailer')
          CALL UGPR08
          call ugpr14(ixl,iyl,ixh,iyh)
        END IF
C    GENERATE INITIAL PICTURE RECORD IF NECESSARY.
        IF (DDXN3.GT.0) THEN
          CALL UGPR07(DDXBX(1:DDXN3))
          CALL UGPR08
        END IF
C    START THE PICTURE ITSELF.
        DDXPC=DDXPC+1
        CALL UGPR07('%%Page: ')
        CALL UGPR05(DDXPC)
        CALL UGPR05(DDXPC)
        DDXBN=DDXBN-1
        CALL UGPR08
        CALL UGPR06((72.0*DDABX)/2.54,5)
        CALL UGPR06((72.0*DDABY)/2.54,5)
        CALL UGPR07('scale')
        CALL UGPR08
        CALL UGPR07('1 setlinecap')
        CALL UGPR08
        CALL UGPR07('1 setlinejoin')
        CALL UGPR08
        DDXDC=0
        DDXPV=1
      END IF
      DDEX(2)=0
      GO TO 901
C
C  OPERATION 6: TERMINATE A GRAPHIC SEGMENT.
 351  IF (DDXLA.NE.0) THEN
         call ugpr04(itmpl,jtmpl)
         CALL UGPR07('f')
         call ugpr08
         DDXLA=0
      END IF
c IF (DDXLA.NE.0) THEN
c        CALL UGPR07('S')
c        CALL UGPR08
c        DDXLA=0
c      END IF
      GO TO 901
C
C  OPERATION 7: MANIPULATE A GRAPHIC SEGMENT.
  401 GO TO 902
C
C  OPERATION 8: INQUIRE ABOUT A GRAPHIC PRIMITIVE.
 451  IF (DDXLA.NE.0) THEN
         call ugpr04(itmpl,jtmpl)
         CALL UGPR07('f')
         call ugpr08
         DDXLA=0
      END IF
c IF (DDXLA.NE.0) THEN
c        CALL UGPR07('S')
c        CALL UGPR08
c        DDXLA=0
c      END IF
      INT1=DDIN(2)
      IF ((INT1.LT.1).OR.(INT1.GT.5)) GO TO 902
      GO TO (456,461,466,471,476),INT1
C    INQUIRE ABOUT POINTS.
  456 CALL UGPR03(1,DDIN(3),DDIN(4),1)
      GO TO 901
C    INQUIRE ABOUT LINES.
  461 CALL UGPR03(2,DDIN(3),DDIN(4),DDIN(7))
      GO TO 901
C    INQUIRE ABOUT TEXT.
  466 CALL UGPR03(3,DDIN(3),DDIN(4),1)
      IF (DDXCT.NE.DDIN(7)) THEN
        CALL UGPR05(NINT(1.667*REAL(DDIN(7))))
        CALL UGPR07('T')
        CALL UGPR08
        DDXCT=DDIN(7)
      END IF
      IF (DDXRA.EQ.0) THEN
        DDXSA=DDIN(8)-90
      ELSE
        DDXSA=DDIN(8)
      END IF
      FLT1=REAL(DDXCT)
      FLT2=COS(REAL(DDIN(8))/57.2957795)
      FLT3=SIN(REAL(DDIN(8))/57.2957795)
      DDEX(2)=-NINT(FLT1*(0.500*FLT2-0.467*FLT3))
      DDEX(3)=-NINT(FLT1*(0.500*FLT3+0.467*FLT2))
      DDEX(4)=NINT(FLT1*FLT2)
      DDEX(5)=NINT(FLT1*FLT3)
      GO TO 901
C    INQUIRE ABOUT POLYGON-FILL.
  471 DDXSI=DDIN(3)
      DDXSC=DDIN(4)
      GO TO 901
C    INQUIRE ABOUT DEVICE-DEPENDENT DATA.
  476 CALL UGPR03(2,DDIN(3),DDIN(4),1)
      GO TO 901
C
C  OPERATION 9: DISPLAY A GRAPHIC PRIMITIVE.
  501 INT1=DDIN(2)
      IF ((INT1.LT.1).OR.(INT1.GT.5)) GO TO 902
      GO TO (506,511,516,521,526),INT1
C    DISPLAY POINTS.
  506 CALL UGPR04(DDIN(3),DDIN(4))
      CALL UGPR07('P')
      CALL UGPR08
      GO TO 901
c modification to get smaller ps files:
c use displacements instead of absolute coordinates
  511 IF (DDIN(5).EQ.0) THEN
        IF (DDXLA.NE.0) THEN
          call ugpr04(itmpl,jtmpl)
          CALL UGPR07('f')
          call ugpr08
          DDXLA=0
        END IF
        if(ddxbn.gt.0) call ugpr08
        CALL UGPR07('i ')
      ELSE
         call ugpr13(itmpl-DDIN(3),jtmpl-DDIN(4))
         if(ddxbn.gt.70) then
            ddxbn=ddxbn-1
            CALL UGPR08
         endif
      END IF
      itmpl=ddin(3)
      jtmpl=ddin(4)
      ixl=min(ixl,itmpl)
      ixh=max(ixh,itmpl)
      iyl=min(iyl,jtmpl)
      iyh=max(iyh,jtmpl)
      DDXLA=1
      GO TO 901
c end modification
C    DISPLAY TEXT.
  516 CALL UGPR09(1,DDST)
      CALL UGPR04(DDIN(3),DDIN(4))
      CALL UGPR07('M')
      CALL UGPR08
      IF (DDXSA.NE.0) THEN
        CALL UGPR05(DDXSA)
        CALL UGPR07('R')
        CALL UGPR08
      END IF
      CALL UGPR07('(')
      DO 517 INT1=1,LEN(DDST)
        IF ((DDXBM-DDXBN).LT.5) THEN
          CALL UGPR07('\')
          CALL UGPR08
        END IF
        IF ((DDST(INT1:INT1).EQ.'(').OR.
     X      (DDST(INT1:INT1).EQ.')').OR.
     X      (DDST(INT1:INT1).EQ.'\')) CALL UGPR07('\')
        CALL UGPR07(DDST(INT1:INT1))
  517 CONTINUE
      CALL UGPR07(') W')
      CALL UGPR08
      IF (DDXSA.NE.0) THEN
        CALL UGPR04(DDIN(3),DDIN(4))
        CALL UGPR07('M')
        CALL UGPR08
        CALL UGPR05(-DDXSA)
        CALL UGPR07('R')
        CALL UGPR08
      END IF
      GO TO 901
C    DISPLAY POLYGON-FILL.
  521 IF (DDXFT.EQ.0) THEN
        CALL UGPR03(4,DDXSI,8,1)
      ELSE
        CALL UGPR03(5,DDXSI,DDXSC,1)
      END IF
      CALL UGPR07('N')
      CALL UGPR08
      DO 522 INT1=1,DDIN(3)
        CALL UGPR04(DDIN(2*INT1+2),DDIN(2*INT1+3))
        IF (INT1.EQ.1) THEN
          CALL UGPR07('M')
        ELSE
          CALL UGPR07('D')
        END IF
        CALL UGPR08
  522 CONTINUE
c end modifications
      IF (DDXFT.EQ.0) THEN
        CALL UGPR07('X')
        CALL UGPR08
        CALL UGPR07('F')
        CALL UGPR08
        CALL UGPR07('Y')
        CALL UGPR08
        CALL UGPR03(5,DDXSI,DDXSC,1)
        CALL UGPR07('S')
        CALL UGPR08
      ELSE
        CALL UGPR07('F')
        CALL UGPR08
      END IF
      GO TO 901
C    DISPLAY DEVICE-DEPENDENT DATA.
  526 IF (LEN(DDST).LT.9) GO TO 901
      IF (DDST(1:8).NE.'POSTSCR:') GO TO 901
      IF ((DDXBM-DDXBN).LT.LEN(DDST)) GO TO 901
      CALL UGPR04(DDIN(3),DDIN(4))
      CALL UGPR07('M')
      CALL UGPR08
      CALL UGPR07(DDST(9:))
      CALL UGPR08
      GO TO 901
C
C  OPERATION 10: PROCESS MISCELLANEOUS CONTROL FUNCTIONS.
  551 GO TO 902
C
C  SET ERROR INDICATOR AND RETURN TO CALLER.
  901 DDEX(1)=0
      GO TO 903
  902 DDEX(1)=1
  903 RETURN
C
      END
      SUBROUTINE UGPR02(HEXS,BITS,NBIT)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO CONVERT A CHARACTER STRING            *
C *  CONTAINING HEXADECIMAL CHARACTERS INTO THE EQUIVALENT BIT        *
C *  STRING.  ANY ERROR CAUSES A ZERO STRING LENGTH TO BE RETURNED.   *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGPR02(HEXS,BITS,NBIT)                                    *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    HEXS  THE GIVEN STRING CONTAINING THE HEXADECIMAL CHARACTERS.  *
C *    BITS  THE COMPUTED BIT STRING.                                 *
C *    NBIT  THE NUMBER OF CHARACTERS IN THE BIT STRING.              *
C *                                                                   *
C *********************************************************************
C
      CHARACTER*(*) HEXS,BITS
      INTEGER       NBIT
C
      CHARACTER*1   DIGT(0:15)
      INTEGER       VAL1,VAL2
C
      INTEGER       INT1,INT2
C
      DATA          DIGT/'0','1','2','3','4','5','6','7',
     X                   '8','9','A','B','C','D','E','F'/
C
C  CONVERT THE HEXADECIMAL STRING.
      NBIT=0
      DO 105 INT1=1,(LEN(HEXS)-1),2
        IF (HEXS(INT1:INT1).EQ.' ') GO TO 201
        DO 101 INT2=0,15
          IF (HEXS(INT1:INT1).EQ.DIGT(INT2)) THEN
            VAL1=INT2
            GO TO 102
          END IF
  101   CONTINUE
        GO TO 202
  102   DO 103 INT2=0,15
          IF (HEXS(INT1+1:INT1+1).EQ.DIGT(INT2)) THEN
            VAL2=INT2
            GO TO 104
          END IF
  103   CONTINUE
        GO TO 202
  104   NBIT=NBIT+1
        BITS(NBIT:NBIT)=CHAR(16*VAL1+VAL2)
  105 CONTINUE
C
C  RETURN TO CALLER.
  201 RETURN
  202 NBIT=0
      GO TO 201
C
      END
      SUBROUTINE UGPR03(TYPE,INTN,COLR,LSTR)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO PROCESS THE INTENSITY, COLOR, AND     *
C *  LINE STRUCTURE INFORMATION.                                      *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGPR03(TYPE,INTN,COLR,LSTR)                               *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    TYPE  THE TYPE OF THE GRAPHIC PRIMITIVE BEING PROCESSED (1     *
C *          MEANS POINTS, 2 MEANS LINES, 3 MEANS TEXT, AND 4 AND 5   *
C *          MEAN FILL AREA).                                         *
C *    INTN  THE INTENSITY LEVEL.                                     *
C *    COLR  THE COLOR (NOT CURRENTLY USED FOR COLOR).                *
C *    LSTR  THE LINE STRUCTURE.                                      *
C *                                                                   *
C *********************************************************************
C
      INTEGER       TYPE,INTN,COLR,LSTR
C
      INCLUDE        'include/ugddacbk.for'
C
      INCLUDE        'include/ugddxpsc.for'
C
      INTEGER       AINT,ACOL
C
C  SET UP THE ACTUAL INTENSITY AND COLOR INDICES.
      IF (DDXGL.EQ.0) THEN
        AINT=1
      ELSE
        AINT=INTN
      END IF
      IF (DDXFT.EQ.0) THEN
        IF (TYPE.NE.4) THEN
          ACOL=1
        ELSE
          ACOL=8
        END IF
      ELSE
c        IF (COLR.NE.8) THEN
c          ACOL=1
c        ELSE
c          ACOL=8
c        END IF
         acol=colr
      END IF
C
C  SET THE INTENSITY LEVEL IF NECESSARY.
      IF (DDXCI.NE.AINT) THEN
        CALL UGPR06(MAX(REAL(AINT)*DDABX*118.11024,1.0),0)
        CALL UGPR07('Sw')
        CALL UGPR08
        DDXCI=AINT
      END IF
C
C  SET THE COLOR IF NECESSARY.
      IF (DDXCC.NE.ACOL) THEN
        IF (ACOL.EQ.1) THEN
c white
          CALL UGPR07('0 ')
          CALL UGPR07('Sg')
       elseif(acol.eq.2) then
c red
          CALL UGPR07('1. 0. 0. ')
          CALL UGPR07('Sc')
       elseif(acol.eq.3) then
c green
          CALL UGPR07('0. 1. 0. ')
          CALL UGPR07('Sc')          
       elseif(acol.eq.4) then
c blue
          CALL UGPR07('0. 0. 1. ')
          CALL UGPR07('Sc')          
       elseif(acol.eq.5) then
c yellow
          CALL UGPR07('1. 1. 0. ')
          CALL UGPR07('Sc')          
       elseif(acol.eq.6) then
c magenta
          CALL UGPR07('1. 0. 1. ')
          CALL UGPR07('Sc')          
       elseif(acol.eq.7) then
c cyan
          CALL UGPR07('0. 1. 1. ')
          CALL UGPR07('Sc')          
       ELSEif(acol.eq.8) then
c black
          CALL UGPR07('1 ')
          CALL UGPR07('Sg')
        END IF
        CALL UGPR08
        DDXCC=ACOL
      END IF
C
C  SET THE LINE STRUCTURE IF NECESSARY.
      IF (DDXCL.NE.LSTR) THEN
        CALL UGPR07('[')
        IF (LSTR.EQ.2) THEN
          CALL UGPR06(1.0/(3.0*DDABX),0)
          DDXBN=DDXBN-1
        ELSE IF (LSTR.EQ.3) THEN
          CALL UGPR06(REAL(AINT)*DDABX*118.11024,0)
          CALL UGPR06(1.0/(4.0*DDABX),0)
          DDXBN=DDXBN-1
        ELSE IF (LSTR.EQ.4) THEN
          CALL UGPR06(1.0/(4.0*DDABX),0)
          CALL UGPR06(1.0/(4.0*DDABX),0)
          CALL UGPR06(REAL(AINT)*DDABX*118.11024,0)
          CALL UGPR06(1.0/(4.0*DDABX),0)
          DDXBN=DDXBN-1
        END IF
        CALL UGPR07('] 0 Sd')
        CALL UGPR08
        DDXCL=LSTR
      END IF
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGPR04(XCRD,YCRD)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO ADD AN X AND Y COORDINATE TO THE      *
C *  OUTPUT BUFFER.                                                   *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGPR04(XCRD,YCRD)                                         *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    XCRD  THE X COORDINATE.                                        *
C *    YCRD  THE Y COORDINATE.                                        *
C *                                                                   *
C *********************************************************************
C
      INTEGER       XCRD,YCRD
C
      INCLUDE        'include/ugddacbk.for'
C
      INCLUDE        'include/ugddxpsc.for'
C
C  ADD THE COORDINATES TO THE OUTPUT BUFFER.
      IF (DDXRA.EQ.0) THEN
        CALL UGPR05(YCRD)
        CALL UGPR05(DDABD(1,2)+DDABD(1,1)-XCRD)
      ELSE
        CALL UGPR05(XCRD)
        CALL UGPR05(YCRD)
      END IF
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE ugpr14(ixl,iyl,ixh,iyh)
c Bounding box
      INCLUDE        'include/ugddacbk.for'
      INCLUDE        'include/ugddxpsc.for'
      ixsc(ix) = (72.0*ddabx*ix)/2.54
      iysc(iy) = (72.0*ddaby*iy)/2.54
      call ugpr07('%%BoundingBox: ')
      IF (DDXRA.EQ.0) THEN
        CALL UGPR05(ixsc(iyl)-2)
        CALL UGPR05(iysc(DDABD(1,2)+DDABD(1,1)-ixh)-2)
        call ugpr05(ixsc(iyh)+2)
        CALL UGPR05(iysc(DDABD(1,2)+DDABD(1,1)-ixl)+2)
      ELSE
        CALL UGPR05(ixsc(ixl)-2)
        CALL UGPR05(iysc(iyl)-2)
        CALL UGPR05(ixsc(ixh)+2)
        CALL UGPR05(iysc(iyh)+2)
      END IF
      call ugpr08
      ixl=1000000
      ixh=-ixl
      iyl=ixl
      iyh=-iyl
      end

      SUBROUTINE UGPR13(XCRD,YCRD)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO ADD AN X AND Y DISPLACEMENT TO THE    *
C *  OUTPUT BUFFER.                                                   *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGPR04(XCRD,YCRD)                                         *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    XCRD  THE X DISPLACEMENT                                       *
C *    YCRD  THE Y DISPLACEMENT.                                        *
C *                                                                   *
C *********************************************************************
C
      INTEGER       XCRD,YCRD
C
      INCLUDE        'include/ugddacbk.for'
C
      INCLUDE        'include/ugddxpsc.for'
C
C  ADD THE COORDINATES TO THE OUTPUT BUFFER.
      IF (DDXRA.EQ.0) THEN
        CALL UGPR05(YCRD)
        CALL UGPR05(-XCRD)
      ELSE
        CALL UGPR05(XCRD)
        CALL UGPR05(YCRD)
      END IF
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGPR05(VALU)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO ADD AN INTEGER VALUE TO THE OUTPUT    *
C *  BUFFER.  THE VALUE IS ALWAYS TERMINATED WITH A BLANK.  THE       *
C *  CALLER MUST ASSURE THAT THERE IS ENOUGH ROOM IN THE OUTPUT       *
C *  BUFFER TO HOLD THE NEW DATA.                                     *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGPR05(VALU)                                              *
C *                                                                   *
C *  THE PARAMETER IN THE CALLING SEQUENCE IS:                        *
C *    VALU  THE INTEGER VALUE TO BE ADDED TO THE OUTPUT BUFFER.      *
C *                                                                   *
C *********************************************************************
C
      INTEGER       VALU
C
      CHARACTER*13  STRG
      INTEGER       NBLK
C
C  CREATE THE STRING AND PACK IT INTO THE OUTPUT BUFFER.
      CALL UGCNVF(REAL(VALU),0,STRG(1:12),NBLK)
      STRG(13:13)=' '
      CALL UGPR07(STRG(13-NBLK:13))
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGPR06(VALU,NDEC)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO ADD A REAL VALUE TO THE OUTPUT        *
C *  BUFFER.  THE VALUE IS ALWAYS TERMINATED WITH A BLANK.  THE       *
C *  CALLER MUST ASSURE THAT THERE IS ENOUGH ROOM IN THE OUTPUT       *
C *  BUFFER TO HOLD THE NEW DATA.                                     *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGPR06(VALU,NDEC)                                         *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    VALU  THE REAL VALUE TO BE ADDED TO THE OUTPUT BUFFER.         *
C *    NDEC  THE NUMBER OF DECIMAL PLACES IN THE OUTPUT STRING.       *
C *                                                                   *
C *********************************************************************
C
      REAL          VALU
      INTEGER       NDEC
C
      CHARACTER*13  STRG
      INTEGER       NBLK
C
C  CREATE THE STRING AND PACK IT INTO THE OUTPUT BUFFER.
      CALL UGCNVF(VALU,NDEC,STRG(1:12),NBLK)
      STRG(13:13)=' '
      CALL UGPR07(STRG(13-NBLK:13))
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGPR07(STRG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO PACK A CHARACTER STRING INTO THE      *
C *  OUTPUT BUFFER.  THE CALLER MUST ASSURE THAT THERE IS ENOUGH      *
C *  ROOM IN THE OUTPUT BUFFER TO HOLD THE NEW DATA.                  *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGPR07(STRG)                                              *
C *                                                                   *
C *  THE PARAMETER IN THE CALLING SEQUENCE IS:                        *
C *    STRG  THE CHARACTER STRING.                                    *
C *                                                                   *
C *********************************************************************
C
      CHARACTER*(*) STRG
C
      INCLUDE        'include/ugddxpsc.for'
C
      INTEGER       NCHR
C
C  PACK THE CHARACTERS INTO THE OUTPUT BUFFER.
      NCHR=LEN(STRG)
      DDXBF(DDXBN+1:DDXBN+NCHR)=STRG(1:NCHR)
      DDXBN=DDXBN+NCHR
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGPR08
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO COMPLETE THE CURRENT RECORD, WRITE    *
C *  IT TO THE OUTPUT FILE, AND RE-INITIALIZE THE BUFFER.  THE        *
C *  CALLER MUST ASSURE THAT THERE IS ENOUGH ROOM IN THE OUTPUT       *
C *  BUFFER TO COMPLETE THE RECORD.                                   *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGPR08                                                    *
C *                                                                   *
C *  THERE ARE NO PARAMETERS IN THE CALLING SEQUENCE.                 *
C *                                                                   *
C *********************************************************************
C
      INCLUDE        'include/ugddxpsc.for'
C
C  VERIFY THAT THE BUFFER CONTAINS DATA TO BE WRITTEN OUT.
      IF (DDXBN.LE.0) GO TO 201
C
C  COMPLETE THE RECORD.
C    ADD THE END OF RECORD STRING IF NECESSARY.
      IF (DDXN6.GT.0) CALL UGPR07(DDXER(1:DDXN6))
C    TRANSLATE THE RECORD IF NECESSARY.
      IF (DDXAS.NE.0) CALL UGPR09(0,DDXBF(1:DDXBN))
C
C  WRITE THE RECORD TO THE DEVICE.
      CALL UGPR12(DDXIO,DDXBF(1:DDXBN))
C
C  RE-INITIALIZE THE BUFFER.
  201 DDXBN=0
C    ADD THE BEGINNING OF RECORD STRING IF NECESSARY.
      IF (DDXN5.GT.0) CALL UGPR07(DDXBR(1:DDXN5))
C
C  RETURN TO CALLER.
      RETURN
C
      END
      SUBROUTINE UGPR09(FLAG,STRG)
C
C *******************  THE UNIFIED GRAPHICS SYSTEM  *******************
C *                                                                   *
C *  THIS SUBROUTINE IS USED TO TRANSLATE CHARACTER STRINGS FROM THE  *
C *  COMPUTER CHARACTER SET TO THE DEVICE CHARACTER SET, OR FROM THE  *
C *  COMPUTER CHARACTER SET TO DISPLAYABLE CHARACTERS.                *
C *                                                                   *
C *  THE CALLING SEQUENCE IS:                                         *
C *    CALL UGPR09(FLAG,STRG)                                         *
C *                                                                   *
C *  THE PARAMETERS IN THE CALLING SEQUENCE ARE:                      *
C *    FLAG  TRANSLATE FLAG (0 MEANS COMPUTER TO DEVICE, AND 1        *
C *          COMPUTER TO DISPLAYABLE CHARACTERS).                     *
C *    STRG  THE CHARACTER STRING TO BE TRANSLATED.                   *
C *                                                                   *
C *********************************************************************
C
      INTEGER       FLAG
      CHARACTER*(*) STRG
C
      INTEGER       INT1
      CHARACTER*1   CHR1
C
C  TRANSLATE THE CHARACTER STRING AS NEEDED.
      IF (FLAG.EQ.1) THEN
        DO 101 INT1=1,LEN(STRG)
          CHR1=STRG(INT1:INT1)
          IF ((CHR1.LT.' ').OR.(CHR1.GT.'~')) STRG(INT1:INT1)='@'
  101   CONTINUE
      END IF
C
C  RETURN TO CALLER.
      RETURN
C
      END

